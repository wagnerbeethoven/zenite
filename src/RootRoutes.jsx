import React from "react";
import { Redirect } from "./../node_modules/react-router-dom";

import dashboardRoutes from "./views/dashboard/DashboardRoutes";
import utilitiesRoutes from "./views/utilities/UtilitiesRoutes";
import sessionRoutes from "./views/sessions/SessionRoutes";
import cardsRoutes from './views/Cards/CardsRoutes';
import pedidoRoutes from './views/Pedido/PedidoRoutes';

import materialRoutes from "./views/material-kit/MaterialRoutes";
import dragAndDropRoute from "./views/Drag&Drop/DragAndDropRoute";

import formsRoutes from "./views/forms/FormsRoutes";

const redirectRoute = [
  {
    path: "/",
    exact: true,
    component: () => <Redirect to="/dashboard/analytics" />
  }
];

const errorRoute = [
  {
    component: () => <Redirect to="/session/404" />
  }
];

const routes = [
  ...sessionRoutes,
  ...materialRoutes,
  ...dashboardRoutes,
  ...utilitiesRoutes,
  ...cardsRoutes,
  ...pedidoRoutes,
  ...dragAndDropRoute,
  ...formsRoutes,
  ...redirectRoute,
  ...errorRoute
];

export default routes;
