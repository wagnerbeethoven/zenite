import React, { Fragment, useState, useEffect, useContext } from "react";
import { withRouter } from "../../node_modules/react-router-dom";
import { connect } from "react-redux";
import AppContext from "../appContext";
import PropTypes from "prop-types";
import { setRedirect } from "../redux/actions/LoginActions";

const redirectRoute = props => {
  const { location, history } = props;
  const { pathname } = location;

  

  if (pathname != '/session/signin')  {
    props.setRedirect(pathname, location.search);
  }
  // switch(pathname)  {
  //   case '/wa':
  //     history.push({
  //       // pathname: "/cards/loja_virtual",
  //       // guid: 'eff7abd9-ad40-4454-9fcc-c95a3f645fc4',
  //       pathname: "/session/signin",
  //       state: { redirectUrl: pathname, guid: location.search.replace('?guid=', '') }  
  //     });
  //     break;
  //   default:
  //     history.push({
  //       pathname: "/session/signin",
  //       state: { redirectUrl: pathname }  
  //     });
  //     break;
  // }

  history.push({
    pathname: "/session/signin",
    //state: { location: location }
    state: { redirectUrl: pathname }
  });

};

const getAuthStatus = (props, routes) => {
  const { location, user } = props;
  const { pathname } = location;
  const matched = routes.find(r => r.path === pathname);
  const authenticated =
    matched && matched.auth && matched.auth.length
      ? matched.auth.includes(user.role)
      : true;

  return authenticated;
};

const AuthGuard = ({ children, ...props }) => {
  const { routes } = useContext(AppContext);

  let [authenticated, setAuthenticated] = useState(
    getAuthStatus(props, routes)
  );

  useEffect(() => {
    if (!authenticated) {
      redirectRoute(props);
    }
    setAuthenticated(getAuthStatus(props, routes));
  //}, [authenticated, routes, props]);
  }, [setAuthenticated, authenticated, routes, props]);

  return authenticated ? <Fragment>{children}</Fragment> : null;
};

const mapStateToProps = state => ({
  user: state.user,
  login: state.login,
  setRedirect : PropTypes.func.isRequired
});

export default withRouter(connect(mapStateToProps, { setRedirect })(AuthGuard));
