import {    
    SHOW_MESSAGE,
    CLOSE_MESSAGE,
  } from "../actions/AlertActions";
  
    const initialState = {
        message:  {
            severity: 'info',
            text: ''
        },
        show: false
    };
  
const AlertReducer = function(state = initialState, action) {
    switch (action.type) {
        case SHOW_MESSAGE: {
            
            return {
                show: true,
                message: action.payload,
            };     
        }

        case CLOSE_MESSAGE: {
            return {
                ...initialState,
            };     
        }

        default: {
            return {
            ...state
            };
        }
    }
};
  
  export default AlertReducer;
  