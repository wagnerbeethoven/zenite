import {
    GET_PRODUCTS_FILTERED,
    GET_CLIENTS_FILTERED,
    SELECT_PRODUCTS,
    SELECT_CLIENTS,
    SET_CARD,
    SET_LOADING,
    SHOW_MESSAGE,
    SET_TEXT_MESSAGE,
    SET_UM_CLIENTE,
    SET_CONNECTED
  } from "../actions/CardsActions";
  
  const initialState = {
    card: null,
    loading: false,
    productsFiltered: undefined,
    productsSelected: [],
    clientsFiltered: undefined,
    clientsSelected: [],
    showAlert: false,
    textMessage: '',
    phoneUmCliente: '',
    nomeUmCliente: '',
    connected: false
  };
  
const CardsReducer = function(state = initialState, action) {
    switch (action.type) {
        case GET_PRODUCTS_FILTERED: {
            if (action.payload)   {
                return {
                    ...state,
                    productsFiltered:  [...action.payload]
                };
            }
            else    {
                return {
                    ...state,
                    productsFiltered:  undefined
                };
            }
        }

        case SHOW_MESSAGE: {
            state.showAlert = action.payload;
            return {
                ...state,
            };     
        }

        case SET_LOADING: {
            return {
                ...state,
                loading: action.payload
            };     
        }

        case SET_TEXT_MESSAGE: {
            state.textMessage = action.payload;
            return {
                ...state,
            };     
        }

        case SET_CONNECTED: {
            return {
                ...state,
                connected: action.payload
            }
        }

        case SET_UM_CLIENTE: {
            state.nomeUmCliente = action.payload.nome;
            state.phoneUmCliente = action.payload.phone;
            return {
                ...state,
            };     
        }

        case SELECT_PRODUCTS: {
            
            if (action.params.check)    {
                if (Array.isArray(action.payload))  {
                    for (let i = 0; i < action.payload.length; i++) {
                        state.productsSelected.push(action.payload[i]);
                    }
                }
                else    {
                    state.productsSelected.push(action.payload);
                }
            }
            else    {
                if (Array.isArray(action.payload))  {

                }
                for (let i = 0; i < action.payload.length; i++) {
                    let index = state.productsSelected.indexOf(action.payload[i]);
                    if (index >= 0) {
                        state.productsSelected.splice(index, 1);
                    }
                }
            }
            if (state.productsSelected.length > 0)  {
                state.showAlert = false;
            }
            console.log('Payload:');
            console.log(action.payload);
            console.log('Result:');
            console.log(state.productsSelected);
            return {
                ...state,
            };        
        }

        case GET_CLIENTS_FILTERED: {
            if (action.payload)   {
                return {
                    ...state,
                    clientsFiltered:  [...action.payload]
                };
            }
            else    {
                return {
                    ...state,
                    clientsFiltered:  undefined
                };
            }
        }

        case SELECT_CLIENTS: {
            // if (action.payload && action.payload.length > 0)    {
            //     state.showAlert = false;
            // }
            return {
                ...state,
                clientsSelected:  action.payload
            };
        }

        case SET_CARD: {
            return {
                ...state,
                card: action.payload
            };
        }

        default: {
            return {
            ...state
            };
        }
    }
};
  
  export default CardsReducer;
  