export const SHOW_MESSAGE = "SHOW_MESSAGE";
export const CLOSE_MESSAGE = "CLOSE_MESSAGE";

export const showInfo = (text) => dispatch => {
    dispatch({
        type: SHOW_MESSAGE,
        payload: {
            severity: 'info',
            text: text 
        }
    });
};

export const showError = (text) => dispatch => {
    
    dispatch({
        type: SHOW_MESSAGE,
        payload: {
            severity: 'error',
            text: text 
        }
    });
};

export const showWarning = (text) => dispatch => {
    dispatch({
        type: SHOW_MESSAGE,
        payload: {
            severity: 'warning',
            text: text 
        }
    });
};

export const showSuccess = (text) => dispatch => {
    dispatch({
        type: SHOW_MESSAGE,
        payload: {
            severity: 'success',
            text: text 
        }
    });
};

export const closeAlert = () => dispatch => {
    dispatch({
        type: CLOSE_MESSAGE,
        payload: null
    });
};
