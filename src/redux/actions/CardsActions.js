import qs from 'qs';
import api from '../../services/api';

export const GET_PRODUCTS_FILTERED = "GET_PRODUCTS_FILTERED";
export const GET_CLIENTS_FILTERED = "GET_CLIENTS_FILTERED";
export const SELECT_PRODUCTS = "SELECT_PRODUCTS";
export const SELECT_CLIENTS = "SELECT_CLIENTS";
export const SHOW_MESSAGE = "SHOW_MESSAGE";
export const SET_CARD = "SET_CARD";
export const SET_LOADING = "SET_LOADING";
export const SET_TEXT_MESSAGE = "SET_TEXT_MESSAGE";
export const SET_UM_CLIENTE = "SET_UM_CLIENTE";
export const SET_CONNECTED = "SET_CONNECTED";

export const getProductFiltered = (filter) => dispatch => {
    api.post("/api/produtotextil/GetProdutosPesquisaAvancada",  qs.stringify(filter))
    .then(res => {
        dispatch({
        type: GET_PRODUCTS_FILTERED,
        payload: res.data
        });
    });
};

export const getClientsFiltered = (filter) => dispatch => {
    api.post("/api/cliente/GetClientesPesquisaAvancada", qs.stringify(filter))
    .then(res => {
        
        dispatch({
        type: GET_CLIENTS_FILTERED,
        payload: res.data.map(elem => {
            elem['id'] = elem.Key;
            return elem;
            })
        });
    });
};

export const setLoading = (value) => dispatch => {
    dispatch({
        type: SET_LOADING,
        payload: value
    });
};

export const setConnected = (value) => dispatch => {
    if (value)  {
        dispatch({
            type: SET_CONNECTED,
            payload: value
        });
    }
    else    {
        api.get("/api/empresa/InstanceConnected").then(res => {
            dispatch({
            type: SET_CONNECTED,
            payload: res.data.connected
            });
        });
    }
};

export const resetProductFiltered = () => dispatch => {
    dispatch({
        type: GET_PRODUCTS_FILTERED,
        payload: undefined
    });
};

export const resetClientsFiltered = () => dispatch => {
    dispatch({
        type: GET_CLIENTS_FILTERED,
        payload: undefined
    });
};

export const selectProduct = (check, tamanhoCorList) => dispatch => {
    dispatch({
        type: SELECT_PRODUCTS,
        params: { check: check },
        payload: tamanhoCorList.map(elem => elem.Id)
    });
};

export const setTextMessge = (text) => dispatch => {
    dispatch({
        type: SET_TEXT_MESSAGE,
        payload: text
    });
};

export const setUmCliente = (nome, phone) => dispatch => {
    dispatch({
        type: SET_UM_CLIENTE,
        payload: { nome : nome, phone: phone }
    });
};

// export const unselectProduct = (tamanhoCorList) => dispatch => {
//     dispatch({
//         type: SELECT_PRODUCTS,
//         params : 
//         payload: tamanhoCorList.map(elem => elem.Id)
//     });
// };

export const selectClient = (ids) => dispatch => {
    dispatch({
        type: SELECT_CLIENTS,
        payload: ids
    });
};

export const openAlert = () => dispatch => {
    dispatch({
        type: SHOW_MESSAGE,
        payload: true
    });
};

export const closeAlert = () => dispatch => {
    dispatch({
        type: SHOW_MESSAGE,
        payload: false
    });
};

export const setCard = (nome, msg) => dispatch => {
    dispatch({
        type: SET_CARD,
        payload: { nome: nome, message: msg }
    });
};
