import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import TableRow from '@material-ui/core/TableRow';
import { DataGrid } from '@material-ui/data-grid';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import Box from '@material-ui/core/Box';
import { AppBar, Tab, Table, Tabs } from '@material-ui/core';
import Collapse from '@material-ui/core/Collapse';
import IconButton from '@material-ui/core/IconButton';
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp';
import DeleteIcon from '@material-ui/icons/Delete';
import CheckIcon from '@material-ui/icons/Check';
import ErrorIcon from '@material-ui/icons/Error';
import AddIcon from '@material-ui/icons/Add';
import PlayIcon from '@material-ui/icons/PlayArrow'
import Modal from '@material-ui/core/Modal';

import Typography from '@material-ui/core/Typography';
import Checkbox from '@material-ui/core/Checkbox';
import TableHeadLayout from './TableHeadLayoutCliente.js'

const useRowStyles = makeStyles((theme)=>({
    
    root: {
        '& > *': {
        borderBottom: 'unset',
        },
    }
}));

const useStyles = makeStyles((theme) => ({
    root: {
      flexGrow: 1,
      backgroundColor: theme.palette.background.paper,
    },
  }));

function TabPanel(props) {
    const { children, value, index, ...other } = props;
  
    return (
      <div
        role="tabpanel"
        hidden={value !== index}
        id={`simple-tabpanel-${index}`}
        aria-labelledby={`simple-tab-${index}`}
        {...other}
      >
        {value === index && (
          <Box p={3}>
            <Typography>{children}</Typography>
          </Box>
        )}
      </div>
    );
  }
  
  TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.any.isRequired,
    value: PropTypes.any.isRequired,
  };

function tabProps(index) {
    return {
        id: `simple-tab-${index}`,
        'aria-controls': `simple-tabpanel-${index}`,
    };
}

export default function TableRowLayoutCliente(props) {
    
    const {row, visibilityCols, hasDetail, hasCheck, isChecked, deleteFunction, hasModal, actionFunction } = props;
    const [detailOpen, setDetailOpen] = React.useState(false);
    const [detailSet, setDetailSet] = React.useState([]);
    const classes = useRowStyles();
    const classes2 = useStyles();
    const [tab, setTab] = React.useState(0);  

    const handleTabChange = (event, newValue) => {
      setTab(newValue);
    };

    return (
      <>
        <TableRow 
            hover
            key={row.Key}
            className={classes.root}
            >
            {
              hasCheck &&
                <TableCell>
                    <Checkbox
                        //key={row.Key}
                        onClick={(event) => props.handleClick(event, row.Key)}
                        checked={isChecked}
                        color="primary"
                        inputProps={{ 'aria-label': 'secondary checkbox' }}
                    />
                </TableCell>
            }
            {
                hasDetail &&
                <TableCell>
                    {/* <IconButton aria-label="expand row" size="small" onClick={(event) => getDetails(event)}> */}
                    <IconButton aria-label="expand row" size="small">
                        {detailOpen ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
                    </IconButton>
                </TableCell>
            }
            {
                Object.keys(row).map((column)=>{
                    if(column !== "Key" && visibilityCols[column]){
                        return (
                            props.handleClick !== undefined?
                            <TableCell onClick={(event) => props.handleClick(event, row)} align="left">{row[column]}</TableCell>
                            :
                            <TableCell align="left">{row[column]}</TableCell>)
                            // <TableCell onClick={(event) => props.handleClick(event, row.Key)} key={row[key]} align="left">{row[key]}</TableCell>
                            // :
                            // <TableCell key={row[key]} align="left">{row[key]}</TableCell>)
                    }
                })
            }
            {
                deleteFunction &&
                <TableCell>
                    <IconButton aria-label="expand row" color='primary' alt='Excluir item' size="small" onClick={(args) => deleteFunction(row.Key)}>
                        <DeleteIcon />
                    </IconButton>
                </TableCell>
            }
            {
                actionFunction &&
                <TableCell>
                    <IconButton aria-label="expand row" color='primary' label='Conferencia Cega' size="small" onClick={(args) => actionFunction(row.Key)}>
                        <PlayIcon />
                    </IconButton>
                </TableCell>
            }
            {
              row['status'] == 'OK' &&
                <TableCell>
                    <CheckIcon />
                </TableCell>
            }
            {
              row['status'] == 'ERROR' &&
                <TableCell>
                    <ErrorIcon color='primary' />
                </TableCell>
            }
            {
                hasModal &&
                <TableCell>
                    <IconButton className="addButton" disabled={!isChecked} aria-label="expand row" size="small" onClick={() => props.handleOpenModal(row.Key, row.NumPedido)} >
                        <div className="btnContent">
                            <AddIcon></AddIcon><span> Nota Fiscal</span>
                        </div>
                    </IconButton>
                </TableCell>
            }
          
        </TableRow>
        {
                detailSet.length > 0 &&
                <div className={classes2.root}>
                <AppBar position="static">
                  <Tabs value={tab} onChange={handleTabChange} aria-label="simple tabs example">
                    <Tab label="Envios" {...tabProps(0)} />
                    <Tab label="Produtos" {...tabProps(1)} />
                  </Tabs>
                </AppBar>
                <TabPanel value={tab} index={0}>
                  Item One
                </TabPanel>
                <TabPanel value={tab} index={1}>
                    <TableRow>
                        <TableCell style={{ paddingBottom: 0, paddingTop: 0 }} colSpan={6}>
                            <Collapse in={detailOpen} timeout="auto" unmountOnExit>
                                <Box margin={1}>
                                    <Table size="small" aria-label="purchases">
                                    <TableHeadLayout titles = {{Nome:'Nome', Quantidade: 'Quantidade'}}/>
                                    <TableBody>
                                        {detailSet.map((detail) => (
                                            <TableRowLayoutCliente 
                                                key={detail.Key} 
                                                row={detail} 
                                                visibilityCols={{Nome:true, Quantidade: true}}
                                            />
                                        ))}
                                    </TableBody>
                                    </Table>
                                </Box>
                            </Collapse>
                        </TableCell>
                    </TableRow>
                </TabPanel>
              </div>


            // // <div style={{ height:`${60 + 60 * detailSet.length}px`, className:classes.grid, width: '100%' }} >
            // <div style={{ height:`${60 + 60 * detailSet.length}px`, width: '100%' }} >
            //     <DataGrid 
            //         //{...data}
            //         rows = {detailSet}
            //         //columns={cols}
            //         autoHeight = {true}x
            //         hideFooterPagination = {true}
            //         hideFooterSelectedRowCount = {true}
            //         disableSelectionOnClick = {true}
            //         hideFooterRowCount = {true}
            //         hideFooter = {true}
            //         autoHeight = {true}
            //         columnBuffer={0}
            //     />
            // </div>

        }
      </>
    );
  }