import * as React from 'react';
import { Fragment } from 'react';
import clsx from 'clsx';
import { Accordion, TextField, AccordionSummary, AccordionDetails, Button, Chip, Input, Grid, Slider, Tooltip, Typography } from '@material-ui/core';
import { ToggleButton, ToggleButtonGroup } from '@material-ui/lab';
import { DataGrid } from '@material-ui/data-grid';
import { makeStyles } from '@material-ui/core/styles';
import { Icon } from '@material-ui/core';
import { loadCSS } from 'fg-loadcss';
import AddShoppingCartIcon from '@material-ui/icons/AddShoppingCart';
import CancelIcon from '@material-ui/icons/Cancel';
import RemoveShoppingCartIcon from '@material-ui/icons/RemoveShoppingCart';
import ArrowUpwardIcon from '@material-ui/icons/ArrowUpward';
import localStorageService from "../../services/localStorageService";
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import AddIcon from '@material-ui/icons/Add';
import RemoveIcon from '@material-ui/icons/Remove';

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
        '& .super-app-theme--cell': {
            backgroundColor: 'rgba(224, 183, 60, 0.55)',
            color: '#1a3e72',
            fontWeight: '600',
        },
        '& .super-app.negative': {
            backgroundColor: 'rgba(157, 255, 118, 0.49)',
            color: '#1a3e72',
            fontWeight: '600',
        },
        '& .super-app.positive': {
            backgroundColor: '#d47483',
            color: '#1a3e72',
            fontWeight: '600',
        },
        '& .super-app.disabled': {
            backgroundColor: '#888888',
            color: '#888888',
            fontWeight: '600',
        },
        '& > .fa': {
            margin: theme.spacing(2),
        }
    },
    grid: {
        '& .grid.heading': {
            fontSize: theme.typography.pxToRem(15),
            fontWeight: theme.typography.fontWeightRegular,
            fontWeight: 'bold'
        },
        '& .grid.body': {
            fontSize: theme.typography.pxToRem(10),
            fontWeight: theme.typography.fontWeightRegular,
            fontWeight: 'normal'
        }
    },
    details: {
        alignItems: 'center',
    },
    column: {
        flexBasis: '33.33%',
    },
    heading: {
        fontSize: theme.typography.pxToRem(15),
        fontWeight: theme.typography.fontWeightRegular,
    }
}));

//ColumnVirtualizationGrid
export default function ProductGrid(props) {
    //const data = useData(props.tamanhoCorList);
    const classes = useStyles();
    const [rows, setRows] = React.useState([]);
    const [cols, setCols] = React.useState([]);
    //const [grid, setGrid] =  React.useState([]);
    const [subTotal, setSubTotal] = React.useState(0);
    //const[parciaisCor, setParciaisCor] = React.useState([]);
    const [nomesTamanhos, setNomesTamanhos] = React.useState([]);
    const [fator, setFator] = React.useState(1);

    const handleFator = (event, newFator) => {
        event.preventDefault();
        setFator(newFator);
      };

      const handleInputChange = (event) => {

        event.preventDefault();
        setFator(event.target.value === '' ? 0 : Number(event.target.value));
    };

    const handleBlur = () => {

        if (fator < 0) {
            setFator(0);
        } else if (fator > 100) {
            setFator(100);
        }
    };

    const alterarSubPedido = (event, params, inc) => {

        event.preventDefault();
        console.log(params);
        console.log(fator);
        if (params.data[params.field] > 0 || params.data['pedido_' + params.field] > 0) {
            let lastRows = params.api.getRowModels().map((elem) => elem.data);
            let row = lastRows[params.rowIndex];
            if (inc) {
                if (row[params.field] - fator < 0) {
                    row['pedido_' + params.field] += row[params.field];
                    row[params.field] = 0;
                }
                else {
                    row['pedido_' + params.field] += fator;
                    row[params.field] -= fator;
                }
            }
            else {
                if (row['pedido_' + params.field] - fator < 0) {
                    row[params.field] += row['pedido_' + params.field];
                    row['pedido_' + params.field] = 0;
                }
                else {
                    row[params.field] += fator;
                    row['pedido_' + params.field] -= fator;
                }
            }
            let newRows = lastRows.filter(e => e.id !== params.data.id);
            newRows.splice(params.rowIndex, 0, row);
            setRows(newRows);
        }
    };

    const alterarSubPedido2 = (event, params) => {
        event.preventDefault();
        let value = Number.parseInt(event.target.value);
        console.log(params);
        console.log(fator);
        if (params.data[params.field] > 0 || params.data['pedido_' + params.field] > 0) {
            let lastRows = params.api.getRowModels().map((elem) => elem.data);
            let row = lastRows[params.rowIndex];

            if (row[params.field] - value < 0) {
                row['pedido_' + params.field] = row[params.field];
                row[params.field] = 0;
            }
            else {
                row['pedido_' + params.field] = value;
                row[params.field] = params.data[params.field] - value;
            }
            let newRows = lastRows.filter(e => e.id !== params.data.id);
            newRows.splice(params.rowIndex, 0, row);
            setRows(newRows);    
        }
    };

    function addCart(event) {
        event.preventDefault();
        console.log(rows);
        if (localStorageService.removeItem(`SUB_PEDIDO_${props.linkCardClienteGuid}_${props.referencia}`)) {
            if (subTotal > 0) {
                localStorageService.setItem(`SUB_PEDIDO_${props.linkCardClienteGuid}_${props.referencia}`, rows);
                props.setStatusToPedido();
            }
        }
    }

    function removeCart(event) {
        event.preventDefault();
        localStorageService.removeItem(`SUB_PEDIDO_${props.linkCardClienteGuid}_${props.referencia}`);
        setRows(rows.map((elem) => {
            nomesTamanhos.forEach((elemTam) => {
                elem['pedido_' + elemTam] = 0;
            });
            return elem;
        }));
        props.setStatusToNeutro();
    }

    function totalizar() {
        let subTot = 0;
        let newParciaisCor = rows.map((elem) => {
            let parciaisTam = [];
            for (let i = 0; i < nomesTamanhos.length; i++) {
                let prod = props.tamanhoCorList.find(e => e.Tamanho == nomesTamanhos[i] && e.CorNome == elem.cor);
                let qtde = elem['pedido_' + nomesTamanhos[i]];
                let valUnit = elem['valUnit_' + nomesTamanhos[i]];
                if (prod && qtde > 0) {
                    parciaisTam.push({ id: prod.Id, tamanho: nomesTamanhos[i], qtde: qtde, valUnit: valUnit, subTotal: qtde * valUnit });
                }
            }
            if (parciaisTam.length > 0) {
                return { referencia: elem.referencia, cor: elem.cor, rgb: elem.rgb, parciaisTamanho: parciaisTam };
            }
        }).filter(e => e != undefined);
        console.log(newParciaisCor);
        newParciaisCor.forEach((elem) => {
            elem.parciaisTamanho.forEach((elemTam) => subTot += elemTam.subTotal);
        });
        //setParciaisCor(newParciaisCor);
        // if (subTot != 0)    {
        //     props.setStatusToPedido();
        // }
        // else {
        //     props.setStatusToNeutro();
        // }
        setSubTotal(subTot);
    }

    React.useEffect(() => {
        if (rows.length == 0) {
            setSubTotal(0);
            //setParciaisCor([]);
        }
        else {
            totalizar();
        }
        // setRowsSumario(
        //     rows.map((elem) => {
        //         let row = { id: elem.id, cor: elem.cor, rgb: elem.rgb,  };
        //         nomesTamanhos.
        //     });
        // );
    }, [rows])

    function configRows(cores, tamanhos) {
        let newRows = [];
        for (let i = 0; i < cores.length; i++) {
            const row = {
                id: props.referencia + "-" + cores[i].nome,
                referencia: props.referencia,
                cor: cores[i].nome,
                rgb: cores[i].rgb
            };

            let match;
            for (let j = 0; j < tamanhos.length; j++) {
                match = props.tamanhoCorList.find(e => e.CorNome == cores[i].nome && e.Tamanho == tamanhos[j]);
                if (match) {
                    row[tamanhos[j]] = match.Qtde;
                    row['pedido_' + tamanhos[j]] = 0;
                    row['valUnit_' + tamanhos[j]] = match.ValUnit;
                }
                else {
                    row[tamanhos[j]] = 0;
                    row['pedido_' + tamanhos[j]] = 0;
                    row['valUnit_' + tamanhos[j]] = 0;
                }
            }

            newRows.push(row);
        }
        let subPedido = localStorageService.getItem(`SUB_PEDIDO_${props.linkCardClienteGuid}_${props.referencia}`);
        if (subPedido != null) {
            setRows(subPedido);
        }
        else {
            setRows(newRows);
        }
    }

    function configColumns(tamanhos) {
        let columns = [{ field: 'id', hide: true }, { field: 'rgb', hide: true }];
        columns.push({
            cellClassName: ( params )=> "product-grid-cell",
            field: 'cor', headerName: ' ',
            renderCell: (params) => {
                return (
                    <div className="product-color" alt={params.data.id}>
                        {/* <span style={{ alignItems: 'center', fontSize: 10, alignContent: 'left', classesName: classes.grid.heading, color: `#${params.data.rgb}` }}> */}
                        <Tooltip className="product-color-name" title={params.data.cor}>
                            <Icon style={{ color: `#${params.data.rgb}` }}>circle</Icon>
                        </Tooltip>
                        <span className="product-color-name-mobile">{params.data.cor}</span>
                        {/* <Icon
                            className="fa fa-plus-circle"
                            style={{ color: `'#${params.data.rgb}'`}} 
                        /> */}
                        {/* <SvgIcon key={params.data.id} 
                            htmlColor={`'#${params.data.rgb}'`} 
                            shapeRendering='crispEdges'
                      /> */}

                    </div >
                );
            },
            // renderHeader: (params) => (
            //     <div>
            //         <ArrowUpwardIcon />
            //         {/* <Button size="small">
            //             x 1
            //         </Button>
            //         <Button size="small">
            //             x 10
            //         </Button>
            //         <Button size="small">
            //             x 100
            //         </Button> */}
            //     </div>
            // ),
        });

        for (let j = 0; j < tamanhos.length; j++) {
            columns.push({
                field: tamanhos[j], headerName: tamanhos[j],

                renderCell: (params) => {
                    if (params.data[params.field] + params.data['pedido_' + params.field] != 0) {
                        if (props.isCart) {
                            return (
                                <Input
                                    //readOnly={false}
                                    value={params.data['pedido_' + params.field]}
                                    id={params.data.id}
                                    type='number'
                                    inputProps={{
                                        min: 0,
                                        step: fator,
                                        readOnly: false,
                                        max: params.data[params.field],
                                        //type: 'number',
                                    }}
                                    onChange={(event) => alterarSubPedido2(event, params)}
                                />

                                // <span style={{ color: `#${params.data.rgb}` }}>
                                //     <Icon
                                //         fontSize='small'
                                //         //onMouseMove = {(event) => event.source.style.cursor = 'pointer' }
                                //         onClick={(event) => alterarSubPedido(event, params, false)}
                                //     >
                                //         remove_circle
                                //     </Icon>



                                //     <strong>
                                //         {params.data['pedido_' + params.field].toFixed(0)}
                                //     </strong>
                                //     {/* {` (${(params.data['pedido_' + params.field] * params.data['valUnit_' + params.field]).toLocaleString('pt-BR', { currency: 'BRL', minimumFractionDigits: 2, maximumFractionDigits: 2 })})`} */}
                                //     <Icon

                                //         //onMouseMove = {(event) => event.source.style.cursor = 'pointer' }
                                //         onClick={(event) => alterarSubPedido(event, params, true)}
                                //     >
                                //         add_circle
                                //     </Icon>
                                // </span>
                            );
                        }
                        else {
                            return (
                                <Icon className="product-color-icon" style={{ color: `#${params.data.rgb}` }}> circle </Icon>
                            );
                        }
                    }
                    else {
                        return (
                            <span className="product-none">
                                0
                            </span>
                        );
                    }
                },
                cellClassName: (params) => {
                    return "product-grid-cell";
                    // if (params.field == 'cor') {
                    //     return clsx('grid', {
                    //         heading: true
                    //     });
                    // }
                    // else {
                    //     return clsx('grid', {
                    //         body: true
                    //     });
                    // }
                },
            });
        }
        
        setCols(columns);
    }

    function initGrid() {
        const cores = [];
        const tamanhos = [];

        const node = loadCSS(
            'https://use.fontawesome.com/releases/v5.12.0/css/all.css',
            document.querySelector('#font-awesome-css'),
        );

        node.parentNode.removeChild(node);

        const ordemTamanhos = ['UN', 'PP', 'P', 'M', 'G', 'XG'];

        props.tamanhoCorList.sort((a, b) => {
            let aIsInt = Number.isInteger(a.Tamanho);
            let bIsInt = Number.isInteger(b.Tamanho);
            if (aIsInt && bIsInt) {
                return Number.parseInt(a.Tamanho) - Number.parseInt(b.Tamanho);
            }
            else if (aIsInt) {
                return -1;
            }
            else if (bIsInt) {
                return 1;
            }
            else if (ordemTamanhos.indexOf(a.Tamanho) < ordemTamanhos.indexOf(b.Tamanho)) {
                return -1;
            }
            if (ordemTamanhos.indexOf(a.Tamanho) > ordemTamanhos.indexOf(b.Tamanho)) {
                return 1;
            }
            return 0;
        });

        for (let i = 0; i < props.tamanhoCorList.length; i++) {
            if (!cores.find(e => e.nome == props.tamanhoCorList[i].CorNome)) {
                cores.push({ nome: props.tamanhoCorList[i].CorNome, rgb: props.tamanhoCorList[i].CorRgb });
            }
            if (!tamanhos.includes(props.tamanhoCorList[i].Tamanho)) {
                tamanhos.push(props.tamanhoCorList[i].Tamanho);
            }
        }

        setNomesTamanhos(tamanhos);

        configRows(cores, tamanhos);
        configColumns(tamanhos);
    }

    React.useEffect(() => {
        initGrid();
    }, []);

    React.useEffect(() => {

        if (nomesTamanhos.length > 0) {
            configColumns(nomesTamanhos);
        }
    }, [fator]);

    return (
        <div className={classes.root}>
            <Grid container spacing={0} alignContent='center' alignItems='center'>
                <Grid className="product-grid">
                    <div className="product-grid-options" style={{ height: `${60 + (60 * rows.length)}px`, className: classes.grid, width: '100%' }} >
                        <DataGrid
                            rows={rows}
                            columns={cols}
                            columnBuffer={0}
                            // autoHeight={true}

                            // RODAPE DA TABELA 
                            hideFooterPagination={true}
                            hideFooterSelectedRowCount={true}
                            hideFooter={true}
                            hideFooterRowCount={true}
                            //LINHAS
                            // disableExtendRowFullWidth={true}
                            // COLUNAS
                            disableColumnMenu={true}
                            disableColumnReorder={true}
                            // BORDAS
                            showCellRightBorder={true}
                            showColumnRightBorder={true}

                            // disableSelectionOnClick={true}
                        />
                    </div>
                </Grid>
                {
                    props.isCart &&
                    <Fragment>
                        <Grid className="button-group-order" item xs={12}>
                            {
                                subTotal > 0 &&
                                <Tooltip title="Adicionar ao carrinho">
                                    <Button className="button-group-order-add" onClick={(event) => addCart(event)}>
                                        <AddShoppingCartIcon />
                                    </Button>
                                </Tooltip>
                            }
                            <Tooltip title="Remover do carrinho">
                                <Button className="button-group-order-remove" size="small" onClick={(event) => { removeCart(event) }}>
                                    <RemoveShoppingCartIcon />
                                </Button>
                            </Tooltip>
                            <Tooltip title="Cancelar alterações recentes">
                                <Button className="button-group-order-cancel " size="small" onClick={(event) => { event.preventDefault(); props.setStatusToNeutro() }}>
                                    <CancelIcon />
                                </Button>
                            </Tooltip>
                        </Grid>
                        <Grid className="price-footer" item xs={12}>
                            <Accordion>
                                <AccordionSummary
                                    expandIcon={<ExpandMoreIcon />}
                                    aria-controls="panel1a-content"
                                    id="panel1a-header"
                                >
                                    <Typography gutterBottom={true}>
                                        <strong>
                                            {`Sub-total: R$ ${subTotal.toLocaleString('pt-BR', { currency: 'BRL', minimumFractionDigits: 2, maximumFractionDigits: 2 })}`}
                                        </strong>
                                    </Typography>
                                </AccordionSummary>
                                <AccordionDetails>
                                    <Grid container spacing={0} alignContent='center' alignItems='center'>
                                        <Grid item xs={4}>
                                            <Typography id="input-slider" gutterBottom>
                                                Itens por vez:
                                            </Typography>
                                        </Grid>
                                        <Grid item xs={8} className="price-footer-input">
                                            <ToggleButtonGroup className="toggle-group" value={fator} onChange={handleFator} aria-label="text formatting" exclusive={true}>
                                                <ToggleButton key={1} value={1} aria-label="bold">
                                                    1
                                                </ToggleButton>
                                                <ToggleButton key={5} value={5} aria-label="bold">
                                                    5
                                                </ToggleButton>
                                                <ToggleButton key={10} value={10} aria-label="bold">
                                                    10
                                                </ToggleButton>
                                                <ToggleButton key={20} value={20} aria-label="bold">
                                                    20
                                                </ToggleButton>
                                            </ToggleButtonGroup>
                                        </Grid>

                                        <Grid item xs={12}>
                                            {/* <Typography variant="caption">
                                            Select your destination of choice
                                            <br />
                                        </Typography> */}
                                        </Grid>
                                    </Grid>

                                </AccordionDetails>
                            </Accordion>
                        </Grid>
                    </Fragment>
                }
            </Grid>

        </div>
    );
}
