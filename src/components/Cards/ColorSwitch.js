import React from 'react';
import { FormGroup, FormControlLabel, Switch } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';

export default function ColorSwitch(props) {

    const CustomSwitch = withStyles({
        switchBase: {
          color: `#${props.color}`,
          '&$checked': {
            color: `#${props.color}`,
          },
          '&$checked + $track': {
            backgroundColor: `#${props.color}`,
          },
        },
        checked: {},
        track: {},
      })(Switch);
      

    return (
        <FormControlLabel
            control={<CustomSwitch 
                checked={props.checked} 
                onChange={props.handleChange}
                name={props.nome}
            />}
            label={props.nome}
        />
    );
}