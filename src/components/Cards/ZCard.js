import React, { useEffect, useState } from 'react';
import ReactDOM from 'react-dom';
import NumberFormat from 'react-number-format';
//import Text from 'react-text';
//import { Carousel } from 'react-responsive-carousel';
import { Carousel } from 'antd';
import { Button, Card, CardActionArea, CardActions, CardContent, CardMedia, Checkbox, Fade, Typography } from '@material-ui/core';
import ShoppingCartIcon from '@material-ui/icons/ShoppingCartSharp';
import Lens from '@material-ui/icons/Lens';
import ZoomIn from '@material-ui/icons/ZoomIn';
import ZoomOut from '@material-ui/icons/ZoomOut';
import SaveIcon from '@material-ui/icons/Save';
//import AddShoppingCartIcon from '@material-ui/icons/AddShoppingCart';
import { makeStyles, useTheme, withStyles } from '@material-ui/core/styles';
import { NoMeetingRoomOutlined } from '@material-ui/icons';
import ReactImageZoom from 'react-image-zoom';
import ProductGrid from './ProductGrid';
import localStorageService from "../../services/localStorageService";
import { green } from '@material-ui/core/colors';
import { connect } from "react-redux";
import PropTypes from "prop-types";
import {
  selectProduct
} from "../../redux/actions/CardsActions";

const useStyles = makeStyles((theme) => ({
  root: {
    // maxWidth: 345,
  },
  media: {
    // height: 490,
    // width: "auto"
  },
  fab: {
    // position: 'absolute',
    // bottom: theme.spacing(2),
    // right: theme.spacing(2),
  },
  fabGreen: {
    color: theme.palette.common.white,
    backgroundColor: green[500],
    // '&:hover': {
    //   backgroundColor: green[600],
    // },
  }
}));

const ZCard = props => {
  const {
    LinkCardClienteGuid,
    Referencia,
    Nome,
    DescricaoPrimaria,
    Imagens,
    MarcaNome,
    ColecaoNome,
    TamanhoCorList,
    Preco
  } = props.prod;

  //const selectProduct = props.selectProduct;
  const classes = useStyles();
  const lensValues = Imagens.map((img) => 'disabled');
  // const lensValues = [];
  // if (Imagens && Imagens.length > 0)  {
  //   lensValues = Imagens.map((img) => 'disabled');
  // }
  if (lensValues.length > 0) {
    lensValues[0] = 'primary';
  }

  const [status, setStatus] = useState('Neutro');
  const [checked, setChecked] = React.useState(true);
  const [imageIndex, setImageIndex] = useState(0);
  const [lens, setLens] = useState(lensValues);
  const [girarCarrosel, setGirarCarrosel] = useState(false);
  const [zoom, setZoom] = useState(false);
  const [cartColor, setCartColor] = useState('primary');
  //const zoomProps = { width: 250, height: 400, zoomWidth: 'default', img: `${process.env.PUBLIC_URL}/assets/${Imagens[imageIndex].Path}`, zoomPosition: 'original'};
  const zoomProps = {
    img: `${process.env.PUBLIC_URL}/assets/${Imagens[imageIndex].Path}`,
    zoomPosition: 'original'
    //onClick: event => setZoom(true)
  };

  const setStatusToNeutro = () => {
    setStatus('Neutro');
    //setCartColor('primary'); 
  };

  const setStatusToPedido = () => {
    if (props.isCart) {
      setStatus('Pedido');
      setCartColor('action');
    }
  };

  const toggleCheck = (event) => {
    event.preventDefault();
    // if (event.target.checked) {
    //   document.getElementById(`add-${event.target.id}`).fade
    // }

    props.selectProduct(event.target.checked, TamanhoCorList);
    setChecked(event.target.checked);
  };

  const setCarousel = (index) => {
    if (index != imageIndex) {
      if (index >= Imagens.length) {
        index = 0;
      }
      if (index < 0) {
        index = Imagens.length - 1;
      }
      for (let i = 0; i < Imagens.length; i++) {
        if (i == index) {
          lensValues[i] = 'primary';
        }
        else {
          lensValues[i] = 'disabled';
        }
      }
      setImageIndex(index);
      setLens(lensValues);
    }
  };

  useEffect(() => {
    if (girarCarrosel) {
      setTimeout(() => {

        if (girarCarrosel) {
          setCarousel(imageIndex + 1);
        }
      }, props.periodo);
    }
  }, [imageIndex]);

  useEffect(() => {
    let subPedido = localStorageService.getItem(`SUB_PEDIDO_${LinkCardClienteGuid}_${Referencia}`);
    if (subPedido != null) {
      setStatusToPedido();
    }
  }, []);

  return (

    <li>
      <Card className={classes.root}>
        {
          !props.isCart &&
          <label htmlFor={props.prod.Referencia} className="product-add">
            <Checkbox
              checked={checked}
              size='small'
              id={props.prod.Referencia}
              onChange={toggleCheck}
              inputProps={{ 'aria-label': 'primary checkbox' }}
            />
            {
              checked &&
              <i 
                //id={`add-${props.prod.Referencia}`}
                className="material-icons product-add-icon-done"
              >
                done
              </i>
            }
            {
              !checked &&
              <i 
                //id={`add-${props.prod.Referencia}`}
                className="material-icons product-add-icon-shop"
              >
                add
              </i>
            }
            <span className="product-add-label">
              Adicionar produto
            </span>
          </label>

        }
        {
          props.isCart && status === 'Pedido' &&
          <i 
            //id={`add-${props.prod.Referencia}`}
            className="material-icons product-add-icon-done"
          >
            done
          </i>
        }
        <CardActionArea>

          {
            Imagens.length > 0 && zoom &&
            <ReactImageZoom className="product-picture" {...zoomProps} />
          }
          {
            Imagens.length > 0 && !zoom &&
            <div className="product-picture">
              <CardMedia

                key={Imagens[imageIndex].Nome}
                className={classes.media}
                image={`${process.env.PUBLIC_URL}/assets/${Imagens[imageIndex].Path}`}
                onClick={(event) => {
                  event.preventDefault();
                  setZoom(true);
                  setGirarCarrosel(false);
                }}
              />
              <div className="picture-nav">
                <div className="picture-navigate picture-navigate-before"
                  onClick={(event) => { event.preventDefault(); setCarousel(imageIndex - 1); }}>
                  <span className="material-icons">
                    navigate_before
                  </span>
                </div>
                <div className="picture-navigate picture-navigate-next"
                  onClick={(event) => { event.preventDefault(); setCarousel(imageIndex + 1); }}>
                  <span className="material-icons">
                    navigate_next
                  </span>
                </div>
              </div>
            </div>
          }
          <div className="product-carousel">
            {Imagens.length > 1 &&
              Imagens.map((img, idx) => (
                <Lens key={`${Referencia}_${idx}`} color={lens[idx]} onClick={(event) => {
                  event.preventDefault();
                  setGirarCarrosel(false);
                  setCarousel(idx);
                }}
                />
              ))
            }
            {Imagens.length > 0 && !zoom &&
              <ZoomIn key='zoom-in' onClick={(event) => {
                event.preventDefault();
                setZoom(true);
                setGirarCarrosel(false);
              }}
              />
            }
            {Imagens.length > 0 && zoom &&
              <ZoomOut key='zoom-out' onClick={(event) => {
                event.preventDefault();
                setZoom(false);
              }}
              />
            }
          </div>
          <CardContent className="product-description">
            <Typography className="product-name" gutterBottom variant="h5" component="h2">
              {Nome} {MarcaNome}
            </Typography>

            <Typography className="product-description" component="p">
              {DescricaoPrimaria} {MarcaNome}
            </Typography>

            <Typography className="product-company" component="p">
              {DescricaoPrimaria}
            </Typography>

            {
              Preco &&
              <Typography className="product-price" gutterBottom variant="h5" component="h2">
                R$ {Preco.toLocaleString('pt-BR', { currency: 'BRL', minimumFractionDigits: 2, maximumFractionDigits: 2 })}
                {/* <NumberFormat value={Preco} displayType={'text'} decimalSeparator={','} thousandSeparator={'.'} prefix={'R$ '} /> */}
              </Typography>
            }
          </CardContent>
        </CardActionArea>
        <CardActions>
          {
            //status == 'Pedido' &&
            // <Fab aria-label='Salvar' className={classes.fab} color='primary'>
            //   <SaveIcon />
            // </Fab>            
          }
          {
            status != 'Editando' && props.isCart &&
            <div className="button-buy">
              <Button onClick={(event) => { event.preventDefault(); setStatus('Editando'); }}>
                <ShoppingCartIcon />
                <span>Comprar</span>
              </Button>
            </div>
          }
          {
            (status == 'Editando' || !props.isCart) &&
            <div className="product-options">
              <ProductGrid
                referencia={Referencia}
                linkCardClienteGuid={LinkCardClienteGuid}
                tamanhoCorList={TamanhoCorList}
                isCart={props.isCart}
                setStatusToNeutro={setStatusToNeutro}
                setStatusToPedido={setStatusToPedido}
                //setCartColor = {setCartColor}
                autofocus
              >
              </ProductGrid>
              {/* <Button size="small" color="primary" onClick= { (event) => {event.preventDefault(); setStatus('Neutro');}}>
                  <AddShoppingCartIcon />
                  Adicionar
                </Button> */}
            </div>
          }
        </CardActions>
      </Card>


    </li>
  );

}

const mapStateToProps = store => ({
  selectProduct: PropTypes.func.isRequired
});

export default withStyles(
  {},
  { withTheme: true }
)(connect(mapStateToProps, { selectProduct })(ZCard));
