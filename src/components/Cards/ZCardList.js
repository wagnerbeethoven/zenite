import React, { useEffect, useState } from 'react';
import ZCard from './ZCard';
import { Checkbox, GridList, GridListTile, ListSubheader } from '@material-ui/core';
import restService from '../../services/linkcards/cardsRest';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
    overflow: 'hidden',
    backgroundColor: theme.palette.background.paper,
  },
  gridList: {
    width: 1080,
    height: 1200,
  },
  icon: {
    color: 'rgba(255, 255, 255, 0.54)',
  },
}));

export default function ZCardList(props) {  
  const classes = useStyles();
  return  (
    // <div  className={classes.root}>
    <div className="product-list">
      <GridList cellHeight={220} className={classes.gridList}>
        {/* <GridListTile key="Subheader" cols={props.cols} style={{ height: 'auto' }}>
          <ListSubheader component="div">
            <Checkbox
              checked={true}
              size='small'
              //onChange={toggleCheck}
              inputProps={{ 'aria-label': 'primary checkbox' }}
            />              
            {props.cards.length > 0 ? `Coleção ${props.cards[0].ColecaoNome}` : 'Nova coleção'}
          </ListSubheader>
        </GridListTile>                                                                                                                                                                                                                                                          */}
        {props.cards.map((elem) => (
          <ZCard 
            className="product-picture"
            key = {elem.Referencia}
            prod = {elem}
            isCart = {props.isCart}
            periodo = {props.periodo}
          />
        ))} 
      </GridList>
    </div>
  );
}
  