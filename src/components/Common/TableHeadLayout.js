import React, { useEffect, useState } from 'react';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TableCell from '@material-ui/core/TableCell';


export default function TableHeadLayout(props) {
    const {titles, masterHead, hasCheck, hasModal} = props;
    const [objKeys, setObjKeys] = useState([]);

    function getVarKeys(){
        let objVarNames = [];
        Object.keys(titles).map((columnName) =>{
            objVarNames.push(titles[columnName]);
        })
        setObjKeys(objVarNames);
    }

    useEffect(()=>{
        getVarKeys();
    }, [])
    return (
        <TableHead>
            <TableRow>
                {
                    hasCheck &&
                    <TableCell />
                }
                {
                    masterHead &&
                    <TableCell />
                }
                {
                    objKeys.map((objKey) =>(
                        <TableCell key={objKey}><strong>{objKey}</strong> </TableCell>
                    ))
                }
                {
                    hasModal &&
                    <TableCell />
                }
                
            </TableRow>
        </TableHead>
    )
}