import React from 'react';
import Table from '@material-ui/core/Table';
import TableContainer from '@material-ui/core/TableContainer';
import TableBody from '@material-ui/core/TableBody';
import TableFooter from '@material-ui/core/TableFooter';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import HeadLayout from './TableHeadLayout.js'
import RowLayout from './TableRowLayout.js'
import { useState } from 'react';

export default function TableLayout(props) {
    const {titles, visibilityCols, rows, hasCheck, page, rowsPerPage, hasDetail, hasModal, hasFooter, deleteFunction, actionFunction } = props;
    
    return (
        <TableContainer component={Paper}>
            <Table aria-label="collapsible table">
                <HeadLayout 
                    titles={titles} 
                    masterHead={hasDetail} 
                    hasCheck={hasCheck}
                    hasModal={hasModal}
                />
                <TableBody>
                    {rows.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map((row) => {
                        let checked = props.verify !== undefined? props.verify(row.Key): false;
                        
                        return(
                            <RowLayout 
                                key={row.Key} 
                                row={row} 
                                visibilityCols={visibilityCols}
                                deleteFunction = {deleteFunction}
                                hasDetail={hasDetail} 
                                hasCheck={hasCheck}
                                hasModal={hasModal}
                                isChecked={checked}
                                handleClick={props.handleClick}
                                handleOpenModal={props.handleOpenModal}
                                actionFunction={actionFunction}
                            />
                        )
                    })}
                </TableBody>
                {
                    hasFooter &&
                    <TableFooter>
                        <TableRow>
                            <TablePagination
                                rowsPerPageOptions={[5, 10, 25]}
                                colSpan={2}
                                count={rows.length}
                                rowsPerPage={rowsPerPage}
                                page={page}
                                SelectProps={{
                                    inputProps: { 'aria-label': 'rows per page' },
                                    native: true,
                                }}
                                onChangePage={props.changePage}
                                onChangeRowsPerPage={props.changeRowsPerPage}
                            />
                        </TableRow>
                    </TableFooter>
                }
                
            </Table>
        </TableContainer>
    )
}