import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import restService from '../../services/linkcards/cardsRest';
import TableRow from '@material-ui/core/TableRow';
import { DataGrid } from '@material-ui/data-grid';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import Box from '@material-ui/core/Box';
import { AppBar, Tab, Table, Tabs } from '@material-ui/core';
import Collapse from '@material-ui/core/Collapse';
import IconButton from '@material-ui/core/IconButton';
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp';
import DeleteIcon from '@material-ui/icons/Delete';
import CheckIcon from '@material-ui/icons/Check'; 
import ErrorIcon from '@material-ui/icons/Error';
import AddIcon from '@material-ui/icons/Add';
import PlayIcon from '@material-ui/icons/PlayArrow'
import Modal from '@material-ui/core/Modal';

import Typography from '@material-ui/core/Typography';
import Checkbox from '@material-ui/core/Checkbox';
import TableHeadLayout from './TableHeadLayout.js'

const useRowStyles = makeStyles((theme)=>({
    
    root: {
        '& > *': {
        borderBottom: 'unset',
        },
    }
}));

const useStyles = makeStyles((theme) => ({
    root: {
      flexGrow: 1,
      backgroundColor: theme.palette.background.paper,
    },
  }));

function TabPanel(props) {
    const { children, value, index, ...other } = props;
  
    return (
      <div
        role="tabpanel"
        hidden={value !== index}
        id={`simple-tabpanel-${index}`}
        aria-labelledby={`simple-tab-${index}`}
        {...other}
      >
        {value === index && (
          <Box p={3}>
            <Typography>{children}</Typography>
          </Box>
        )}
      </div>
    );
  }
  
  TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.any.isRequired,
    value: PropTypes.any.isRequired,
  };

function tabProps(index) {
    return {
        id: `simple-tab-${index}`,
        'aria-controls': `simple-tabpanel-${index}`,
    };
}

export default function TableRowLayout(props) {
    
    const {row, visibilityCols, hasDetail, hasCheck, isChecked, deleteFunction, hasModal, actionFunction } = props;
    const [detailOpen, setDetailOpen] = React.useState(false);
    const [detailSet, setDetailSet] = React.useState([]);
    const classes = useRowStyles();
    const classes2 = useStyles();
    const [tab, setTab] = React.useState(0);

    const handleTabChange = (event, newValue) => {
      setTab(newValue);
    };

    const toggleDetail = async (event) => {
      event.preventDefault();
      
      if (!detailOpen) {
        let clientesData = await restService.getClientesCard(row.Key);
      
        console.log(clientesData);
        setDetailSet(Array.from(clientesData));
        console.log(event);
      }
      setDetailOpen(!detailOpen);
    }

    return (
      <>
        <TableRow 
            hover
            key={row.Key}
            className={classes.root}
            >
            {
              hasCheck &&
                <TableCell>
                    <Checkbox
                        //key={row.Key}
                        onClick={(event) => props.handleClick(event, row.Key)}
                        checked={isChecked}
                        color="primary"
                        inputProps={{ 'aria-label': 'secondary checkbox' }}
                    />
                </TableCell>
            }
            {
                hasDetail &&
                <TableCell>
                    <IconButton aria-label="expand row" size="small" onClick={(event) => toggleDetail(event)}>
                        {detailOpen ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
                    </IconButton>
                </TableCell>
            }
            {
                Object.keys(row).map((column)=>{
                  //
                  if(visibilityCols[column]){
                        return (
                            props.handleClick !== undefined?
                            <TableCell onClick={(event) => props.handleClick(event, row)} align="left">{row[column]}</TableCell>
                            :
                            <TableCell align="left">{row[column]}</TableCell>)
                            // <TableCell onClick={(event) => props.handleClick(event, row.Key)} key={row[key]} align="left">{row[key]}</TableCell>
                            // :
                            // <TableCell key={row[key]} align="left">{row[key]}</TableCell>)
                    }
                })
            }
            {
                deleteFunction &&
                <TableCell>
                    <IconButton aria-label="expand row" color='primary' alt='Excluir item' size="small" onClick={(args) => deleteFunction(row.Key)}>
                        <DeleteIcon />
                    </IconButton>
                </TableCell>
            }
            {
                actionFunction &&
                <TableCell>
                    <IconButton aria-label="expand row" color='primary' label='Conferencia Cega' size="small" onClick={(args) => actionFunction(row.Key)}>
                        <PlayIcon />
                    </IconButton>
                </TableCell>
            }
            {
              row['status'] == 'OK' &&
                <TableCell>
                    <CheckIcon />
                </TableCell>
            }
            {
              row['status'] == 'ERROR' &&
                <TableCell>
                    <ErrorIcon color='primary' />
                </TableCell>
            }
            {
                hasModal &&
                <TableCell>
                    <IconButton className="addButton" disabled={!isChecked} aria-label="expand row" size="small" onClick={() => props.handleOpenModal(row.Key, row.NumPedido)} >
                        <div className="btnContent">
                            <AddIcon></AddIcon><span> Nota Fiscal</span>
                        </div>
                    </IconButton>
                </TableCell>
            }
          
        </TableRow>
        {
          detailSet.length > 0 &&
          <TableRow
            key={row.Key}

          >
            <TableCell style={{ paddingBottom: 0, paddingTop: 0 }} colSpan={6}>
              <Collapse in={detailOpen} timeout="auto" unmountOnExit>
                  <Box margin={1}>
                      <Table size="small" aria-label="purchases">
                      <TableHeadLayout
                        titles = {{ Key: 'Código', ClienteNome:'Nome',
                          WaNumber: 'Número', Status: 'Status',
                          DhEnvio: 'Enviado', DeliveryEvent: 'Entregue', 
                          ReceivedEvent: 'Recebido', ReadEvent: 'Lido' 
                        }}
                      />
                      <TableBody>
                          {detailSet.map((detail) => (
                              <TableRowLayout 
                                  key={detail.Key} 
                                  row={detail} 
                                  hasDetail={false}
                                  visibilityCols = {{ Key: true, ClienteNome:true,
                                    WaNumber: true, Status: true,
                                    DhEnvio: true, DeliveryEvent: true, 
                                    ReceivedEvent: true, ReadEvent: true,
                                    IdLinkCard: true
                                  }}
                              />
                          ))}
                      </TableBody>
                      </Table>
                    </Box>
                </Collapse>
            </TableCell>
          </TableRow>
        }
      </>
    );
  }