import React, { useEffect, useState } from 'react';
import ReactDOM from 'react-dom'
import PropTypes from "prop-types";
import { Alert } from '@material-ui/lab';
import { connect } from "react-redux";
import { Store } from "../../redux/Store";
import { Provider } from "react-redux";
// import {
//     closeAlert
// } from "../../redux/actions/AlertActions";
  
const AlertLinkCards = props => {
    const {
        alert,
        //closeAlert
      } = props;

    // const [show, setShow] = React.useState(alert.showAlert);

    // useEffect(() =>   {
    //     
    //     setShow(alert.showAlert);
    // },
    //   [alert.showAlert]);

    const closeAlert = (id)  =>   {
        ReactDOM.render(<></>, document.getElementById(id));
    }
    
    return  (
        //show &&
        // <div tabIndex='-1' ref={alertRef}>
        <Provider store={Store}>
            <Alert
                onClose={ () => { 
                    closeAlert(props.parentId);
                } }
                severity={alert.severity}
            >
                {alert.text}
            </Alert>
        </Provider>
    );

}

const mapStateToProps = store => ({
    alert: store.alert,
    //closeAlert: PropTypes.func.isRequired,
  });
  
export default (connect(mapStateToProps, null)(AlertLinkCards));
  