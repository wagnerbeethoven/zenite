import React from 'react';
import AppContext from "../../appContext";
import { Store } from "../../redux/Store";
import AuthGuard from "../../auth/AuthGuard";
import Auth from "../../auth/Auth";
import routes from "../../RootRoutes";
import { Router } from "../../../node_modules/react-router-dom";
import { Provider } from "react-redux";
import ReactDOM from 'react-dom'
import { AppBar, Button, Dialog, Divider, Icon, IconButton, List, ListItem, ListItemText, Slide, Toolbar, Tooltip, Typography } from '@material-ui/core';
import DataGrid, { Column } from 'devextreme-react/data-grid';
import ArrayStore from 'devextreme/data/array_store';
import DataSource from 'devextreme/data/data_source';
import restService from '../../services/linkcards/cardsRest';
import CloseIcon from '@material-ui/icons/Close';
import LojaVirtual from '../../views/Cards/LojaVirtual';
import history from "../../history.js";
import { SignalCellular0Bar, SignalCellular1Bar, SignalCellular2Bar, SignalCellular3Bar, SignalCellular4Bar } from "@material-ui/icons"
import ShoppingCarOutlinedIcon from '@material-ui/icons/ShoppingCartOutlined';
import Send from '@material-ui/icons/Send';

const Transition = React.forwardRef(function Transition(props, ref) {
    return <Slide direction="up" ref={ref} {...props} />;
  });

const container = (guid) => {
    return (
      <AppContext.Provider value={{ routes }}>
        <Provider store={Store}>
            {/* <Auth> */}
              <Router history={history}>
                <AuthGuard>
                    <LojaVirtual
                        guid={guid}
                    />
                </AuthGuard>
              </Router>
            {/* </Auth> */}
        </Provider>
      </AppContext.Provider>
    );
}

class DataGridDetailDevExpr extends React.Component {
  constructor(props) {
    super(props);
    
    console.log(props);
    this.state = {
        dataSource: new DataSource({
            store: new ArrayStore({
                data: [],
                key: 'Key'
            })//,
        //filter: ['EmployeeID', '=', key]
        }),
        masterData: props.data.data,
        data: [],
        openDialog: false,
        openStatus: false
    };
    this.getDetailAsync(props.data.key);
  }

  getDetailAsync = async (id)   =>  {
    let clientesData = await restService.getClientesCard(id);
    this.setState({ 
        ...this.state,
        dataSource: new DataSource({
            store: new ArrayStore({
                data: Array.from(clientesData),
                key: 'Key'
            })
        }),
        data: Array.from(clientesData),
        openDialog: false
    });
};

renderCellCodigo = (metaData) => {
    let key = metaData.data[metaData.column.dataField];
    return  (

      <Tooltip  title={`Abrir Link Cards - ${key}`}>
        <IconButton
          onClick={(event => {
              
              this.setState({
                  ...this.state,
                  openDialog: true
              });
              setTimeout(() => {
                  ReactDOM.render(container(key), document.getElementById('lojaVirtualContainer'));
              }, 60);
              console.log(this.state);
          })}
        >
          <Icon><ShoppingCarOutlinedIcon/></Icon>
        </IconButton>                
      </Tooltip>



      // <Button
      //   title={key}
      //   onClick={(event => {
      //       
      //       this.setState({
      //           ...this.state,
      //           openDialog: true
      //       });
      //       setTimeout(() => {
      //           ReactDOM.render(container(key), document.getElementById('lojaVirtualContainer'));
      //       }, 60);
      //       console.log(this.state);
      //   })}
      // >
      //     {key}
      // </Button>  
    );
 };

 renderCellNome = (metaData) => {
  let nome = metaData.data[metaData.column.dataField];
  let phone = metaData.data['WaNumber'];
  return  (
    <Tooltip title={phone}>
      <Typography>
        {nome}
      </Typography>
    </Tooltip>
  );
};
 
    renderCellProgress = (metaData) => {
      let status = metaData.data['Status'];
      let key = metaData.data['Key'];
      let phone = metaData.data['WaNumber'];
      let textMsg = this.state.masterData.Texto.replace('~{Cliente.Nome}', metaData.data['ClienteNome']);
      let statusCaption = '';
      let bar = 0;
      let icon = <></>;
      if (metaData.data['ProdutosCompradosCount'] > 0)  {
        bar = 4;
        statusCaption = 'Parabéns!! Cliente positivado.'
        icon = (
          <Icon color='action'>
            <SignalCellular4Bar />
          </Icon>
        );
      }
      else  {
        switch(status)  {
          case 'WaitingConfirm':
            bar = 0;
            icon = (
              <Icon color='disabled'>
                <SignalCellular0Bar />
              </Icon>
            );
            statusCaption = 'Enviada para o WA...'
            break;
          case 'Delivery':
            bar = 1;
            statusCaption = 'Recebida pelo WA...'
            icon = (
              <Icon color='inherit'>
                <SignalCellular1Bar />
              </Icon>
            );
            break;
          case 'RECEIVED':
            bar = 2;
            statusCaption = 'Recebida pelo cliente...'
            icon = (
              <Icon color='secondary'>
                <SignalCellular2Bar />
              </Icon>
            );
            break;
          case 'READ':
            bar = 3;
            statusCaption = 'Lida pelo cliente...'
            icon = (
              <Icon color='primary'>
                <SignalCellular3Bar />
              </Icon>
            );
            break;
          default:
            bar = 0;
            icon = (
              <Icon color='default'>
                <SignalCellular0Bar />
              </Icon>
            );
          }
        }

      return  (
        <div>
          <Tooltip title={statusCaption}>
            {icon}
          </Tooltip>
          <Tooltip  title={`Reenviar Link Cards - ${key}`}>
            <IconButton
              onClick={(event => {
                  event.preventDefault();
                  window.open(`https://api.whatsapp.com/send?phone=${phone}&text=${textMsg}  \n\nzenite.inf.br:33018/wa?guid=${key}`);
              })}
            >
              <Icon
                fontSize='small'
              ><Send/></Icon>
            </IconButton>                
          </Tooltip>
        </div>
      );
  
    // return (
    //     <a href={`http://zenite.inf.br:33018/wa?guid=${key}`}>
    //         {key}
    //     </a>
    // )
};

handleCloseDialog = () => {
    this.setState({
        ...this.state,
        openDialog: false
    });
};

  render() {
    //let { Id, Nome } = this.props.data.data;
    return (
      <React.Fragment>
        {/* <div className="master-detail-caption">
          {`LinkCards ${Id} - ${Nome}`}
        </div> */}
        <DataGrid
          dataSource={this.state.dataSource}
          keyExpr="Key"
          showBorders={true}
          columnAutoWidth={true}
          noDataText=''
        >
          <Column dataField="Status" caption="" cellRender={this.renderCellProgress} />
          <Column type="buttons" width={110}>
            <Button name="" />
            <Button name="delete" />
            <Button hint="Clone" icon="repeat" visible={this.isCloneIconVisible} onClick={this.cloneIconClick} />
          </Column>
          <Column dataField="ClienteNome" caption="Cliente" width={120} cellRender={this.renderCellNome}/>
          {/* <Column dataField="WaNumber" caption="WA"/> */}
          <Column dataField="ProdutosCompradosCount"  caption="Itens" />
          <Column dataField="TotalPedidos" caption="Valor" />
          <Column dataField="Key" caption="Código" cellRender={this.renderCellCodigo}/>
          <Column dataField="DhEnvio" width={120} dataType="datetime" format="dd/MM/yy HH:mm" caption="Enviada"/>
          <Column dataField="DeliveryEvent" width={120} dataType="datetime" format="dd/MM/yy HH:mm" caption="Entregue" />
          <Column dataField="ReceivedEvent" width={120} dataType="datetime" format="dd/MM/yy HH:mm" caption="Recebida" />
          <Column dataField="ReadEvent" width={120} dataType="datetime" format="dd/MM/yy HH:mm" caption="Lida" />
          {/* <Column
            caption="Completed"
            dataType="boolean"
            calculateCellValue={this.completedValue}
          /> */}
        </DataGrid>
        <Dialog fullScreen open={this.state.openDialog} onClose={this.handleCloseDialog} TransitionComponent={Transition}>
            <AppBar>
            <Toolbar>
                <IconButton edge="start" color="inherit" onClick={this.handleCloseDialog} aria-label="close">
                <CloseIcon />
                </IconButton>
                <Typography variant="h6">
                {/* {this.state.openDialog ? this.state.dataSource.data['Key'] : ''} */}
                    {this.state.data.Key}
                </Typography>
                {/* <Button autoFocus color="inherit" onClick={this.handleCloseDialog}>
                save
                </Button> */}
            </Toolbar>
            </AppBar>
            <div id='lojaVirtualContainer'>
                {/* {this.state.openDialog && <LojaVirtual guid={this.state.data.Key} />} */}
            </div>
        </Dialog>
      </React.Fragment>
    );
  }
//   completedValue(rowData) {
//     return rowData.Status === 'Completed';
//   }
}

export { DataGridDetailDevExpr };
