import { ProductCart } from '../cards/ProductCart'

class Pedido    {
    public Key : number;
    public DhCreate : Date;
    public NumeroPedidoErp : string;
    public ValorTotal : number;
    public Desconto : number;
    public IsNew : boolean;
    public LinkCardClienteKey: string;
    public IndConsignado: boolean;
    public PedidoProdutoDtoSet : ProductCart[];

    constructor(key: number, dhCreate: Date, numPedErp: string, valTotal: number, valDesc : number, isNew : boolean, guid: string, indConsignado: boolean) {
        this.Key = key;
        this.DhCreate = dhCreate;
        this.NumeroPedidoErp = numPedErp;
        this.ValorTotal = valTotal;
        this.Desconto = valDesc;
        this.IsNew = isNew;
        this.LinkCardClienteKey = guid;
        this.IndConsignado = indConsignado;
        this.PedidoProdutoDtoSet = [];
    }

}

export { Pedido }