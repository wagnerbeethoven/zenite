import { LinkCardProdutoDto } from './LinkCardProdutoDto';
import { LinkCardClienteWriteDto } from './LinkCardClienteWriteDto';
import { LinkCardMensagemDto } from './LinkCardMensagemDto';

class LinkCardsDto   {
    public Key : number;
    public IsNew : boolean;
    public Nome : string;
    public Titulo : string;
    public DtInicioVigencia : Date | null;
    public DtFinalVigencia : Date | null;
    public DhAgendamento : Date | null;
    public PipelineWidth : number;
    public PipelinePeriod : number;
    public ViaApi : boolean;

    public LinkCardProdutoDtoSet : LinkCardProdutoDto[];
    public LinkCardClienteWriteDtoSet : LinkCardClienteWriteDto[];
    public LinkCardMensagemDtoSet : LinkCardMensagemDto[];

    constructor(key: number, isNew: boolean, nome: string, titulo : string, 
        dtIniVig : Date | null, 
        dtFinalVig : Date | null, 
        dhAgenda : Date | null,
        pipelineWidth : number,
        pipelinePeriod : number,
        viaApi : boolean)
    {
        this.Key = key;
        this.IsNew = isNew;
        this.Nome = nome;
        this.Titulo = titulo;
        this.DtInicioVigencia = dtIniVig;
        this.DtFinalVigencia = dtFinalVig;
        this.DhAgendamento = dhAgenda;
        this.PipelineWidth = pipelineWidth;
        this.PipelinePeriod = pipelinePeriod;
        this.ViaApi = viaApi;
        this.LinkCardProdutoDtoSet = [];
        this.LinkCardClienteWriteDtoSet = [];
        this.LinkCardMensagemDtoSet = [];
    }

}

export { LinkCardsDto }