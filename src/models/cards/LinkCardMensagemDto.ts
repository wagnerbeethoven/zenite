class LinkCardMensagemKey   {
    public Index : number;
    public IdLinkCard : number;

    constructor(index: number, idLink : number) {
        this.Index = index;
        this.IdLinkCard = idLink;
    }
}


class LinkCardMensagemDto   {
    public Key : LinkCardMensagemKey;
    public IsNew : boolean;
    public Index : number;
    public Texto : string;

    constructor (key : LinkCardMensagemKey, isNew : boolean, texto : string) {
        this.Key = key;
        this.IsNew = isNew;
        this.Index = key.Index;
        this.Texto = texto;
    }

}


export { LinkCardMensagemKey, LinkCardMensagemDto }