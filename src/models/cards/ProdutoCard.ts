import { Product } from '../product/Product'

class ProdutoTamanhoCor {
    public Id : number;
    public Tamanho : string;
    public CorNome : string;
    public CorRgb : string;
    public Qtde : number;
    public ValUnit : number;
    public Desconto : number;


    constructor(
        id : number,
        tamanho : string,
        corNome : string,
        corRgb : string,
        qtde : number,
        valUnit: number,
        desconto: number)
    {
        this.Id = id;
        this.Tamanho = tamanho;
        this.CorNome = corNome;
        this.CorRgb = corRgb;
        this.Qtde = qtde;
        this.ValUnit = valUnit;
        this.Desconto = desconto;
    }
}

class ProdutoImagem 
{
    public Nome : string;
    public Path : string;
    public Principal : boolean;

    constructor(nome : string, path: string, principal: boolean)   {
        this.Nome = nome;
        this.Path = path;
        this.Principal = principal;
    }
}

class ProdutoCard extends Product<string> {
    public LinkCardClienteGuid: string;
    public PrecoOriginal : number;
    public Preco : number;
    public ImagensPath : string[];
    public TamanhoCorList : ProdutoTamanhoCor[];

    constructor(referencia : string, 
        nome: string, 
        descricaoPrimaria : string, 
        descricaoSecundaria : string, 
        marcaNome : string,
        colecaoNome : string, 
        precoOriginal : number,
        preco : number,
        guid: string)   
    {   
        super(referencia, referencia, nome, descricaoPrimaria, descricaoSecundaria, marcaNome, colecaoNome);
        this.PrecoOriginal = precoOriginal;
        this.Preco = preco;
        this.LinkCardClienteGuid = guid;
        this.ImagensPath = [];
        this.TamanhoCorList = [];
    }
}

export { ProdutoCard, ProdutoTamanhoCor, ProdutoImagem }