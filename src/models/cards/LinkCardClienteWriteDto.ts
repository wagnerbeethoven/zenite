import { read } from "fs";

class LinkCardClienteWriteDto    {
    public Key : string;
    public IsNew : boolean;
    public IndexMsg : number;
    public ClienteKey : number;
    public VendedorKey : number;

    constructor(key: string, clienteKey : number, isNew: boolean, 
        vendedorKey: number, indexMsg : number)  {
        this.Key = key;
        this.ClienteKey = clienteKey;
        this.IsNew = isNew;
        this.VendedorKey = vendedorKey;
        this.IndexMsg = indexMsg;
    }
}

export { LinkCardClienteWriteDto }