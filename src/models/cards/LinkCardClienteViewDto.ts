import { read } from "fs";

class LinkCardClienteViewDto    {
    public Key : string;
    public IsNew : boolean;
    public IdLinkCard : number;
    public Status : string;

    public IndexMsg : number;
    public ClienteKey : number;
    
    public ClienteNome : string;
    
    public VendedorKey : number;
    public WaNumber : string;
    public MercadoConsumidorNome : string;
    public DhEnvio : Date | null;
    public DeliveryEvent : Date | null;
    public ReceivedEvent  : Date | null;
    public ReadEvent : Date | null;

    public ProdutosCompradosCount : number;
    public TotalPedidos : number;

    constructor(key: string, clienteKey : number, isNew: boolean, 
        idLinkCard : number, status : string,
        clienteNome: string, vendedorKey: number, 
        indexMsg : number, waNumber : string,
        mercadoConsumidorNome : string, dhEnvio : Date | null,
        deliveryEvent : Date | null, receivedEvent  : Date | null, readEvent : Date | null,
        produtosCompradosCount : number, totalPedidos : number)  {
        this.Key = key;
        this.ClienteKey = clienteKey;
        this.IsNew = isNew;
        this.ClienteNome = clienteNome;
        this.VendedorKey = vendedorKey;
        this.IndexMsg = indexMsg;

        this.IdLinkCard = idLinkCard;
        this.Status = status;
        this.WaNumber = waNumber;
        this.MercadoConsumidorNome = mercadoConsumidorNome;
        this.DhEnvio = dhEnvio;
        this.DeliveryEvent = deliveryEvent;
        this.ReceivedEvent = receivedEvent;
        this.ReadEvent = readEvent;
        this.ProdutosCompradosCount = produtosCompradosCount;
        this.TotalPedidos = totalPedidos;
    }
}

export { LinkCardClienteViewDto }