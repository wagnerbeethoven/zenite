class LinkCardsCabecalhoVendedorDto {
    public Id : number;
    public Nome : string;
    public Titulo : string;
    public DtInicioVigencia : Date;
    public DtInicioVigenciaStr : string;
    public DtFinalVigencia : Date | null;
    public DtFinalVigenciaStr : string;
    public DhAgendamento : Date | null;
    public Index : number;
    public Texto : string;
    public ClientesCount : number;
    public ProdutosCount : number;
    public PedidosCount : number;
    public ValorTotalPedidos : number;
    // public TempoMedioParaLer : string;
    // public TempoMedioParaReceber : string;

    constructor (id : number,
        nome : string,
        titulo : string,
        dtInicioVigencia : Date,
        dtFinalVigencia : Date | null,
        dhAgendamento : Date | null,
        index : number,
        texto : string,
        clientesCount : number,
        produtosCount : number,
        pedidosCount : number,
        valorTotalPedidos : number)
        // tempoMedioParaLer : string,
        // tempoMedioParaReceber : string)
    {
        this.Id = id;
        this.Nome = nome;
        this.Titulo = titulo;
        this.DtInicioVigencia = dtInicioVigencia;
        this.DtFinalVigencia = dtFinalVigencia;
        this.DhAgendamento = dhAgendamento;
        this.Index = index;
        this.Texto = texto;
        this.ClientesCount = clientesCount;
        this.ProdutosCount = produtosCount;
        this.PedidosCount = pedidosCount;
        this.ValorTotalPedidos = valorTotalPedidos;
        // this.TempoMedioParaLer = tempoMedioParaLer;
        // this.TempoMedioParaReceber = tempoMedioParaReceber;
        this.DtInicioVigenciaStr = new Intl.DateTimeFormat('pt-BR').format(dtInicioVigencia);
        this.DtFinalVigencia = dtFinalVigencia;
        if (dtFinalVigencia)    {
            this.DtFinalVigenciaStr = new Intl.DateTimeFormat('pt-BR').format(dtFinalVigencia);
        }
        else    {
            this.DtFinalVigenciaStr = '';
        }
    }
}

export { LinkCardsCabecalhoVendedorDto }