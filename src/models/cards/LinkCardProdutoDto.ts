class LinkCardProdutoKey {
    public IdProduto : number;
    public IdLinkCard : number;

    constructor (idProd: number, idLinkCard : number)   {
        this.IdProduto = idProd;
        this.IdLinkCard = idLinkCard;
    }
}


class LinkCardProdutoDto    {
    public Key : LinkCardProdutoKey;
    public IsNew : boolean;
    public Nome : string;
    public DescricaoPrimaria : string;
    public DescricaoSecundaria : string;
    public MarcaNome : string;
    public Qtde : number;
    public ValUnit : number;
    public Desconto : number;

    constructor(key : LinkCardProdutoKey,
        isNew : boolean,
        nome: string,
        descricaoPrimaria : string,
        descricaoSecundaria : string,
        marcaNome : string,
        qtde : number,
        valUnit : number,
        desconto : number) 
    {
        this.Key = key;
        this.IsNew = isNew;
        this.Nome = nome;
        this.DescricaoPrimaria = descricaoPrimaria;
        this.DescricaoSecundaria = descricaoSecundaria;
        this.MarcaNome = marcaNome;
        this.Qtde = qtde;
        this.ValUnit = valUnit;
        this.Desconto = desconto;
    }
}

export { LinkCardProdutoDto, LinkCardProdutoKey }