class LinkCardsCabecalhoClienteDto {

    public Id : number;
    public Nome : string;
    public Titulo : string;
    public Index : number;
    public Texto : string;
    public GuidMsg : string;
    public Status : string;
    public WaNumber : string;

    public DhEnvio : Date | null;
    public DeliveryEvent : Date | null;
    public ReceivedEvent  : Date | null;
    public ReadEvent : Date | null;
    public ProdutosCardsCount : number;
    public PedidosCount : number;
    public ProdutosPedidosCount : number;
    public ValorTotalPedidos : number;
    public ValMedioPedidos : number | null;

    constructor (id : number,
        nome : string,
        titulo : string,
        index : number,
        texto : string,
        guid : string,
        status : string,
        waNumber : string,
        dhEnvio : Date | null,
        deliveryEvent : Date | null,
        receivedEvent  : Date | null,
        readEvent : Date | null,
        produtosCardsCount : number,
        pedidosCount : number,
        produtosPedidosCount : number,
        valorTotalPedidos : number,
        valMedioPedidos : number | null)
    {
        this.Id = id;
        this.Nome = nome;
        this.Titulo = titulo;
        this.Index = index;
        this.Texto = texto;
        this.GuidMsg = guid;
        this.Status = status;
        this.WaNumber = waNumber;
        this.DhEnvio = dhEnvio;
        this.DeliveryEvent = deliveryEvent;
        this.ReceivedEvent = receivedEvent;
        this.ReadEvent = readEvent;
        this.ProdutosCardsCount = produtosCardsCount;
        this.ProdutosPedidosCount = produtosPedidosCount;
        this.ValMedioPedidos = valMedioPedidos;
        this.PedidosCount = pedidosCount;
        this.ValorTotalPedidos = valorTotalPedidos;
    }
}

export { LinkCardsCabecalhoClienteDto }