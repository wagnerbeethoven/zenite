import { Product } from '../product/Product'

class PedidoProdutoKey  {
    public IdProduto : number;
    public IdPedido : number;

    constructor(idProd: number, idPed: number)  {
        this.IdProduto = idProd;
        this.IdPedido = idPed;
    }
}

class ProductCart extends Product<PedidoProdutoKey> {
    public Qtde : number;
    public ValUnit : number;

    constructor(key: PedidoProdutoKey,
        referencia : string, 
        nome: string, 
        descricaoPrimaria : string, 
        descricaoSecundaria : string, 
        marcaNome : string,
        colecaoNome : string, 
        qtde : number, 
        valUnit : number)   
    {
        super(key, referencia, nome, descricaoPrimaria, descricaoSecundaria, marcaNome, colecaoNome);
        this.Qtde = qtde;
        this.ValUnit = valUnit;
    }
}

export { ProductCart, PedidoProdutoKey }