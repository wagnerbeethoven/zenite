class HierarquiaGrpSubGrp
{
    public GrpId : number;
    public GrpNome : string
    public SubId : number;
    public SubNome : string;

    constructor(grpId: number, grpNome: string, subId: number, subNome : string)    {
        this.GrpId = grpId;
        this.GrpNome = grpNome;
        this.SubId = subId;
        this.SubNome = subNome;
    }
}

export { HierarquiaGrpSubGrp }
