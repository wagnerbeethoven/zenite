//import { number } from "prop-types";

class ColecaoDto    {
    public Key : number;
    public Nome : string;
    public CodigoErp : string;
    public Tema : string;
    public Semana : number;
    public Mes : number;
    public Ano : number;
    public IndLancamento : boolean;

    constructor(key : number, nome: string, codigoErp : string, tema : string, semana : number, mes : number, ano : number, indLancamento : boolean)  {
        this.Key = key;
        this.Nome = nome;
        this.CodigoErp = codigoErp;
        this.Tema = tema;
        this.Semana = semana;
        this.Mes = mes;
        this.Ano = ano;
        this.IndLancamento = indLancamento;
    }
}

export { ColecaoDto }