class ProdutoParams {
    public MarcasList : number[];
    public TamanhosList : string[];
    public GruposList : number[]
    public SubGruposList : number[]
    public CoresList : string[];
    public ColecoesList : number[]

    constructor ()  {
        this.MarcasList = [];
        this.TamanhosList = [];
        this.GruposList = [];
        this.SubGruposList = [];
        this.CoresList = [];
        this.ColecoesList = [];
    }
}

export { ProdutoParams }