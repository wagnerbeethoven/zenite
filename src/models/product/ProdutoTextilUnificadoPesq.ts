import { Product } from "./Product";
import { ProdutoTamanhoCor } from "../cards/ProdutoCard";

class ProdutoTextilUnificadoPesq extends Product<string>   {
    public GrupoKey : number;
    public GrupoNome: string;
    public SubGrupoKey : number;
    public SubGrupoNome: string;
    public ColecaoKey : number;
    public MarcaKey : number;
    public ImagensPath : string[];
    public TamanhoCorList : ProdutoTamanhoCor[];

    constructor(key: string, referencia : string, 
        nome: string, 
        descricaoPrimaria : string, 
        descricaoSecundaria : string, 
        marcaNome : string,
        colecaoNome : string,
        grupoKey: number,
        grupoNome: string,
        subGrpKey: number,
        subGrpNome: string,
        colecaoKey : number,
        marcaKey: number)   {
            super(referencia, referencia, nome, descricaoPrimaria, descricaoSecundaria, marcaNome, colecaoNome);

            this.GrupoKey = grupoKey;
            this.GrupoNome = grupoNome;
            this.SubGrupoKey= subGrpKey;
            this.SubGrupoNome = subGrpNome;
            this.ColecaoKey =colecaoKey;
            this.MarcaKey = marcaKey;
            this.ImagensPath = [];
            this.TamanhoCorList = [];
            }
}

export { ProdutoTextilUnificadoPesq }