class Product<TKey> {
    public Key : TKey;
    public Referencia : string;
    public Nome : string;
    public DescricaoPrimaria : string;
    public DescricaoSecundaria : string;
    public MarcaNome : string;
    public ColecaoNome : string;

    constructor(key: TKey, referencia : string, 
        nome: string, 
        descricaoPrimaria : string, 
        descricaoSecundaria : string, 
        marcaNome : string,
        colecaoNome : string)   
    {
        this.Key = key;
        this.Referencia = referencia;
        this.Nome = nome;
        this.DescricaoPrimaria = descricaoPrimaria;
        this.DescricaoSecundaria = descricaoSecundaria;
        this.MarcaNome = marcaNome;
        this.ColecaoNome = colecaoNome;
    }

}

export { Product }