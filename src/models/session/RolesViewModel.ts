class RolesViewModel
{
    public RoleId : number;
    public RoleName : string;

    //public IEnumerable<string> Functionalities { get; set; }

    constructor (roleId : number, roleName : string)    {
        this.RoleId = roleId;
        this.RoleName = roleName;
    }
}

export { RolesViewModel }