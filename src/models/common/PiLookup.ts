class PiLookup<T>   {
    public Value : T;
    public Text : string;
    public Group : string;

    constructor(value : T, text: string, group: string) {
        this.Value = value;
        this.Text = text;
        this.Group = group;
    }
}

export { PiLookup }