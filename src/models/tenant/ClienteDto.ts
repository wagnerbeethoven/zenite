class ClienteDto    {
    public CodigoErp : string;
    public Nome : string;
    public WaNumber : string
    public Cpf_Cnpj : string
    public MercadoConsumidorKey : number;
    public MercadoConsumidorNome : string;
    public VendedorKey : number;
    public VendedorNome : string;
    public UsuarioKey : number;
    public UsuarioUserName : string; 

    constructor(codErp : string,
        nome: string,
        waNumber: string,
        cpfCnpj : string,
        mercKey: number,
        mercNome: string,
        vendKey : number,
        vendNome: string,
        usuKey : number,
        usuUserName : string)
    {
        this.CodigoErp = codErp;
        this.Nome = nome;
        this.WaNumber = waNumber;
        this.Cpf_Cnpj = cpfCnpj;
        this.MercadoConsumidorKey = mercKey;
        this.MercadoConsumidorNome = mercNome;
        this.VendedorKey = vendKey;
        this.VendedorNome = vendNome;
        this.UsuarioKey = usuKey;
        this.UsuarioUserName = usuUserName;
    }
}

export { ClienteDto }