class ClienteParams {
    public MercadosList : number[];
    public PartName : string;
    public WaNumberIni : string

    constructor(partName : string, waNumberIni : string)    {
        this.PartName = partName;
        this.WaNumberIni = waNumberIni;
        this.MercadosList = [];
    }

}

export { ClienteParams }