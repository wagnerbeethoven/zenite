import { makeStyles } from '@material-ui/core/styles';
import React, { Component } from "react";
import {
  Button,
  Card,
  Checkbox,
  FormControl,
  FormControlLabel,
  Grid,
  InputLabel,
  Select
} from "@material-ui/core";
import jwtAuthService from "../../services/jwtAuthService";
import { ValidatorForm, TextValidator } from "react-material-ui-form-validator";
import { connect } from "react-redux";
import { authRoles } from "../../auth/authRoles";
import { Alert } from '@material-ui/lab';
//import { authRoles } from "..//auth/authRoles";

class SignUp extends Component {
  // state = {
  //   username: "",
  //   email: "",
  //   password: "",
  //   roleId: "",
  //   agreement: "",
  //   rolesOptions: []
  // };

  constructor(props) {
    super(props);
    this.alertRef = React.createRef();
    this.state = {
      username: "",
      email: "",
      password: "",
      roleId: "",
      agreement: "",
      rolesOptions: [],
      adminRole: authRoles.admin,
      saRole: authRoles.sa,
      currentUser: jwtAuthService.getUser(),
      textAlert: '',
      severityAlert: 'info',
      showAlert: false
    };
    // jwtAuthService.getAllRoles()
    // .then(data => {
    //   this.setState({
    //     ...this.state,
    //     rolesOptions: data,
    //   });
    // });
  }  

  useStyles = makeStyles((theme) => ({
    formControl: {
      margin: theme.spacing(1),
      minWidth: 120,
    },
    selectEmpty: {
      marginTop: theme.spacing(2),
    },
  }));

  alert = (msg, severity) =>  {
    let sev = 'info';
    if (severity && severity.length > 0)  {
      sev = severity;
    }
    this.setState({
      ...this.state,
      textAlert: msg,
      severityAlert: sev,
      showAlert: true
    });
    setTimeout(() => {
      this.alertRef.current.focus();
    }, 90);
  };

  getRolesOptions = async () =>  {
    return await jwtAuthService.getAllRoles();
      //.then(data => data);
  };
  
  handleRoleChange = (event) => {
    event.persist();
    this.setState({
      ...this.state,
      roleId: event.target.value,
    });
  };

  handleChange = event => {
    event.persist();
    
    switch(event.target.name) {
      case 'username':
        let userName = event.target.value;
        let pos = event.target.value.indexOf('@');
        if (pos >= 0) {
          userName = event.target.value.substring(0, pos);
          let domain = event.target.value.substring(pos + 1);
          if (domain !== this.state.currentUser.tenantDomain && this.state.saRole.indexOf(this.state.currentUser.role) === -1) {
            domain = this.state.currentUser.tenantDomain;
          }
          userName = userName + '@' + domain;
        }
        else  {
          userName = event.target.value + '@' + this.state.currentUser.tenantDomain;
        }
        // this.state.username = userName;
        this.setState({
          ...this.state,
          ['username']: userName
        });    
        break;
      default:
        this.setState({
          ...this.state,
          [event.target.name]: event.target.value
        });
        break;
    }
  };

  handleFormSubmit = event => {};

  componentDidMount() {
    this.getRolesOptions().then(roles => {
      
      this.setState({
        ...this.state,
        rolesOptions: roles,
      });
    });
  }

  render() {
    let { username, email, password } = this.state;
    return (
      <div className="signup flex justify-center w-full h-full-screen">
        { 
        this.state.showAlert && 
        <div tabIndex='-1' ref={this.alertRef}>
          <Alert
              onClose={ () => { 
                  this.state.showAlert = false;
              } }
              severity={this.state.severityAlert}
          >
              {this.state.textAlert}
          </Alert>
        </div>
        }
        <div className="p-8">
          <Card className="signup-card position-relative y-center">
            <Grid container>
              <Grid item lg={5} md={5} sm={5} xs={12}>
                <div className="p-8 flex justify-center bg-light-gray items-center h-full">
                  <img
                    src="/assets/images/illustrations/posting_photo.svg"
                    alt=""
                  />
                </div>
              </Grid>
              <Grid item lg={7} md={7} sm={7} xs={12}>
                <div className="p-9 h-full">
                  <ValidatorForm ref="form" onSubmit={this.handleFormSubmit}>
                    <TextValidator
                      className="mb-6 w-full"
                      variant="outlined"
                      label="Username"
                      onChange={this.handleChange}
                      type="text"
                      name="username"
                      value={username}
                      validators={["required"]}
                      errorMessages={["this field is required"]}
                    />
                    <FormControl variant="filled">
                    {/* <FormControl variant="filled" className={this.useStyles().formControl}> */}
                      {/* <InputLabel htmlFor="filled-age-native-simple">Perfil</InputLabel> */}
                      <InputLabel>Perfil</InputLabel>
                      <Select
                        className="mb-6 w-full"
                        native
                        value={this.state.roleId}
                        onChange={this.handleRoleChange}
                        // inputProps={{
                        //   name: 'age',
                        //   id: 'filled-age-native-simple',
                        // }}
                      >
                        {
                          this.state.rolesOptions.map(role =>
                            (<option key={role.RoleId} value={role.RoleId}>{role.RoleName}</option>)  
                          )
                        }

                        {/* {
                        await jwtAuthService.getAllRoles().map(role => {
                          return (<option value={role.RoleId}>{role.RoleName}</option>)
                        })
                        } */}


                        {/* <option aria-label="None" value="" /> */}



                      </Select>
                    </FormControl>
                    <TextValidator
                      className="mb-6 w-full"
                      variant="outlined"
                      label="Email"
                      onChange={this.handleChange}
                      type="email"
                      name="email"
                      value={email}
                      validators={["required", "isEmail"]}
                      errorMessages={[
                        "this field is required",
                        "email is not valid"
                      ]}
                    />
                    <TextValidator
                      className="mb-4 w-full"
                      label="Password"
                      variant="outlined"
                      onChange={this.handleChange}
                      name="password"
                      type="password"
                      value={password}
                      validators={["required"]}
                      errorMessages={["this field is required"]}
                    />
                    <FormControlLabel
                      className="mb-4"
                      name="agreement"
                      onChange={this.handleChange}
                      control={<Checkbox />}
                      label="Eu e li e aceito os termos do Serviço."
                    />
                    <div className="flex items-center">
                      <Button
                        className="capitalize"
                        variant="contained"
                        color="primary"
                        type="submit"
                      >
                        Registrar
                      </Button>
                      <span className="mx-2 ml-5">ou</span>
                      <Button
                        className="capitalize"
                        onClick={() =>
                          this.props.history.push("/session/signin")
                        }
                      >
                        Log in
                      </Button>
                    </div>
                  </ValidatorForm>
                </div>
              </Grid>
            </Grid>
          </Card>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  // setUser: PropTypes.func.isRequired
});

export default connect(mapStateToProps, {})(SignUp);
