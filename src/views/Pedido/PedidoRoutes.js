import React from "react";
import { authRoles } from "../../auth/authRoles";

const pedidoRoutes = [
  {
    path: "/pedidos",
    component: React.lazy(() => import("./MeusPedidos")),
    auth: authRoles.cliente
  }
];

export default pedidoRoutes;