import React, { useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { useLocation } from "react-router-dom";
import ZCardList from '../../components/Cards/ZCardList';
import restService from '../../services/linkcards/pedidoRest';
import SaveIcon from '@material-ui/icons/Save';
import { Fab } from '@material-ui/core';
import { Alert } from '@material-ui/lab';
import { green } from '@material-ui/core/colors';
import TableLayout from '../../components/Common/TableLayout';
import { ToggleButton, ToggleButtonGroup } from '@material-ui/lab';

const useStyles = makeStyles((theme) => ({
  fab: {
    position: 'absolute',
    bottom: theme.spacing(2),
    right: theme.spacing(2),
  },
  fabGreen: {
    color: theme.palette.common.white,
    backgroundColor: green[500],
    '&:hover': {
      backgroundColor: green[600],
    },
  }
}));

export default function MeusPedidosCliente({history, props}) {
    const classes = useStyles();
    //const location = useLocation();
    const [page, setPage] = useState(0);
    const [rowsPerPage, setRowsPerPage] = useState(10);
    const[dias, setDias] = useState(15);
    const [pedidos, setPedidos] = useState([]);

    const handleDias = (event, newDias) => {
        event.preventDefault();
        setDias(newDias);
    };

    const handleChangePage = (event, newPage) => {
      setPage(newPage);
  };


  useEffect(() =>   {
    const loadPedidos = async (cliId, d) => {
        let pedidosData = await restService.getPedidoCabecalhoCliente(cliId, d);
        let data = pedidosData.map(elem => {
          let e = {...elem};
          //e.DtInicioVigenciaStr = (new Intl.DateTimeFormat('pt-BR')).format(elem.DtInicioVigencia);
          e.Key = elem.Id;  
          e.key = elem.Id;  
          return e;
        });
        console.log(data);
        setPedidos(Array.from(data));
    }

    setPedidos([]);
    loadPedidos(1, dias);
  },
  [dias]);

  return (
    <div>
        <ToggleButtonGroup value={dias} onChange={handleDias} exclusive aria-label="text formatting">
            <ToggleButton key={15} value={15} aria-label="bold">
                15
            </ToggleButton>
            <ToggleButton key={30} value={30} aria-label="bold">
                30
            </ToggleButton>
            <ToggleButton key={60} value={60} aria-label="bold">
                60
            </ToggleButton>
            <ToggleButton key={90} value={90} aria-label="bold">
                90
            </ToggleButton>
            <ToggleButton key={120} value={120} aria-label="bold">
                120
            </ToggleButton>
        </ToggleButtonGroup>
        <TableLayout 
            titles={{ Key: 'Número', DhCreate: 'Criado em', StatusNome: 'StatusP', NumeroPedidoErp: 'Número ERP', ValorTotal: 'Total', LinkCardClienteKey: 'Card' }}
            visibilityCols={{ Key: true, DhCreate: true, StatusNome: true, NumeroPedidoErp: true, ValorTotal: true, LinkCardClienteKey: true  }}
            rows={pedidos} 
            page={page}
            rowsPerPage={rowsPerPage}
            changePage={handleChangePage}
            // changeRowsPerPage={handleChangeRowsPerPage}
            hasCheck={false} 
            hasDetail={false}
            hasModal={false}
            // verify={isChecked} 
            // handleClick={handleClick}
            // handleOpenModal={handleOpenModal}
            hasFooter={true}
        />
    </div>
  );
}
