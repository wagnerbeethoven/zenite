import React, { useEffect, useState } from 'react';
import restService from '../../services/linkcards/tenantRest';
import { makeStyles, useTheme, withStyles } from '@material-ui/core/styles';
import { TextField, Button } from '@material-ui/core';
import { connect } from "react-redux";
import PropTypes from "prop-types";
import {
    setCard,
    setTextMessge,
    setConnected,
    SET_CONNECTED
} from "../../redux/actions/CardsActions";
import { render } from '@testing-library/react';
  
const MakeCardsStep3 = props => {
    const {
        setCard,
        setTextMessge,
        textMessage,
        connected,
        setConnected,
        showMessage
      // container,
      //theme,
      // settings,
      // user
    } = props;

    const qrCodePeriod = 18000;
    const connectedImg = `${process.env.PUBLIC_URL}/assets/images/connected.jpg`;
      const [enviando, setEnviando] = useState(false);

    // CRUD - LinkCard
    const [nomeLinkCard, setNomeLinkCard] = useState('');
    const [text, setText] = useState('');
    //const [checkNumbers, setCheckNumbers] = useState(false);

    // QR-CODE
    const [qrCodeBase64, setQrCodeBase64] = useState(connectedImg);
    const [disconnecting, setDisconnecting] = useState(false);

    // async function loadQrCode() {
    //     
    //     let qrCode = restService.getQrCodeBase64();
    //     if (qrCode && qrCode.data && qrCode.data.value && qrCode.data.value.length > 0)   {
    //         setQrCodeBase64(qrCode.data.value);
    //     }
    //     else    {
    //         setQrCodeBase64(connectedImg);
    //     }
    // };

    useEffect(() =>   {
        setCard(nomeLinkCard, textMessage);
    },
    [nomeLinkCard]);

    useEffect(() =>   {
        async function load(){
            let qrCode = await restService.getQrCodeBase64();
            setTimeout(() => {
                if (qrCode && qrCode.data && qrCode.data.value && qrCode.data.value.length > 0)   {
                    setQrCodeBase64(qrCode.data.value);
                }
                else    {
                    setQrCodeBase64(connectedImg);
                    setConnected(true);
                }
            }, 90);
        }
        
        if (!connected) {
            setTimeout(() => {  
                setQrCodeBase64(`${process.env.PUBLIC_URL}/assets/images/loading.gif`);
                load();
            }, 90);
        }
        const timer = setInterval(() => {
            load();
            // if (connectedImg !== qrCodeBase64)  {
            //     load();
            // }
        }, qrCodePeriod);

        return () => clearInterval(timer);        
    },
    []);

    // useEffect(() =>   {

    //     async function load(){
    //         let qrCode = await restService.getQrCodeBase64();
    //         if (qrCode && qrCode.data && qrCode.data.value && qrCode.data.value.length > 0)   {
    //             //setConnected();
    //             const timer = setTimeout(() => {
    //                 setQrCodeBase64(qrCode.data.value);
    //             }, qrCodePeriod);
    //         }
    //         else    {
    //             setQrCodeBase64(connectedImg);
    //         }
    //     }

    //     
    //     if (qrCodeBase64 !== connectedImg)  {
    //         load();
    //     }

    //     // 
    //     // if (!connected) {
    //     //     load();
    //     // }
    //     // const timer = setInterval(() => {
    //     //     if (!connected)   {
    //     //         load();
    //     //     }
    //     // }, qrCodePeriod);

    //     // return () => clearInterval(timer);        

    // },
    // [qrCodeBase64]);

    const handleDisconnect = (event) => {
        async function disconnect()   {
            if (await restService.disconnect()) {
                setConnected(false);
                setQrCodeBase64(`${process.env.PUBLIC_URL}/assets/images/loading.gif`);
                //render();
                setDisconnecting(false);
            }
        }
        event.preventDefault();
        
        setDisconnecting(true);
        disconnect();
    }

    return (
        <div>
        <div>
            <img
                src={qrCodeBase64}
            />
            <Button onClick= { (event) => handleDisconnect(event)} color="primary">
                    {
                        disconnecting &&
                        <img
                            src={`${process.env.PUBLIC_URL}/assets/images/loading.gif`}
                        />
                    }
                    {
                        !disconnecting && qrCodeBase64 == connectedImg &&
                        'Desconectar'
                    }
            </Button>
            </div>
            <div>
                <TextField
                    autoFocus
                    margin="dense"
                    value={nomeLinkCard}
                    label="Nome do Link Cards"
                    onChange={event => setNomeLinkCard(event.target.value)}
                    //type="number"
                    fullWidth
                />
                <TextField
                    margin="dense"
                    label="Mensagem"
                    value={textMessage}
                    rows={10}
                    onChange={event => setTextMessge(event.target.value)}
                    //type="number"
                    multiline
                    fullWidth
                />
                {/* <Button onClick= { (event) => handleSend(event)} color="primary">
                        {
                            enviando &&
                            <img
                                src={`${process.env.PUBLIC_URL}/assets/images/loading.gif`}
                            />
                        }
                        {
                            !enviando &&
                            'Enviar'
                        }
                </Button> */}
            </div>
        </div>
        );
    }

const mapStateToProps = store => ({
    setCard: PropTypes.func.isRequired,
    setTextMessge: PropTypes.func.isRequired,
    textMessage: store.cards.textMessage,
    connected: store.cards.connected,
    setConnected: PropTypes.func.isRequired
});

export default withStyles(
    {},
    { withTheme: true }
  )(connect(mapStateToProps, { setCard, setTextMessge, setConnected })(MakeCardsStep3));
