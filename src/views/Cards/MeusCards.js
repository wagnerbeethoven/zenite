import React, { useEffect, useState, useRef } from 'react';
import ReactDOM from 'react-dom'
import { makeStyles, useTheme, withStyles } from '@material-ui/core/styles';
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Alert } from '@material-ui/lab';
import restService from '../../services/linkcards/cardsRest';
import SaveIcon from '@material-ui/icons/Save';
import { AppBar, Button, Dialog, Divider, IconButton, List, ListItem, ListItemText, Slide, Toolbar, Typography } from '@material-ui/core';
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import KeyboardArrowRight from '@material-ui/icons/KeyboardArrowRight';
import { green } from '@material-ui/core/colors';
import TableLayout from '../../components/Common/TableLayout';
import { ToggleButton, ToggleButtonGroup } from '@material-ui/lab';
import 'react-data-grid/dist/react-data-grid.css';
import {
  showError,
  closeAlert
} from "../../redux/actions/AlertActions";
import MeusCardsVendedorDevExpr from './MeusCardsVendedorDevExpr';
import MeusCardsClienteDevExpr from './MeusCardsClienteDevExpr';

const useStyles = makeStyles((theme) => ({
  fab: {
    position: 'absolute',
    bottom: theme.spacing(2),
    right: theme.spacing(2),
  },
  fabGreen: {
    color: theme.palette.common.white,
    backgroundColor: green[500],
    '&:hover': {
      backgroundColor: green[600],
    },
  },
  appBar: {
    position: 'relative',
  },
  title: {
    marginLeft: theme.spacing(2),
    flex: 1,
  }
}));

const MeusCards = props => {
    const classes = useStyles();
    const alertRef = useRef(null);
    const [page, setPage] = useState(0);
    const [rowsPerPage, setRowsPerPage] = useState(100);
    const[dias, setDias] = useState(15);
    const [detailOpen, setDetailOpen] = React.useState(false);
    const [cards, setCards] = useState([]);
    const [initialCards, setInitialCards] = useState([]);
    const[openDialog, setOpenDialog] = useState(false);
    const[dialogTitle, setDialogTitle] = useState('');
    const[linkCardId, setLinkCardId] = useState(0);
    const[openAlert, setOpenAlert] = useState(false);

    const handleDias = (event, newDias) => {
        event.preventDefault();
        setDias(newDias);
    };

  const showError = (text) => {
    props.showError(text);
    alertRef.current.focus();
  }

  useEffect(() =>   {
    const loadCards = async (id, d) => {
        let cardsData = [];
        if (props.view === 'VENDEDOR')  {
            cardsData = await restService.getLinkCardsCabecalhoVendedor(id, d, showError);
        }
        else    {
            cardsData = await restService.getLinkCardsCabecalhoCliente(id, d, showError);
        }
        if (cardsData)  {
          let data = cardsData.map(elem => {
            let e = {...elem};
            e.Key = elem.Id;  
            e.key = elem.Id;  
            return e;
          });
          console.log(data);
          setCards(Array.from(data));
        }
    }
    console.log(props);
    setCards([]);
    //loadCards(props.id, dias);
    loadCards(1, dias);
  },
  [dias]);

  return (
    <div>
        {
        props.alert.show &&
        <div id='alert' tabIndex='-1' ref={alertRef} >
          <Alert
            onClose={ () => { 
                props.closeAlert();
            } }
            severity={props.alert.message.severity}
          >
            {props.alert.message.text}
          </Alert>
        </div>
      }
      <ToggleButtonGroup value={dias} onChange={handleDias} exclusive aria-label="text formatting">
            <ToggleButton key={15} value={15} aria-label="bold">
                15
            </ToggleButton>
            <ToggleButton key={30} value={30} aria-label="bold">
                30
            </ToggleButton>
            <ToggleButton key={60} value={60} aria-label="bold">
                60
            </ToggleButton>
            <ToggleButton key={90} value={90} aria-label="bold">
                90
            </ToggleButton>
            <ToggleButton key={120} value={120} aria-label="bold">
                120
            </ToggleButton>
        </ToggleButtonGroup>

        {
        props.view === 'VENDEDOR' &&
        <MeusCardsVendedorDevExpr dias={dias}/>
        }
        {
        (props.view === 'CLIENTE' || true) &&
        <MeusCardsClienteDevExpr dias={dias} data={cards}/>
        }

    </div>
  );
}

const mapStateToProps = store => ({
  alert: store.alert,
  showError: PropTypes.func.isRequired,
  closeAlert: PropTypes.func.isRequired
});

export default withStyles(
  {},
  { withTheme: true }
)(connect(mapStateToProps, { showError, closeAlert })(MeusCards));
