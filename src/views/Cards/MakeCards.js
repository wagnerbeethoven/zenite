import React, { useEffect, useState, useRef } from 'react';
import PropTypes from "prop-types";
import { Alert, AlertTitle } from '@material-ui/lab';
import Box from '@material-ui/core/Box';

import { Button, Step, StepLabel, Stepper, Typography } from '@material-ui/core';
import { makeStyles, useTheme, withStyles } from '@material-ui/core/styles';
import MakeCardsStep1 from './MakeCardsStep1'
import MakeCardsStep2 from './MakeCardsStep2'
import MakeCardsStep3 from './MakeCardsStep3'
import restService from '../../services/linkcards/cardsRest';
import { connect } from "react-redux";
import Grid from '@material-ui/core/Grid';
import {
  showError,
  showInfo,
  showWarning,
  closeAlert
} from "../../redux/actions/AlertActions";
import {
  setLoading
} from "../../redux/actions/CardsActions";
import { propTypes } from 'react-bootstrap/esm/Image';

const useStyles = makeStyles((theme) => ({
  stepperRoot: {
    width: '100%',
  },
  stepperButton: {
    marginRight: theme.spacing(1),
  },
  instructions: {
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(1),
  },
}));


const steps = ['Produtos', 'Clientes', 'Pedidos'];

const MakeCards = props => {
  const {
    card,
    loading,
    productsSelected,
    clientsSelected,
    textMessage,
    nomeUmCliente,
    phoneUmCliente,
    connected,
    setConnected
    // container,
    //theme,
    // settings,
    // user
  } = props;
  const classes = useStyles();
  const [activeStep, setActiveStep] = React.useState(0);
  const [waHref, setWaHref] = React.useState('');
  const [linkVisible, setLinkVisible] = React.useState(false);
  const alertRef = useRef(null);
  //const linkRef = React.useRef(null);

  const showError = (text) => {
    props.showError(text);
    setTimeout(() => {
      alertRef.current.focus();
    }, 60);
  }

  const showWarning = (text) => {
    props.showWarning(text);
    setTimeout(() => {
      alertRef.current.focus();
    }, 60);
  }


  // const alert = (msg, severity) =>  {
  //   setSeverityMsg(severity);
  //   setTextMsg(msg);
  //   openAlert();
  //   //window.scrollTo(0,0);
  //   // setTimeout(() => {
  //   //   
  //   //   alertRef.current.focus();
  //   // }, 90);
  //   //setShowMsg(true);
  // };

  const getStepContent = (stepIndex) => {
    setTimeout(() => {
      setLoading(true);
    }, 30);
    switch (stepIndex) {
      case 0:
        return (
          <MakeCardsStep1 showMessage={showError} />
        );
      case 1:
        return (
          <MakeCardsStep2 showMessage={showError} />
        );
      case 2:
        return (
          <MakeCardsStep3 showMessage={showError} />
        );
      default:
        setTimeout(() => {
          setLoading(false);
        }, 30);
        return 'Unknown stepIndex';
    }
  }

  const envioSolo = async () => {
    let result = await restService.postLinkCards(card.nome, textMessage, productsSelected, clientsSelected, false);
    if (result.success) {
      setWaHref(`https://api.whatsapp.com/send?phone=${phoneUmCliente}&text=${textMessage.replace('~{Cliente.Nome}', nomeUmCliente)}   \n\nzenite.inf.br:33018/wa?guid=${result.data}`);
      setTimeout(() => {
        console.log(waHref);
      }, 90);
      props.showInfo('LinkCard gerado com sucesso! Clique no link para abrir o WhatsApp.');
      setLinkVisible(true);
      //linkRef.click();        
    }
  }

  const handleNext = async () => {
    closeAlert();
    if (activeStep == steps.length - 1) {
      
      let result = await restService.postLinkCards(card.nome, textMessage, productsSelected, clientsSelected, true);
      alert(result.data, result.success ? 'success' : 'error');
    }
    else {
      switch (activeStep) {
        case 0:
          if (productsSelected && productsSelected.length > 0) {
            setActiveStep((prevActiveStep) => prevActiveStep + 1);
          }
          else {
            alert('Nenhum Produto selecionado. Ao menos um deve ser escolhido.', 'warning');
          }
          break;
        case 1:
          if (clientsSelected && clientsSelected.length > 0) {
            setConnected(true);
            setActiveStep((prevActiveStep) => prevActiveStep + 1);
          }
          else {
            alert('Nenhum Cliente selecionado. Ao menos um deve ser escolhido.', 'warning');
          }
          break;
      }
    }
  };

  const handleBack = () => {
    setActiveStep((prevActiveStep) => prevActiveStep - 1);
  };

  return (
    // <div className="m-sm-30" className={classes.root} > TAG COM O ESPAÇAMENTO DA PÁGINA
    <div className={classes.root} >
    {/* <h1 className="mt-0 mb-4 font-medium text-28">Titulo da página</h1> TITULO DA PÁGINA  */}
      {
        props.alert.show &&
        <div id='alert' tabIndex='-1' ref={alertRef} >
          <Alert onClose={() => { props.closeAlert(); }} severity={props.alert.message.severity} >
            {
              props.alert.message.text.substr(1, 24).toUpperCase().indexOf('HTML') < 0 && props.alert.message.text
            }
            {
              props.alert.message.text.substr(1, 24).toUpperCase().indexOf('HTML') >= 0 && <div dangerouslySetInnerHTML={{ __html: props.alert.message.text }}> </div>
            }

          </Alert>
        </div>
      }
      {/* NAVEGAÇÃO ENTRE OS ITENS */}
      <div className="steps-container">

        {/* BOTÃO DE VOLTAR */}
        <div className="steps-back">
          <Button disabled={activeStep === 0} onClick={handleBack} className={classes.backButton}>
            <span className="material-icons">
              arrow_back_ios
            </span>
            Voltar
          </Button>
        </div>

        {/* STEPS */}
        <div className="steps-details">
          <Stepper xs={12} activeStep={activeStep} alternativeLabel>
            {steps.map((label) => (
              <Step key={label}>
                <StepLabel>{label}</StepLabel>
              </Step>
            ))}
          </Stepper>
        </div>

        {/* BOTÃO DE AVANCAR */}
        <div className="steps-new">
          {
            (activeStep != 2 || connected) &&
              <Button variant="contained" color="primary" onClick={handleNext}>
                {activeStep === steps.length - 1 ? 'Enviar' : 'Próximo'}
                <span className="material-icons">
                  arrow_forward_ios
                </span>
            </Button>
          }
        </div>
      </div>
      {activeStep === steps.length ? (
        <Alert severity="info">
          <AlertTitle>Atenção</AlertTitle>
        Mensagens sendo enviadas.
        </Alert>
      ) : (
          <div>
            {getStepContent(activeStep)}
            {!loading &&
              <div className="d-none">
                <Button 
                  disabled={activeStep === 0}
                  onClick={handleBack}
                  className={classes.backButton}
                >
                  Voltar
                </Button>
                {
                  (activeStep != 2 || connected) &&
                  <Button  variant="contained" color="primary" onClick={handleNext}>
                    {activeStep === steps.length - 1 ? 'Enviar' : 'Próximo'}
                  </Button>
                }
                {
                  clientsSelected && clientsSelected.length == 1 && activeStep === 2 &&
                  <div>
                    <Button  variant="contained" color="primary" onClick={envioSolo}>
                      Enviar 1
                    </Button>
                    {
                      linkVisible &&
                      <a
                        //rel="Envio único, direto pelo WhatsApp"
                        href={waHref}
                        //ref={elem => linkRef = elem }
                        //href={`https://api.whatsapp.com/send?phone=${phoneUmCliente}&text=${textMessage.replace('~{Cliente.Nome}', nomeUmCliente)}`}
                        target="_blank"
                      >Abrir WhatsApp</a>
                    }
                  </div>
                }
              </div>
            }
          </div>
        )}
    </div>
  );

}

const mapStateToProps = store => ({
  card: store.cards.card,
  loading: store.cards.loading,
  setLoading: PropTypes.func.isRequired,
  alert: store.alert,
  showInfo: PropTypes.func.isRequired,
  showWarning: PropTypes.func.isRequired,
  showError: PropTypes.func.isRequired,
  closeAlert: PropTypes.func.isRequired,
  productsSelected: store.cards.productsSelected,
  clientsSelected: store.cards.clientsSelected,
  textMessage: store.cards.textMessage,
  nomeUmCliente: store.cards.nomeUmCliente,
  phoneUmCliente: store.cards.phoneUmCliente,
  setConnected: PropTypes.func.isRequired,
  connected: store.cards.connected
});

export default withStyles(
  {},
  { withTheme: true }
)(connect(mapStateToProps, { setLoading, showInfo, showWarning, showError, closeAlert })(MakeCards));


