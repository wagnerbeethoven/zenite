import React, { useEffect, useState, useRef } from 'react';
import { makeStyles, useTheme, withStyles } from '@material-ui/core/styles';
import { useLocation } from "react-router-dom";
import ZCardList from '../../components/Cards/ZCardList';
import restService from '../../services/linkcards/cardsRest';
import restPedidoService from '../../services/linkcards/pedidoRest';
import DoneAllIcon from '@material-ui/icons/DoneAll';
import CloseIcon from '@material-ui/icons/Close';
import ConsignadoIcon from '@material-ui/icons/AttachFile';
import PedidoIcon from '@material-ui/icons/AttachMoney';
import ForwardIcon5 from '@material-ui/icons/Forward5';
import ForwardIcon10 from '@material-ui/icons/Forward10';
import ForwardIcon30 from '@material-ui/icons/Forward30';
import RemoveShoppingCartIcon from '@material-ui/icons/RemoveShoppingCart';
import CancelIcon from '@material-ui/icons/Cancel';
import DeleteForeverIcon from '@material-ui/icons/DeleteForever';
import { AppBar, Avatar, Button, Dialog, DialogTitle, Divider, Fab, IconButton, List, ListItem, ListItemAvatar, ListItemText, Slide, Toolbar, Typography } from '@material-ui/core';
import { Alert } from '@material-ui/lab';
import { green } from '@material-ui/core/colors';
import { blue } from '@material-ui/core/colors';
import PropTypes from "prop-types";
import { connect } from "react-redux";
import {
  showError,
  showInfo,
  closeAlert
} from "../../redux/actions/AlertActions";
import qs from 'qs';
import PedidoRestService from '../../services/linkcards/pedidoRest';

const useStyles = makeStyles((theme) => ({
  fab: {
    position: 'absolute',
    bottom: theme.spacing(2),
    right: theme.spacing(2),
  },
  title: {
    marginLeft: theme.spacing(2),
    flex: 1,
  },
  appBar: {
    position: 'relative',
  },
  avatar: {
    backgroundColor: blue[100],
    color: blue[600],
  },
  fabGreen: {
    color: theme.palette.common.white,
    backgroundColor: green[500],
    '&:hover': {
      backgroundColor: green[600],
    },
  }
}));

const LojaVirtual = props => {
  const alertRef = useRef(null);
  const location = useLocation();
  const classes = useStyles();
  const [cards, setCards] = useState([]);
  const [severityMsg, setSeverityMsg] = React.useState('info');
  const [textMsg, setTextMsg] = React.useState('');
  const [showMsg, setShowMsg] = React.useState(false);
  const [openDialog, setOpenDialog] = useState(false);
  const [pedidos, setPedidos] = useState([]);
  const [storageList, setStorageList] = useState([]);
  const [valTotalPedido, setValTotalPedido] = useState(0);
  const [qtdePedidos, setQtdePedidos] = useState(0);
  const [qtdeItens, setQtdeItens] = useState(0);
  const [qtdePecas, setQtdePecas] = useState(0);

  const Transition = React.forwardRef(function Transition(props, ref) {
    return <Slide direction="up" ref={ref} {...props} />;
  });

  const showError = (text) => {
    props.showError(text);
    alertRef.current.focus();
  }

  const handleSave = async (event) => {
    event.preventDefault();
    if (cards && cards.length > 0) {
      let metaPedidos = restPedidoService.createPedidos(cards, false);
      console.log(metaPedidos);
      setPedidos(metaPedidos.data);
      setStorageList(metaPedidos.storageList);
      let total = 0;
      let qtdePed = 0;
      let qtdeItens = 0;
      let qtdePecas = 0;
      metaPedidos.data.forEach(value => {
        total += value.ValorTotal;
        qtdePed++;
        value.PedidoProdutoDtoSet.forEach(item => {
          qtdeItens++;
          qtdePecas += item.Qtde;
        });
      });

      setValTotalPedido(total);
      setQtdePedidos(qtdePed);
      setQtdeItens(qtdeItens);
      setQtdePecas(qtdePecas);
    }
    setOpenDialog(true);

  };

  const handleCloseDialog = () => {
    setOpenDialog(false);
  };

  const handleEmptyCart = () => {
    if (storageList && storageList.length > 0) {
      restPedidoService.emptyCart(storageList);
      setPedidos([]);
      setStorageList([]);
    }
    setOpenDialog(false);
  }

  const handlePostPedido = async (isConsignado) => {
    pedidos.forEach(ped => {
      ped.IndConsignado = isConsignado;
    });
    setOpenDialog(false);
    let result = await restPedidoService.postPedidos(pedidos, storageList, showError);
    if (result) {
      props.showInfo(result);
      alertRef.current.focus();
    }
  };

  useEffect(() => {
    async function loadCards(cardId) {
      let cardsData = await restService.getProdutosCard(cardId);
      if (cardsData) {
        setCards(Array.from(cardsData));
      }
    }
    async function loadCardsDirect(guid) {
      let cardsData = await restService.getProdutosCardDirect(guid);
      if (cardsData) {
        setCards(Array.from(cardsData));
      }
    }
    
    if (props && props.linkCardId && props.linkCardId > 0) {
      loadCards(props.linkCardId);
    }
    else if (props && props.guid && props.guid.length > 0) {
      loadCardsDirect(props.guid);
    }
    else {
      switch (location.pathname) {
        case '/catalogo':
          loadCards(0);
          break;
        default:
          if (location.state) {
            if (location.state.id >= 0) {
              loadCards(location.state.id);
            }
            else if (location.state.guid) {
              loadCardsDirect(location.state.guid);
            }
          }
          else if (location.search) {
            if (location.search.indexOf('?guid=') >= 0) {
              loadCardsDirect(location.search.replace('?guid=', ''));
            }
            else if (location.search.indexOf('?id=') >= 0) {
              loadCards(location.search.replace('?id=', ''));
            }
          }
          break;
      }
    }

  },
    //[props]);
    []);

  return (
    <div>
      {
        props.alert.show &&
        <div id='alert' tabIndex='-1' ref={alertRef} >
          <Alert
            onClose={() => {
              props.closeAlert();
            }}
            severity={props.alert.message.severity}
          >
            {props.alert.message.text}
          </Alert>
        </div>
      }
      <div>
        <ZCardList
          cards={cards}
          periodo={3600}
          isCart={true}
          cols={3}
        />
      </div>
      <div className="button-order">
          <Fab aria-label='Gerar Pedido/Consignado' title="Gerar Pedido ou Consignado" className={classes.fab} color='primary' href='#' onClick={(event) => handleSave(event)}>
            <DoneAllIcon />
            <span className="button-order-description">Gerar Pedido ou Consignado</span>
          </Fab>
      </div>
      <Dialog className="dialog-order" open={openDialog} onClose={handleCloseDialog} aria-labelledby="simple-dialog-title">
        <IconButton className="dialog-close" edge="start" color="inherit" onClick={handleCloseDialog} aria-label="close">
          <CloseIcon />
        </IconButton>
        <DialogTitle className="dialog-title" id="simple-dialog-title">
          <div className="dialog-order-items">
            {`${qtdeItens} item(ns)`}
          </div>
          <div className="dialog-order-pieces">
            {`${qtdePecas} peça(s)`}
          </div>
          <div className="dialog-order-prices">
            {`R$ ${Math.round(valTotalPedido * 100) / 100}`}
          </div>
        </DialogTitle>
        <List>
          <Divider />
          <ListItem button onClick={() => handlePostPedido(false)} key={0}>
            <ListItemAvatar>
              <Avatar className={classes.avatar}>
                <PedidoIcon />
              </Avatar>
            </ListItemAvatar>
            <ListItemText primary="Enviar Pedido" />
          </ListItem>
          <Divider />

          <ListItem button onClick={() => handlePostPedido(true)} key={1}>
            <ListItemAvatar>
              <Avatar className={classes.avatar}>
                <ConsignadoIcon />
              </Avatar>
            </ListItemAvatar>
            <ListItemText primary="Enviar Consignado" />
          </ListItem>
          <Divider />
          {/* <ListItem button onClick={() => setOpenDialog(false)} key={-5}>
              <ListItemAvatar>
                <Avatar className={classes.avatar}>
                  <ForwardIcon5 />
                </Avatar>
              </ListItemAvatar>
              <ListItemText primary="Lembrar em 05 minutos" />
            </ListItem> */}
          <ListItem button onClick={() => setOpenDialog(false)} key={-10}>
            <ListItemAvatar>
              <Avatar className={classes.avatar}>
                <ForwardIcon10 />
              </Avatar>
            </ListItemAvatar>
            <ListItemText primary="Lembrar em 10 minutos" />
          </ListItem>
          <Divider />

          <ListItem button onClick={() => setOpenDialog(false)} key={-30}>
            <ListItemAvatar>
              <Avatar className={classes.avatar}>
                <ForwardIcon30 />
              </Avatar>
            </ListItemAvatar>
            <ListItemText primary="Lembrar em 30 minutos" />
          </ListItem>
          {/* <ListItem button onClick={() => setOpenDialog(false)} key={-1}>
              <ListItemAvatar>
                <Avatar className={classes.avatar}>
                  <CancelIcon />
                </Avatar>
              </ListItemAvatar>
              <ListItemText primary="Cancelar" />
            </ListItem> */}
          <Divider />
          <ListItem button onClick={handleEmptyCart} key={-99}>
            <ListItemAvatar>
              <Avatar className={classes.avatar}>
                <RemoveShoppingCartIcon color="error" />
              </Avatar>
            </ListItemAvatar>
            <ListItemText primary="Esvaziar carrinho" />
          </ListItem>

        </List>
      </Dialog>
    </div>
  );
}

LojaVirtual.defaultProps = {
  linkCardId: 0,
}

const mapStateToProps = store => ({
  alert: store.alert,
  showError: PropTypes.func.isRequired,
  showInfo: PropTypes.func.isRequired,
  closeAlert: PropTypes.func.isRequired
});

export default withStyles(
  {},
  { withTheme: true }
)(connect(mapStateToProps, { showError, closeAlert, showInfo })(LojaVirtual));
