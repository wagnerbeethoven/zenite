import React, { useEffect, useState } from 'react';
import ReactDOM from 'react-dom'
import { makeStyles } from '@material-ui/core/styles';
import { useLocation } from "react-router-dom";
import ZCardList from '../../components/Cards/ZCardList';
import restService from '../../services/linkcards/cardsRest';
import SaveIcon from '@material-ui/icons/Save';
import { AppBar, Button, Dialog, Divider, IconButton, List, ListItem, ListItemText, Slide, Toolbar, Typography } from '@material-ui/core';
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import KeyboardArrowRight from '@material-ui/icons/KeyboardArrowRight';
import { Alert } from '@material-ui/lab';
import { green } from '@material-ui/core/colors';
import TableLayout from '../../components/Common/TableLayout';
import { ToggleButton, ToggleButtonGroup } from '@material-ui/lab';
import CloseIcon from '@material-ui/icons/Close';
import LojaVirtual from './LojaVirtual';
import DataGrid from 'react-data-grid';
import 'react-data-grid/dist/react-data-grid.css';
import { DataGridDetail } from '../../components/Common/DataGridDetail';

const useStyles = makeStyles((theme) => ({
  fab: {
    position: 'absolute',
    bottom: theme.spacing(2),
    right: theme.spacing(2),
  },
  fabGreen: {
    color: theme.palette.common.white,
    backgroundColor: green[500],
    '&:hover': {
      backgroundColor: green[600],
    },
  },
  appBar: {
    position: 'relative',
  },
  title: {
    marginLeft: theme.spacing(2),
    flex: 1,
  }
}));

const sortRows = (initialRows, sortColumn, sortDirection) => rows => {
  const comparer = (a, b) => {
    if (sortDirection === "ASC") {
      return a[sortColumn] > b[sortColumn] ? 1 : -1;
    } else if (sortDirection === "DESC") {
      return a[sortColumn] < b[sortColumn] ? 1 : -1;
    }
  };
  //
  return sortDirection === "NONE" ? initialRows : [...rows].sort(comparer);
};

const loadDetail = (event, props) => {
  
  event.preventDefault();
  let rowElement = event.currentTarget.parentElement.parentElement;
  //let elem = (<div>Hello world!!</div>);

  //let childrenArray = new Array(rowElement.children.length);

  //let childrenArray = [];
  let childrenHtml = '';
  for(let i = 0; i < rowElement.children.length; i++) {
    childrenHtml += rowElement.children[i].outerHTML;
  }

  //childrenArray.push('<div>Hello world!!</div>'); 
  
  // for (var key in rowElement.children) {
  //     if (childrenArray.length === 7) {
  //       break;
  //     }
  //     childrenArray.push(rowElement.children[key].outerHTML);
  //     //childrenArray[i] =  rowElement.children[key];
  //     //i = i + 1;
  // }  
  ReactDOM.render(React.createElement(DataGridDetail, { idx: props.rowIdx, row: props.row, columns: columns }, null), rowElement);
  //console.log(event);
}

const DetailFormatter = props => {
  console.log(props);
  return (
    <IconButton aria-label="expand row" size="small" onClick={event => loadDetail(event, props)}>
      {/* {detailOpen ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />} */}
      <KeyboardArrowRight /> {props.row[props.column.key]}
    </IconButton>
  )
};

const DateFormatter = props => {
  //return <ProgressBar now={value} label={`${value}%`} />;
  
  let dateStr = props.row[props.column.key].substring(0, 10);
  if (dateStr && dateStr.length >= 10)  {
    dateStr = dateStr.substring(0, 10);
    dateStr = dateStr.substring(8) + '/' + dateStr.substring(5, 7) + '/' +  dateStr.substring(0, 4);
  }
  else  {
    dateStr = '';
  }
  //console.log(props);
  // if (props && props.row && props.column && props.column.key) {
  //   return new Intl.DateTimeFormat('pt-BR',  { dateStyle: 'short' }).format(props.row[props.column.key]);
  //   //return props.row[props.column.key].toLocaleString('pt-BR');
  //  }
  return dateStr; 
  // if (props.value)  {
  //   return props.value.getDate().toString();
  // }
  // else {
  //   return 'XXX';
  // }
};

// const rowRenderer = props => {
//   
//   console.log(props);
//   const { row } = props;
//   if (row.expand) {
//     return <div>my Custom view {row.title}</div>;
//   } else {
//     return <></>;
//   }
// };


const columns = [
  { key: 'Key', name: 'Número', sortable: true, width: 60, frozen: true, formatter: DetailFormatter },
  { key: 'Nome', name: 'Nome', resizable: true, width: 180 },
  { key: 'DtInicioVigencia', name: 'Início', sortable: true, formatter: DateFormatter, width: 90 },
  { key: 'DtFinalVigencia', name: 'Validade', sortable: true, formatter: DateFormatter, width: 90 },
  { key: 'Texto', name: 'Mensagem', resizable: true, width: 360, editor: DataGridDetail },
  { key: 'ClientesCount', name: 'Clientes', sortable: true, width: 40 },
  { key: 'ProdutosCount', name: 'Produtos', sortable: true, width: 40 }
];

export default function MeusCardsVendedor({history, props}) {
    const classes = useStyles();
    //const location = useLocation();
    const [page, setPage] = useState(0);
    const [rowsPerPage, setRowsPerPage] = useState(100);
    const[dias, setDias] = useState(15);
    const [detailOpen, setDetailOpen] = React.useState(false);
    const [cards, setCards] = useState([]);
    const [initialCards, setInitialCards] = useState([]);
    const[openDialog, setOpenDialog] = useState(false);
    const[dialogTitle, setDialogTitle] = useState('');
    const[linkCardId, setLinkCardId] = useState(0);

    const handleDias = (event, newDias) => {
        event.preventDefault();
        setDias(newDias);
    };

    const handleChangePage = (event, newPage) => {
      setPage(newPage);
  };

  const handleCloseDialog = () => {
    setOpenDialog(false);
  };

  const handleClick = (event, card) => {
    event.preventDefault();
    console.log(card);
    setLinkCardId(card.Id);
    setDialogTitle(card.nome);
    setTimeout(() => {
      setOpenDialog(true);
    }, 80);
    //history.push("/cards/loja_virtual", { id: card.Id });
};

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

  useEffect(() =>   {
    const loadCards = async (vendId, d) => {
        let cardsData = await restService.getLinkCardsCabecalhoVendedor(vendId, d);
        let data = cardsData.map(elem => {
          let e = {...elem};
          //e.DtInicioVigenciaStr = (new Intl.DateTimeFormat('pt-BR')).format(elem.DtInicioVigencia);
          e.Key = elem.Id;  
          e.key = elem.Id;  
          e.subRowDetails =             [ { Id: 11064, ClientesCount: 100, ProdutosCount: 200 },
            { Id: 11064, ClientesCount: 300, ProdutosCount: 400 }
          ];
          return e;
        });
        console.log(data);
        setCards(Array.from(data));
        setInitialCards(Array.from(data));
    }

    setCards([]);
    loadCards(3, dias);
  },
  [dias]);

//   useEffect(() =>   {
//     async function loadCards(vendId, dias){
//       let cardsData = await restService.getLinkCardsCabecalhoVendedor(vendId, dias);
//       setCards(Array.from(cardsData));
//     }
//     loadCards(3, dias);
//   },
//   [dias]);

  return (
    <div>
        <ToggleButtonGroup value={dias} onChange={handleDias} exclusive aria-label="text formatting">
            <ToggleButton key={15} value={15} aria-label="bold">
                15
            </ToggleButton>
            <ToggleButton key={30} value={30} aria-label="bold">
                30
            </ToggleButton>
            <ToggleButton key={60} value={60} aria-label="bold">
                60
            </ToggleButton>
            <ToggleButton key={90} value={90} aria-label="bold">
                90
            </ToggleButton>
            <ToggleButton key={120} value={120} aria-label="bold">
                120
            </ToggleButton>
        </ToggleButtonGroup>

        <DataGrid
          columns={columns}
          rows={cards}
          rowGetter={i => cards[i]}
          enableCellSelect={true}
          //rowHeight={90}
          rowKey='Id'
          isRowHovered={true}
          // subRowDetails={
          //   [ { Id: 11064, ClientesCount: 100, ProdutosCount: 200 },
          //     { Id: 11064, ClientesCount: 300, ProdutosCount: 400 }
          //   ]
          // }
          //rowRenderer={(props) => <div>Hello world!!</div>}
          //rowRenderer={rowRenderer}
          //onRowDoubleClick={() => console.log('Olá...')}
          //enableRowSelect={true}
          //rowGetter={rowGetter}
          rowsCount={cards.length }
          // onGridSort={(sortColumn, sortDirection) =>  {
          //   
          //   let cardsSorted = sortRows(initialCards, sortColumn, sortDirection);
          //   setTimeout(() => {
          //     setCards(cardsSorted);  
          //   }, 60);            
          // }
          // }
        />

        {/* <TableLayout 
            titles={{ Key: 'Número', Nome: 'Nome', DtInicioVigenciaStr: 'Início vigência', DtFinalVigenciaStr: 'Final vigência', Texto: 'Mensagem', ClientesCount: '#Clientes', ProdutosCount: '#Produtos' }}
            visibilityCols={{ Key: false, Nome: true, DtInicioVigenciaStr: true, DtFinalVigenciaStr: true, Texto: true, ClientesCount: true, ProdutosCount: true }}
            rows={cards} 
            page={page}
            rowsPerPage={rowsPerPage}
            changePage={handleChangePage}
            // changeRowsPerPage={handleChangeRowsPerPage}
            hasCheck={false} 
            hasDetail={true}
            hasModal={false}
            // verify={isChecked} 
            handleClick={handleClick}
            // handleOpenModal={handleOpenModal}
            hasFooter={true}
        /> */}

      <Dialog fullScreen open={openDialog} onClose={handleCloseDialog} TransitionComponent={Transition}>
        <AppBar className={classes.appBar}>
          <Toolbar>
            <IconButton edge="start" color="inherit" onClick={handleCloseDialog} aria-label="close">
              <CloseIcon />
            </IconButton>
            <Typography variant="h6" className={classes.title}>
              {dialogTitle}
            </Typography>
            {/* <Button autoFocus color="inherit" onClick={handleCloseDialog}>
              save
            </Button> */}
          </Toolbar>
        </AppBar>
          <List>
            <ListItem button>
              <ListItemText primary="Phone ringtone" secondary="Titania" />
            </ListItem>
            <Divider />
            <ListItem button>
              <ListItemText primary="Default notification ringtone" secondary="Tethys" />
            </ListItem>
          </List>        {
          // openDialog &&
          // <LojaVirtual linkCardId={linkCardId} />
        }
      </Dialog>

    </div>
  );
}
