import React, { useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { useLocation } from "react-router-dom";
import ZCardList from '../../components/Cards/ZCardList';
import restService from '../../services/linkcards/cardsRest';
import SaveIcon from '@material-ui/icons/Save';
import { Fab } from '@material-ui/core';
import { Alert } from '@material-ui/lab';
import { green } from '@material-ui/core/colors';
import TableLayout from '../../components/Cliente/TableLayoutCliente';
import { ToggleButton, ToggleButtonGroup } from '@material-ui/lab';

const useStyles = makeStyles((theme) => ({
  fab: {
    position: 'absolute',
    bottom: theme.spacing(2),
    right: theme.spacing(2),
  },
  fabGreen: {
    color: theme.palette.common.white,
    backgroundColor: green[500],
    '&:hover': {
      backgroundColor: green[600],
    },
  }
}));

export default function MeusCardsCliente({history, props}) {
    const classes = useStyles();
    //const location = useLocation();
    const [page, setPage] = useState(0);
    const [rowsPerPage, setRowsPerPage] = useState(100);
    const[dias, setDias] = useState(15);
    const [cards, setCards] = useState([]);

    const handleDias = (event, newDias) => {
        event.preventDefault();
        setDias(newDias);
    };

    const handleChangePage = (event, newPage) => {
      setPage(newPage);
  };

  const handleClick = (event, card) => {
    event.preventDefault();
    console.log(card);
    history.push("/cards/loja_virtual", { guid: card.GuidMsg });
};

  useEffect(() =>   {
    const loadCards = async (cliId, d) => {
        let cardsData = await restService.getLinkCardsCabecalhoCliente(cliId, d);
        let data = cardsData.map(elem => {
          let e = {...elem};
          //e.DtInicioVigenciaStr = (new Intl.DateTimeFormat('pt-BR')).format(elem.DtInicioVigencia);
          e.Key = elem.Id;  
          e.key = elem.Id;  
          return e;
        });
        console.log(data);
        setCards(Array.from(data));
    }

    setCards([]);
    loadCards(1, dias);
  },
  [dias]);

//   useEffect(() =>   {
//     async function loadCards(vendId, dias){
//       let cardsData = await restService.getLinkCardsCabecalhoVendedor(vendId, dias);
//       setCards(Array.from(cardsData));
//     }
//     loadCards(3, dias);
//   },
//   [dias]);

  return (
    <div>
        <ToggleButtonGroup value={dias} onChange={handleDias} exclusive aria-label="text formatting">
            <ToggleButton key={15} value={15} aria-label="bold">
                15
            </ToggleButton>
            <ToggleButton key={30} value={30} aria-label="bold">
                30
            </ToggleButton>
            <ToggleButton key={60} value={60} aria-label="bold">
                60
            </ToggleButton>
            <ToggleButton key={90} value={90} aria-label="bold">
                90
            </ToggleButton>
            <ToggleButton key={120} value={120} aria-label="bold">
                120
            </ToggleButton>
        </ToggleButtonGroup>
        <TableLayout 
            titles={{ Nome: 'Nome', Texto: 'Mensagem', Status: 'Status', DeliveryEvent: 'DeliveryEvent', ReadEvent: 'ReadEvent', PedidosCount: 'PedidosCount', ValTotalPedidos: 'ValorTotalPedidos', ValMedioPedidos: 'ValMedioPedidos'  }}
            visibilityCols={{ Nome: true, Texto: true, Status: true, DeliveryEvent: true, ReadEvent: true, PedidosCount: true, ValorTotalPedidos: true, ValMedioPedidos: true  }}
            rows={cards} 
            page={page}
            rowsPerPage={rowsPerPage}
            changePage={handleChangePage}
            // changeRowsPerPage={handleChangeRowsPerPage}
            hasCheck={false} 
            hasDetail={false}
            hasModal={false}
            // verify={isChecked} 
            handleClick={handleClick}
            // handleOpenModal={handleOpenModal}
            hasFooter={true}
        />
    </div>
  );
}
