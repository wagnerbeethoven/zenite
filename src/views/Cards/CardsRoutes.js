import React from "react";
import { authRoles } from "../../auth/authRoles";
import { Redirect } from "../../../node_modules/react-router-dom";

const cardsRoutes = [
  {
    path: "/cards/loja_virtual",
    component: React.lazy(() => import("./LojaVirtual")),
    auth: authRoles.admin
  },
  {
    path: "/wa",
    component: React.lazy(() => import("./LojaVirtual")),
    auth: authRoles.admin
  },
  {
    path: "/linkcards",
    component: React.lazy(() => import("./MakeCards"))
  },
  {
    path: "/linkcardsVend",
    component: React.lazy(() => import("./MeusCardsVendedorDevExpr")),
    auth: authRoles.vendedor
  },
  {
    path: "/catalogo",
    exact: true,
    component: React.lazy(() => import("./LojaVirtual")),
    //component: () => <Redirect to="/cards/loja_virtual?id=0" />
  },
  {
    path: "/linkcardsCli",
    component: React.lazy(() => import("./MeusCards"), { view : 'CLIENTE', id: 1 }),
    auth: authRoles.cliente
  }
];

export default cardsRoutes;