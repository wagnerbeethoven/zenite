import React, { useEffect, useState, useRef } from 'react';
import ReactDOM from 'react-dom'
import { makeStyles, useTheme, withStyles } from '@material-ui/core/styles';
import PropTypes from "prop-types";
import { connect } from "react-redux";
import AlertLinkCards from '../../components/Common/Alert';
import DataGrid,
{
  Column,
  Sorting,
  MasterDetail
} from 'devextreme-react/data-grid';
import { Alert } from '@material-ui/lab';
import { useLocation } from "react-router-dom";
import ZCardList from '../../components/Cards/ZCardList';
import restService from '../../services/linkcards/cardsRest';
import SaveIcon from '@material-ui/icons/Save';
import { AppBar, Button, Dialog, Divider, IconButton, List, ListItem, ListItemText, Slide, Toolbar, Typography } from '@material-ui/core';
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import KeyboardArrowRight from '@material-ui/icons/KeyboardArrowRight';
import { green } from '@material-ui/core/colors';
import TableLayout from '../../components/Common/TableLayout';
import { ToggleButton, ToggleButtonGroup } from '@material-ui/lab';
import CloseIcon from '@material-ui/icons/Close';
import LojaVirtual from './LojaVirtual';
import 'react-data-grid/dist/react-data-grid.css';
import { DataGridDetailDevExpr } from '../../components/Common/DataGridDetailDevExpr';
import {
  showError,
  closeAlert
} from "../../redux/actions/AlertActions";
import './MeusCardsVendedorDevExpr.css';

const useStyles = makeStyles((theme) => ({
  fab: {
    position: 'absolute',
    bottom: theme.spacing(2),
    right: theme.spacing(2),
  },
  fabGreen: {
    color: theme.palette.common.white,
    backgroundColor: green[500],
    '&:hover': {
      backgroundColor: green[600],
    },
  },
  appBar: {
    position: 'relative',
  },
  title: {
    marginLeft: theme.spacing(2),
    flex: 1,
  }
}));

const MeusCardsVendedorDevExpr = props => {
    const classes = useStyles();
    const alertRef = useRef(null);
    const [page, setPage] = useState(0);
    const [rowsPerPage, setRowsPerPage] = useState(100);
    const[dias, setDias] = useState(15);
    const [detailOpen, setDetailOpen] = React.useState(false);
    const [cards, setCards] = useState([]);
    const [initialCards, setInitialCards] = useState([]);
    const[openDialog, setOpenDialog] = useState(false);
    const[dialogTitle, setDialogTitle] = useState('');
    const[linkCardId, setLinkCardId] = useState(0);
    const[openAlert, setOpenAlert] = useState(false);

    const handleDias = (event, newDias) => {
        event.preventDefault();
        setDias(newDias);
    };

  const handleCloseDialog = () => {
    setOpenDialog(false);
  };

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

  const showError = (text) => {
    props.showError(text);
    alertRef.current.focus();
  }

  useEffect(() =>   {
    const loadCards = async (vendId, d) => {
        let cardsData = await restService.getLinkCardsCabecalhoVendedor(vendId, d, showError);
        if (cardsData)  {
          let data = cardsData.map(elem => {
            let e = {...elem};
            //e.DtInicioVigenciaStr = (new Intl.DateTimeFormat('pt-BR')).format(elem.DtInicioVigencia);
            e.Key = elem.Id;  
            e.key = elem.Id;  
              return e;
          });
          console.log(data);
          setCards(Array.from(data));
          setInitialCards(Array.from(data));
        }
    }

    setCards([]);
    loadCards(3, dias);
  },
  [dias]);

  return (
    <div>
        {
        props.alert.show &&
        <div id='alert' tabIndex='-1' ref={alertRef} >
          <Alert
            onClose={ () => { 
                props.closeAlert();
            } }
            severity={props.alert.message.severity}
          >
            {props.alert.message.text}
          </Alert>
        </div>
      }
      <ToggleButtonGroup value={dias} onChange={handleDias} exclusive aria-label="text formatting">
            <ToggleButton key={15} value={15} aria-label="bold">
                15
            </ToggleButton>
            <ToggleButton key={30} value={30} aria-label="bold">
                30
            </ToggleButton>
            <ToggleButton key={60} value={60} aria-label="bold">
                60
            </ToggleButton>
            <ToggleButton key={90} value={90} aria-label="bold">
                90
            </ToggleButton>
            <ToggleButton key={120} value={120} aria-label="bold">
                120
            </ToggleButton>
        </ToggleButtonGroup>

        <DataGrid id="grid-container"
          dataSource={cards}
          keyExpr="Id"
          noDataText=''
          showBorders={true}
        >
          <Sorting mode="single"/> {/* or "multiple" | "none" */}
          <Column dataField="Key" width={60} caption="Número" allowSorting={true} />
          <Column dataField="Nome" width={120} allowSorting={true} allowResizing={true} />
          <Column dataField="DtInicioVigencia" width={60} allowSorting={true} caption="Início" dataType="date" format="dd/MM/yy"/>
          <Column dataField="DtFinalVigencia" width={60} allowSorting={true} caption="Final" dataType="date" format="dd/MM/yy"/>
          <Column dataField="Texto" width={360} caption="Mensagem" allowSorting={true} allowResizing={true} />
          <Column dataField="ClientesCount" width={60} caption="Clientes" alignment='right' allowSorting={true}/>
          <Column dataField="ProdutosCount" width={60} caption="Produtos" alignment='right' allowSorting={true}/>
          <Column dataField="PedidosCount" width={60} caption="Pedidos" alignment='right' allowSorting={true}/>
          <Column dataField="ValorTotalPedidos" width={240} caption="Valor" alignment='right' format="#,##0.00" allowSorting={true}/>          
          
          <MasterDetail
            enabled={true}
            component={DataGridDetailDevExpr}
          />
        </DataGrid>

      <Dialog fullScreen open={openDialog} onClose={handleCloseDialog} TransitionComponent={Transition}>
        <AppBar className={classes.appBar}>
          <Toolbar>
            <IconButton alignment="right" edge="start" color="inherit" onClick={handleCloseDialog} aria-label="close">
              <CloseIcon />
            </IconButton>
            <Typography variant="h6" className={classes.title}>
              {dialogTitle}
            </Typography>
            {/* <Button autoFocus color="inherit" onClick={handleCloseDialog}>
              save
            </Button> */}
          </Toolbar>
        </AppBar>
          <List>
            <ListItem button>
              <ListItemText primary="Phone ringtone" secondary="Titania" />
            </ListItem>
            <Divider />
            <ListItem button>
              <ListItemText primary="Default notification ringtone" secondary="Tethys" />
            </ListItem>
          </List>        {
          // openDialog &&
          // <LojaVirtual linkCardId={linkCardId} />
        }
      </Dialog>

    </div>
  );
}

const mapStateToProps = store => ({
  alert: store.alert,
  showError: PropTypes.func.isRequired,
  closeAlert: PropTypes.func.isRequired
});

export default withStyles(
  {},
  { withTheme: true }
)(connect(mapStateToProps, { showError, closeAlert })(MeusCardsVendedorDevExpr));
