import React, { useEffect, useState } from 'react';
import { Alert, ToggleButton, ToggleButtonGroup } from '@material-ui/lab';
import { Button, Chip, FormControl, FormGroup, Grid, Icon, InputLabel, MenuItem, Select, TextField, Typography } from '@material-ui/core';
import { DataGrid } from '@material-ui/data-grid';
import { withRouter } from "../../../node_modules/react-router-dom";
import { connect } from "react-redux";
import ZCardList from '../../components/Cards/ZCardList';
import restService from '../../services/linkcards/tenantRest';
import { makeStyles, useTheme, withStyles } from '@material-ui/core/styles';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import ColorSwitch from '../../components/Cards/ColorSwitch';
import PropTypes from "prop-types";
import {
  getClientsFiltered,
  resetClientsFiltered,
  selectClient,
  setUmCliente
} from "../../redux/actions/CardsActions";

const useStyles = makeStyles((theme) => ({
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
    maxWidth: 400,
  },
  tamNumeros: {
    width: 300,
  },
  button: {
    margin: theme.spacing(1),
  },
  chips: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  chip: {
    margin: 2,
  },
  grid: {
    '& .grid.heading': {
      fontSize: theme.typography.pxToRem(15),
      fontWeight: theme.typography.fontWeightRegular,
      fontWeight: 'bold'
    },
    '& .grid.body': {
      fontSize: theme.typography.pxToRem(10),
      fontWeight: theme.typography.fontWeightRegular,
      fontWeight: 'normal'
    }
  },
}));

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;

const MakeCardsStep2 = props => {
  const {
    clientsFiltered,
    clientsSelected,
    getClientsFiltered,
    resetClientsFiltered,
    selectClient,
    setUmCliente,
    showMessage
    // container,
    //theme,
    // settings,
    // user
  } = props;
  const classes = useStyles();
  const theme = useTheme();
  const [mercados, setMercados] = useState([]);
  const [mercadosSel, setMercadosSel] = useState([]);
  const [partName, setPartName] = useState('');
  const [phoneStart, setPhoneStart] = useState('');
  const [status, setStatus] = useState('Neutro');
  const [rows, setRows] = useState([]);
  const handleMercados = (event, newMercados) => {
    event.preventDefault();
    setMercadosSel(newMercados);
  };

  useEffect(() => {
    if (clientsFiltered) {
      if (status == 'Pesquisando') {
        // to do: selecionar todos os clientes...
        setRows(clientsFiltered);
        console.log(clientsFiltered);
        setStatus('Finalizado');
      }
    }
    else {
      setStatus('Neutro');
    }
  },
    [clientsFiltered]);

  useEffect(() => {
    if (status == 'Finalizado') {
      resetClientsFiltered();
    }
  },
    [mercadosSel, partName, phoneStart]);

  const cols = [
    { field: 'CodigoErp', headerName: 'Cod', hide: false, width: 60 },
    { field: 'Nome', headerName: 'Nome', hide: false, width: 120 },
    { field: 'WaNumber', headerName: 'WhatsApp', hide: false, width: 180 },
    { field: 'MercadoConsumidorNome', headerName: 'Situação', hide: false, width: 180 },
    { field: 'Cpf_Cnpj', headerName: 'CPF/CNPJ', hide: false, width: 120 },
    { field: 'VendedorNome', headerName: 'Vendedor', hide: false, width: 120 },
    { field: 'UsuarioUserName', headerName: 'Usuário', hide: false, width: 180 },
  ];

  const pesquisar = (event) => {
    event.preventDefault();
    setStatus('Pesquisando');
    
    if (mercadosSel.length > 0) {
      let pesqParams = {
        MercadosList: mercadosSel,
        PartName: partName,
        WaNumberIni: phoneStart
      };
      console.log(pesqParams);
      getClientsFiltered(pesqParams);
    }
    else {
      showMessage('Filtro atual não retornou Clientes.', 'warning');
    }
  };

  useEffect(() => {
    async function loadDadosMestres() {
      
      Promise.all([
        restService.getMercados(),
      ])
        .then(function (results) {
          console.log(results);
          setMercados(Array.from(results[0].data));
        })
        .catch((error) => {  showMessage(error.response.data, 'error'); }
        );

    }
    loadDadosMestres();
  },
    []);

  return (
    <Grid className="filter-container">
      <Grid className="filter-option">
        {
          mercados.length > 0 &&
          <ToggleButtonGroup className="steps-client" value={mercadosSel} onChange={handleMercados} aria-label="text formatting">
            {
              mercados.map((mercado) =>
              (
                <ToggleButton key={mercado.Value} value={mercado.Value} aria-label="bold">
                  {mercado.Text}
                </ToggleButton>
              )
              )
            }
          </ToggleButtonGroup>
        }

        <TextField
          autoFocus
          margin="normal"
          label="Parte do nome"
          value={partName}
          onChange={event => setPartName(event.target.value)}
          //type="number"
          fullWidth
        />
        <TextField
          margin="dense"
          label="Telefone iniciando por..."
          value={phoneStart}
          onChange={event => setPhoneStart(event.target.value)}
          type="number"
          fullWidth
        />


        {/* <Input
                    className={classes.input}
                    //alignContent='right'
                    value={fator}
                    margin="dense"
                    //onChange={handleInputChange}
                    //onBlur={handleBlur}
                    inputProps={{
                        step: 10,
                        min: 0,
                        max: 100,
                        type: 'text',
                        'aria-labelledby': 'input-slider',
                    }}
                /> */}


        {
          //mercadosSel.length > 0 &&
          <div className="button-search">
            <Button
              variant="contained"
              color="primary"
              className={classes.button}
              onClick={(event) => pesquisar(event)}
              endIcon={<Icon>search</Icon>}
            >
              Pesquisar
                </Button>
          </div>
        }
      </Grid>
      <Grid className="filter-content">
        {
          clientsFiltered && clientsFiltered.length > 0 &&
          <div className="classesgrid">
          <DataGrid
    
            //{...data}
            rows={rows}
            columns={cols}
            checkboxSelection={true}
            autoHeight={true}
            hideFooterPagination={true}
            hideFooterSelectedRowCount={true}
            disableSelectionOnClick={false}
            hideFooterRowCount={true}
            hideFooter={true}
            autoHeight={true}
            columnBuffer={0}
            onSelectionChange={(newSelection) => {
              
              if (newSelection && newSelection.rows && newSelection.rows.length > 0) {
                selectClient(newSelection.rows.map(elem => elem.Key));
                if (newSelection.rows.length === 1) {
                  setUmCliente(newSelection.rows[0].Nome, newSelection.rows[0].WaNumber);
                }
                else {
                  setUmCliente('', '');
                }
              }
              console.log(newSelection);
            }}
          />
          </div>
        }
        {/* {
                clientsFiltered && clientsFiltered.length == 0 &&
                <Typography align='center' gutterBottom variant="h5" component="h2">
                    {'Filtro atual não retornou Clientes.'} 
                </Typography>
                } */}
      </Grid>
    </Grid>
  );
}
const mapStateToProps = store => ({
  clientsFiltered: store.cards.clientsFiltered,
  clientsSelected: store.cards.clientsSelected,
  getClientsFiltered: PropTypes.func.isRequired,
  resetClientsFiltered: PropTypes.func.isRequired,
  selectClient: PropTypes.func.isRequired,
  setUmCliente: PropTypes.func.isRequired
});

export default withStyles(
  {},
  { withTheme: true }
)(connect(mapStateToProps, { getClientsFiltered, resetClientsFiltered, setUmCliente, selectClient })(MakeCardsStep2));
