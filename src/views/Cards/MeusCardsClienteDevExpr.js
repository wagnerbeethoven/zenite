import React, { useEffect, useState, useRef } from 'react';
import ReactDOM from 'react-dom'
import { AppBar, Button, Dialog, Divider, Icon, IconButton, List, ListItem, ListItemText, Slide, Toolbar, Tooltip, Typography } from '@material-ui/core';
import { makeStyles, useTheme, withStyles } from '@material-ui/core/styles';
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { SignalCellular0Bar, SignalCellular1Bar, SignalCellular2Bar, SignalCellular3Bar, SignalCellular4Bar } from "@material-ui/icons"
import DataGrid,
{
  Column,
  Sorting,
  MasterDetail
} from 'devextreme-react/data-grid';
import { Alert } from '@material-ui/lab';
import { useLocation } from "react-router-dom";
import ZCardList from '../../components/Cards/ZCardList';
import restService from '../../services/linkcards/cardsRest';
import SaveIcon from '@material-ui/icons/Save';
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import KeyboardArrowRight from '@material-ui/icons/KeyboardArrowRight';
import { green } from '@material-ui/core/colors';
import TableLayout from '../../components/Common/TableLayout';
import { ToggleButton, ToggleButtonGroup } from '@material-ui/lab';
import CloseIcon from '@material-ui/icons/Close';
import LojaVirtual from './LojaVirtual';
import 'react-data-grid/dist/react-data-grid.css';
import { DataGridDetailDevExpr } from '../../components/Common/DataGridDetailDevExpr';
import Send from '@material-ui/icons/Send';
import {
  showError,
  closeAlert
} from "../../redux/actions/AlertActions";

const useStyles = makeStyles((theme) => ({
  fab: {
    position: 'absolute',
    bottom: theme.spacing(2),
    right: theme.spacing(2),
  },
  fabGreen: {
    color: theme.palette.common.white,
    backgroundColor: green[500],
    '&:hover': {
      backgroundColor: green[600],
    },
  },
  appBar: {
    position: 'relative',
  },
  title: {
    marginLeft: theme.spacing(2),
    flex: 1,
  }
}));

const MeusCardsClienteDevExpr = props => {
    const classes = useStyles();
    const [page, setPage] = useState(0);
    const [rowsPerPage, setRowsPerPage] = useState(100);
    const[dias, setDias] = useState(15);
    const [detailOpen, setDetailOpen] = React.useState(false);
    const [cards, setCards] = useState([]);
    const [initialCards, setInitialCards] = useState([]);
    const[openDialog, setOpenDialog] = useState(false);
    const[dialogTitle, setDialogTitle] = useState('');
    const[linkCardId, setLinkCardId] = useState(0);
    const[openAlert, setOpenAlert] = useState(false);

    const handleDias = (event, newDias) => {
        event.preventDefault();
        setDias(newDias);
    };

  const handleCloseDialog = () => {
    setOpenDialog(false);
  };

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

  const showError = (text) => {
    props.showError(text);
    //alertRef.current.focus();
  }

  useEffect(() =>   {
    const loadCards = async (cliId, d) => {
        let cardsData = await restService.getLinkCardsCabecalhoCliente(cliId, d, showError);
        if (cardsData)  {
          let data = cardsData.map(elem => {
            let e = {...elem};
            //e.DtInicioVigenciaStr = (new Intl.DateTimeFormat('pt-BR')).format(elem.DtInicioVigencia);
            e.Key = elem.Id;  
            e.key = elem.Id;  
            return e;
          });
          console.log(data);
          setCards(Array.from(data));
        }
    }
    setCards([]);
    loadCards(1, dias);
  },
  [dias]);

  const renderCellProgress = (metaData) => {
    let status = metaData.data['Status'];
    let key = metaData.data['GuidMsg'];
    let phone = metaData.data['WaNumber'];
    let newPhone = '';
    
    for (let i =0; i < phone.length; i++) {
      newPhone += isNaN(phone[i]) ? '' : phone[i];
    }
    //let newPhone = phone.map(e => isNaN(e) ? '' : e);
    let textMsg = metaData.data.Texto.replace('~{Cliente.Nome}', 'Cliente');
    let statusCaption = '';
    let bar = 0;
    let icon = <></>;
    if (metaData.data['ProdutosCompradosCount'] > 0)  {
      bar = 4;
      statusCaption = 'Parabéns!! Cliente positivado.'
      icon = (
        <Icon color=''>
          <SignalCellular4Bar />
        </Icon>
      );
    }
    else  {
      switch(status)  {
        case 'WaitingConfirm':
          bar = 0;
          icon = (
            <Icon color='disabled'>
              <SignalCellular0Bar />
            </Icon>
          );
          statusCaption = 'Enviada para o WA...'
          break;
        case 'Delivery':
          bar = 1;
          statusCaption = 'Recebida pelo WA...'
          icon = (
            <Icon color='inherit'>
              <SignalCellular1Bar />
            </Icon>
          );
          break;
        case 'RECEIVED':
          bar = 2;
          statusCaption = 'Recebida pelo cliente...'
          icon = (
            <Icon color='secondary'>
              <SignalCellular2Bar />
            </Icon>
          );
          break;
        case 'READ':
          bar = 3;
          statusCaption = 'Lida pelo cliente...'
          icon = (
            <Icon color='primary'>
              <SignalCellular3Bar />
            </Icon>
          );
          break;
        default:
          bar = 0;
          icon = (
            <Icon color='default'>
              <SignalCellular0Bar />
            </Icon>
          );
        }
      }
    return  (
      <div>
        <Tooltip title={statusCaption}>
          {icon}
        </Tooltip>
        <Tooltip  title={`Reenviar Link Cards - ${key}`}>
          <IconButton
            onClick={(event => {
                event.preventDefault();
                window.open(`https://api.whatsapp.com/send?phone=${newPhone}&text=${textMsg}  \n\nzenite.inf.br:33018/wa?guid=${key}`);
            })}
          >
            <Icon
              fontSize='small'
            ><Send/></Icon>
          </IconButton>                
        </Tooltip>
      </div>
    );
  }

  return (
    <div>
        <DataGrid id="grid-container"
          dataSource={props.data}
          keyExpr="Key"
          noDataText=''
          showBorders={true}
        >
          <Sorting mode="single"/> {/* or "multiple" | "none" */}
          <Column dataField="Key" width={90} caption="Número" allowSorting={true} />
          <Column dataField="Status" caption="" cellRender={renderCellProgress} />
          <Column dataField="Nome" width={120} allowSorting={true} allowResizing={true} />
          <Column dataField="Texto" width={360} caption="Mensagem" allowSorting={true} allowResizing={true} />
          <Column dataField="DhEnvio" width={120} dataType="datetime" format="dd/MM/yy HH:mm" caption="Enviada"/>
          <Column dataField="DeliveryEvent" width={120} dataType="datetime" format="dd/MM/yy HH:mm" caption="Entregue" />
          <Column dataField="ReceivedEvent" width={120} dataType="datetime" format="dd/MM/yy HH:mm" caption="Recebida" />
          <Column dataField="ReadEvent" width={120} dataType="datetime" format="dd/MM/yy HH:mm" caption="Lida" />
          <Column dataField="ProdutosCardsCount" width={60} caption="Produtos" alignment='right' allowSorting={true}/>
          <Column dataField="PedidosCount" width={60} caption="Pedidos" alignment='right' allowSorting={true}/>
          <Column dataField="ValorTotalPedidos" width={240} caption="Valor" alignment='right' format="#,##0.00" allowSorting={true}/>          
          
          {/* <MasterDetail
            enabled={true}
            component={DataGridDetailDevExpr}
          /> */}
        </DataGrid>

        {/* <Dialog fullScreen open={this.state.openDialog} onClose={this.handleCloseDialog} TransitionComponent={Transition}>
            <AppBar>
            <Toolbar>
                <IconButton edge="start" color="inherit" onClick={this.handleCloseDialog} aria-label="close">
                <CloseIcon />
                </IconButton>
                <Typography variant="h6">
                    {this.state.data.Key}
                </Typography>
            </Toolbar>
            </AppBar>
            <div id='lojaVirtualContainer'>
            </div>
        </Dialog> */}

    </div>
  );
}

const mapStateToProps = store => ({
  alert: store.alert,
  showError: PropTypes.func.isRequired,
  closeAlert: PropTypes.func.isRequired
});

export default withStyles(
  {},
  { withTheme: true }
)(connect(mapStateToProps, { showError, closeAlert })(MeusCardsClienteDevExpr))
