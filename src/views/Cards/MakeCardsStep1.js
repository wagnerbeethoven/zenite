import React, { Fragment, useEffect, useState } from 'react';
import { ToggleButton, ToggleButtonGroup, TreeItem, TreeView } from '@material-ui/lab';
import { Accordion, AccordionSummary, AccordionDetails, Button, Checkbox, Chip, FormControl, FormControlLabel, FormGroup, Grid, Icon, Input, InputLabel, MenuItem, Select, Typography } from '@material-ui/core';
import { withRouter } from "../../../node_modules/react-router-dom";
import { connect } from "react-redux";
import ZCardList from '../../components/Cards/ZCardList';
import restService from '../../services/linkcards/produtoRest';
import { makeStyles, useTheme, withStyles } from '@material-ui/core/styles';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import ColorSwitch from '../../components/Cards/ColorSwitch';
import PropTypes from "prop-types";


import {
  getProductFiltered,
  resetProductFiltered,
  setLoading,
  selectProduct
} from "../../redux/actions/CardsActions";

const useStyles = makeStyles((theme) => ({
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
    maxWidth: 400,
  },
  tamNumeros: {
    width: 300,
  },
  treeView: {
    height: 216,
    flexGrow: 1,
    maxWidth: 400,
  },
  button: {
    margin: theme.spacing(1),
  },
  chips: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  chip: {
    margin: 2,
  },
}));

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const ColecoesMenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 400,
    },
  },
};

function colecoesGetStyles(key, colecoes, theme) {
  return {
    fontWeight:
      colecoes.findIndex(e => e.Key == key) === -1
        ? theme.typography.fontWeightRegular
        : theme.typography.fontWeightMedium,
  };
};

const loadingImgFilename = '/assets/images/loading.gif';

const MakeCardsStep1 = props => {
  const {
    productsFiltered,
    productsSelected,
    getProductFiltered,
    resetProductFiltered,
    selectProduct,
    showMessage
    // container,
    //theme,
    // settings,
    // user
  } = props;
  const classes = useStyles();
  const theme = useTheme();
  const [marcas, setMarcas] = useState([]);
  const [marcasSel, setMarcasSel] = useState([]);
  //const [allTamanhosCheck, setAllTamanhosCheck] = useState(false);
  const [tamNum, setTamNum] = useState([]);
  const [numerosSel, setNumerosSel] = useState([]);
  const [tamNaoNum, setTamNaoNum] = useState([]);
  const [tamanhosSel, setTamanhosSel] = useState([]);
  const [hieraquiaGrpSub, setHieraquiaGrpSub] = useState([]);
  const [allGruposSubgrupos, setAllGruposSubgrupos] = useState([]);
  const [tvwNodeSel, setTvwNodeSel] = useState([]);
  const [cores, setCores] = useState([]);
  const [allCores, setAllCores] = useState(false);
  const [coresSel, setCoresSel] = useState(null);
  const [colecoes, setColecoes] = useState([]);
  const [colecoesSel, setColecoesSel] = useState([]);
  const [status, setStatus] = useState('Neutro');
  const [loadingMarcas, setLoadingMarcas] = useState(true);
  const [loadingTamanhos, setLoadingTamanhos] = useState(true);
  const [loadingHierarquia, setLoadingHierarquia] = useState(true);
  const [loadingColecoes, setLoadingColecoes] = useState(true);
  const [loadingCores, setLoadingCores] = useState(true);
  const ordemTamanhos = ['UN', 'PP', 'P', 'M', 'G', 'XG'];

  const renderTreeView = (levels) => {
    let grupos = [];
    let grupo = 0
    for (let i = 0; i < levels.length; i++) {
      if (levels[i].GrpId != grupo) {
        grupo = levels[i].GrpId;
        grupos.push(grupo);
      }
    }
    return grupos.map((elem) => {
      return renderGrupo(levels.filter(e => e.GrpId == elem));
    });
  };

  const handleMarcas = (event, newMarcas) => {
    event.preventDefault();
    setMarcasSel(newMarcas);
  };

  const handleNumeros = (event, newValue) => {
    event.preventDefault();
    setNumerosSel(newValue);
    //setAllTamanhosCheck(newValue.length === tamNum.length && tamanhosSel.length === tamNaoNum.length);
  };

  const handleTamanhos = (event, newTamanhos) => {
    event.preventDefault();
    setTamanhosSel(newTamanhos);
    //setAllTamanhosCheck(newTamanhos.length === tamNaoNum.length && numerosSel.length === tamNum.length);
  };

  const colecoesHandleChange = (event) => {
    setColecoesSel(event.target.value);
  };

  useEffect(() => {
    if (productsFiltered) {
      if (status == 'Pesquisando') {
        let prods = [];
        
        for (let i = 0; i < productsFiltered.length; i++) {
          for (let j = 0; j < productsFiltered[i].TamanhoCorList.length; j++) {
            prods.push(productsFiltered[i].TamanhoCorList[j]);
          }
        }
        selectProduct(true, prods);
        console.log(prods);
        setStatus('Finalizado');
        setTimeout(() => {
          console.log({ objeto: 'productsSelected', data: productsSelected });
        }, 400);
      }
    }
    else {
      setStatus('Neutro');
    }
  },
    [productsFiltered]);

  useEffect(() => {
    if (status == 'Finalizado') {
      resetProductFiltered();
      setStatus('Neutro');
    }
    //if (numerosSel.length == )
  },
    [marcasSel, tvwNodeSel, numerosSel, tamanhosSel, colecoesSel]);

  useEffect(() => {
    async function loadDadosMestres() {
      
      Promise.all([
        restService.getMarcas(showError),
        restService.getTamanhosNumericos(showError),
        restService.getTamanhosNaoNumericos(showError),
        restService.getHierarquiaGrupoSubGrupo(showError),
        restService.getCores(showError),
        restService.getColecoes(showError)
      ])
        .then(function (results) {
          console.log(results);
          if (results[0]) {
            setMarcas(Array.from(results[0].data));
            setLoadingMarcas(false);
          }
          if (results[1]) {
            setTamNum(Array.from(results[1].data));
            setLoadingTamanhos(false);
          }
          if (results[2]) {
            setTamNaoNum(Array.from(results[2].data.sort((a, b) => {
              if (ordemTamanhos.indexOf(a) < ordemTamanhos.indexOf(b)) {
                return -1;
              }
              if (ordemTamanhos.indexOf(a) > ordemTamanhos.indexOf(b)) {
                return 1;
              }
              return 0;
            })));
            setLoadingTamanhos(false);
          }
          if (results[3]) {
            setHieraquiaGrpSub(Array.from(results[3].data));
            setAllGruposSubgrupos(getAllGruposSubgrupos(Array.from(results[3].data)));
            setLoadingHierarquia(false);
          }
          if (results[4]) {
            setCores(Array.from(results[4].data));
            let selCores = {};
            for (let i = 0; i < results[4].data.length; i++) {
              selCores[results[4].data[i].Text] = false;
            }
            setCoresSel(selCores);
            setLoadingCores(false);
          }
          if (results[5]) {
            setColecoes(Array.from(results[5].data));
            setLoadingColecoes(false);
          }
          setLoading(false);
        })
        .catch((error) => {  showMessage(error.response.data, 'error'); }
        );

    }

    setLoading(true);
    loadDadosMestres();
  },
    []);

  const pesquisar = (event) => {
    //setShowMsg(false);      
    event.preventDefault();
    setStatus('Pesquisando');
    let allTam = numerosSel.map((elem) => elem.toString());
    allTam = allTam.concat(tamanhosSel);

    
    let coresSelected = [];
    cores.forEach(value => {
      if (coresSel[value.Text]) {
        coresSelected.push(value.Text);
      }
    });

    if (marcasSel.length > 0 && tvwNodeSel.length > 0 && (numerosSel.length > 0 || tamanhosSel.length > 0) && colecoesSel.length > 0) {
      let pesqParams = {
        MarcasList: marcasSel,
        TamanhosList: allTam,
        GruposList: tvwNodeSel.filter(e => e.indexOf('Grp_') >= 0).map(elem => Number.parseInt(elem.replace('Grp_', ''))),
        SubGruposList: tvwNodeSel.filter(e => e.indexOf('Sub_') >= 0).map(elem => Number.parseInt(elem.replace('Sub_', ''))),
        CoresList: coresSelected,
        ColecoesList: colecoesSel.map(elem => elem.Key)
      };

      console.log(pesqParams);
      getProductFiltered(pesqParams);
    }
    else {
      showMessage('Filtro atual não retornou Produtos.', 'warning');
    }
  };

  const showError = (msg) => {
    showMessage(msg, 'error');
  }

  const corHandleChange = (event, name, checked) => {
    
    event.preventDefault();
    if (checked)  {
      let all = true;
      for (let i = 0; i < cores.length; i++)  {
        if (cores[i].Text !== name) {
          all = all && coresSel[cores[i].Text];
        }
        if (!all) {
          break;
        }
      }
      setAllCores(all);
    }
    else  {
      setAllCores(false);
    }
    setCoresSel({ ...coresSel, [name]: checked });
  };

  const allTamanhosClick = (event) => {
    
    if (event.target.checked) {
      setNumerosSel(tamNum);
      setTamanhosSel(tamNaoNum);
    }
    else {
      setNumerosSel([]);
      setTamanhosSel([]);
    }
    //setAllTamanhosCheck(event.target.checked);
  };

  const allGruposChange = (event) => {
    if (event.target.checked) {
      setTvwNodeSel(allGruposSubgrupos);
    }
    else {
      setTvwNodeSel([]);
    }
    //setAllGruposCheck(event.target.checked);
    // setTimeout(() => {
    //   renderTreeView(hieraquiaGrpSub);
    // }, 90);
  };

  const allCoresClick = (event) => {
    let result = {};
    
    cores.map(cor => result[cor.Text] = event.target.checked);
    setAllCores(event.target.checked);
    setCoresSel(result);
};

  const getAllGruposSubgrupos = (grpSubHierq) => {
    let result = [];
    grpSubHierq.forEach(elem => {
      if (result.indexOf(`Grp_${elem.GrpId}`) === -1) {
        result.push(`Grp_${elem.GrpId}`);
      }
      result.push(`Sub_${elem.SubId}`);
    });
    return result;
  };

  const checkGrpSubClick = (event, value) => {
    event.preventDefault();
    event.stopPropagation();
    
    let isGroup = value.indexOf('Grp_') === 0;
    let sels = tvwNodeSel.slice(0);
    if (event.target.checked) {
      if (tvwNodeSel.indexOf(value) === -1) {
        sels.push(value);
        if (isGroup) {
          let subs = hieraquiaGrpSub.filter(e => e.GrpId == value.substring('Grp_'.length));
          subs.forEach(elem => {
            if (sels.indexOf(`Sub_${elem.SubId}`) === -1) {
              sels.push(`Sub_${elem.SubId}`);
            }
          });
        }
        else {
          let currentChild = hieraquiaGrpSub.find(e => e.SubId == value.substring('Sub_'.length));
          let children = hieraquiaGrpSub.filter(e => e.GrpId == currentChild.GrpId);

          let allCheck = true;
          children.forEach(elem => {
            allCheck = allCheck && (sels.indexOf(`Sub_${elem.SubId}`) >= 0 || elem.SubId === currentChild.SubId);
          });
          if (allCheck) {
            sels.push(`Grp_${currentChild.GrpId}`);
          }
        }
      }
    }
    else {
      let index = sels.indexOf(value);
      if (index >= 0) {
        sels.splice(index, 1);
      }

      if (isGroup) {
        let subs = hieraquiaGrpSub.filter(e => e.GrpId == value.substring('Grp_'.length));
        subs.forEach(elem => {
          let idx = sels.indexOf(`Sub_${elem.SubId}`);
          if (idx >= 0) {
            sels.splice(idx, 1);
          }
        });
      }
      else {
        let currentChild = hieraquiaGrpSub.find(e => e.SubId == value.substring('Sub_'.length));
        index = sels.indexOf(`Grp_${currentChild.GrpId}`);
        if (index >= 0) {
          sels.splice(index, 1);
        }
      }
    }

    setTimeout(() => {
      setTvwNodeSel(sels);
    }, 60);
  };

  function renderGrupo(itens) {
    let first = itens[0];
    return (
      <TreeItem
        key={`Grp_${first.GrpId}`}
        nodeId={`Grp_${first.GrpId}`}
        label={
          <FormControlLabel
            control={
              <Checkbox
                checked={tvwNodeSel.indexOf(`Grp_${first.GrpId}`) >= 0}
                size='small'
                onClick={(event) => checkGrpSubClick(event, `Grp_${first.GrpId}`)}
                onFocus={(event) => event.stopPropagation()}
              // checked={state.checkedB}
              // onChange={handleChange}
              // name="checkedB"
              // color="primary"
              />
            }
            onClick={(event) => event.stopPropagation()}
            onFocus={(event) => event.stopPropagation()}
            label={first.GrpNome}
          />}

      >
        {renderSubGrupo(itens)}
      </TreeItem>
    );
  }

  function renderSubGrupo(itens) {
    return itens.map((item) => (
      <TreeItem
        key={`Sub_${item.SubId}`}
        nodeId={`Sub_${item.SubId}`}
        label={
          <FormControlLabel
            control={
              <Checkbox
                checked={tvwNodeSel.indexOf(`Sub_${item.SubId}`) >= 0}
                size='small'
                onClick={(event) => checkGrpSubClick(event, `Sub_${item.SubId}`)}
                onFocus={(event) => event.stopPropagation()}
              // checked={state.checkedB}
              // onChange={handleChange}
              // name="checkedB"
              // color="primary"
              />
            }
            label={item.SubNome}
          />
        }
      />
    ));
  }

  return (
    <Grid className="filter-container">
      <Grid className="filter-option" item md={3} xs={12}>
        <Grid item xs={12}>
          {
            loadingMarcas &&
            <div className="loading-custom">
              <img
                src={`${process.env.PUBLIC_URL + loadingImgFilename}`}
              />
              <Typography className={classes.heading}>Carregando Marcas...</Typography>
            </div>
          }

          {
            marcas.length > 0 &&
            <section className="filter-company">
              <div className="section-title">Selecione a marca</div>
              <ToggleButtonGroup className="toggle-group" value={marcasSel} onChange={handleMarcas} aria-label="text formatting">
                {
                  marcas.map((marca) =>
                  (
                    <ToggleButton key={marca.Value} value={marca.Value} aria-label="bold">
                      {marca.Text}
                    </ToggleButton>
                  )
                  )
                }
              </ToggleButtonGroup>

            </section>


          }
        </Grid>
        <Grid item xs={12}>
          <Accordion>
            <AccordionSummary
              expandIcon={<ExpandMoreIcon />}
            // aria-controls="panel1a-content"
            // id="panel1a-header"
            >
              {
                loadingTamanhos &&
                <div className="loading-custom">
                  <img
                    src={`${process.env.PUBLIC_URL + loadingImgFilename}`}
                  />
                  <Typography className={classes.heading}>Carregando Tamanhos...</Typography>
                </div>
              }

              {
                !loadingTamanhos &&
                <FormControlLabel
                  aria-label="Acknowledge"
                  onClick={(event) => event.stopPropagation()}
                  onFocus={(event) => event.stopPropagation()}
                  control={<Checkbox
                    size='small'
                    checked={numerosSel.length > 0 && tamanhosSel.length > 0 && tamNum.length === numerosSel.length && tamNaoNum.length === tamanhosSel.length}
                    //checked={allTamanhosCheck}
                    onChange={allTamanhosClick}

                  />
                  }
                  label="Tamanhos"
                />
              }
            </AccordionSummary>
            <AccordionDetails>
              {
                tamNum.length > 0 &&
                <ToggleButtonGroup value={numerosSel} onChange={handleNumeros} aria-label="text formatting">
                  {
                    tamNum.map((tam) =>
                    (
                      <ToggleButton key={tam} value={tam} aria-label="bold">
                        {tam}
                      </ToggleButton>
                    )
                    )
                  }
                </ToggleButtonGroup>
              }
              {
                tamNaoNum.length > 0 &&
                <ToggleButtonGroup value={tamanhosSel} onChange={handleTamanhos} aria-label="text formatting">
                  {
                    tamNaoNum.map((tam) =>
                    (
                      <ToggleButton key={tam} value={tam} aria-label="bold">
                        {tam}
                      </ToggleButton>
                    )
                    )
                  }
                </ToggleButtonGroup>
              }
            </AccordionDetails>
          </Accordion>
        </Grid>
        <Grid item xs={12}>
          <Accordion>
            <AccordionSummary
              expandIcon={<ExpandMoreIcon />}
            // aria-controls="panel1a-content"
            // id="panel1a-header"
            >
              {
                loadingHierarquia &&
                <div className="loading-custom">
                  <img
                    src={`${process.env.PUBLIC_URL + loadingImgFilename}`}
                  />
                  <Typography className={classes.heading}>Carregando Grupos e Subgrupos...</Typography>
                </div>
              }
              {
                !loadingHierarquia &&
                <FormControlLabel
                  aria-label="Acknowledge"
                  onClick={(event) => event.stopPropagation()}
                  onFocus={(event) => event.stopPropagation()}
                  control={<Checkbox
                    checked={tvwNodeSel.length > 0 && tvwNodeSel.length == allGruposSubgrupos.length}
                    onChange={(event) => allGruposChange(event)}
                    size='small'
                  />}
                  label="Grupos/Subgrupos"
                />
              }
            </AccordionSummary>
            <AccordionDetails>
              {
                hieraquiaGrpSub.length > 0 &&
                <TreeView
                  className={classes.root}
                  defaultCollapseIcon={<ExpandMoreIcon />}
                  defaultExpandIcon={<ChevronRightIcon />}
                // selected={tvwNodeSel}
                // onNodeSelect={nodeSelectHandle}
                // multiSelect={true}
                >
                  {renderTreeView(hieraquiaGrpSub)}
                </TreeView>
              }
            </AccordionDetails>
          </Accordion>
        </Grid>
        <Grid item xs={12}>
          {
            loadingColecoes &&
            <div>
              <img
                src={`${process.env.PUBLIC_URL + loadingImgFilename}`}
              />
              <Typography className={classes.heading}>Carregando Coleções...</Typography>
            </div>
          }

          {
            !loadingColecoes &&
            <section className="filter-collection">
              <FormControl className={classes.formControl}>
                {/* <InputLabel id="colecoes-label">Coleções</InputLabel> */}
                <div className="section-title">Selecione a coleção</div>
                <Select
                  className="select-collection"
                  labelId="colecoes-label"
                  id="colecoes"
                  multiple
                  label='Coleções'
                  value={colecoesSel}
                  onChange={colecoesHandleChange}
                  //input={<Input id="colecoes-select-multiple" />}
                  renderValue={(selected) => (
                    <div className={classes.chips}>
                      {selected.map((colecao) => (
                        <Chip key={colecao.Key} label={colecao.Nome} className={classes.chip} />
                      ))}
                    </div>
                  )}
                  MenuProps={ColecoesMenuProps}
                >
                  {colecoes.map((colecao) => (
                    <MenuItem key={colecao.Key} value={colecao} style={colecoesGetStyles(colecao.Key, colecoes, theme)}>
                      {colecao.Nome}
                    </MenuItem>
                  ))}
                </Select>
              </FormControl>
            </section>}
        </Grid>
        <Grid item xs={12}>
          <Accordion>
            <AccordionSummary
              expandIcon={<ExpandMoreIcon />}
            // aria-controls="panel1a-content"
            // id="panel1a-header"
            >
              {
                loadingCores &&
                <div className="loading-custom">
                  <img
                    src={`${process.env.PUBLIC_URL + loadingImgFilename}`}
                  />
                  <Typography className={classes.heading}>Carregando Cores...</Typography>
                </div>
              }
              {
                !loadingCores &&
                <FormControlLabel
                  aria-label="Acknowledge"
                  onClick={(event) => event.stopPropagation()}
                  onFocus={(event) => event.stopPropagation()}
                  control={<Checkbox
                    size='small'
                    checked={allCores}
                    //checked={allTamanhosCheck}
                    onChange={allCoresClick}
                  />
                  }
                  label="Cores"
                />
              }
            </AccordionSummary>
            <AccordionDetails>
              {
                coresSel != null &&
                <FormGroup>
                  {
                    cores.map((cor, idx) => (
                      <FormControlLabel
                        key={idx}
                        aria-label="Acknowledge"
                        onClick={(event) => event.stopPropagation()}
                        onFocus={(event) => event.stopPropagation()}
                        //onChange={(event) => console.log(event)}
                        control={
                          <Fragment>
                            <Checkbox
                              checked={coresSel[cor.Text]}
                              onChange={(event, checked) => {
                                corHandleChange(event, cor.Text, checked);
                              }}
                              size='small'
                            />
                            <Icon className="product-color-icon" fontSize='small' style={{ color: `#${cor.Value}` }}> circle</Icon>

                            {cor.Text}

                          </Fragment>
                        } />
                    ))
                  }
                </FormGroup>
              }
            </AccordionDetails>
          </Accordion>

        </Grid>
        <Grid item xs={12}>
          {
            //marcasSel.length > 0 && tvwNodeSel.length > 0 && (numerosSel.length > 0 || tamanhosSel.length > 0) && colecoesSel.length > 0 &&
            !loadingMarcas && !loadingTamanhos && !loadingHierarquia && !loadingColecoes && !loadingCores &&
            <div className="button-search">
              <Button
                variant="contained"
                color="primary"
                className={classes.button}
                onClick={(event) => pesquisar(event)}
                endIcon={<Icon>search</Icon>}
              >
                Pesquisar
                </Button>
            </div>
          }
        </Grid>
      </Grid>
      <Grid className="filter-content" item md={9} xs={12}>
        {
          productsFiltered && productsFiltered.length > 0 &&
          <ZCardList
            cards={productsFiltered}
            periodo={3600}
            isCart={false}
            cols={2}
          />
        }
        {/* {
                productsFiltered && productsFiltered.length == 0 &&
                <Typography align='center' gutterBottom variant="h5" component="h2">
                    {'Filtro atual não retornou Produtos.'} 
                </Typography>
              } */}
      </Grid>
    </Grid>
  );
}
const mapStateToProps = store => ({
  productsFiltered: store.cards.productsFiltered,
  productsSelected: store.cards.productsSelected,
  getProductFiltered: PropTypes.func.isRequired,
  setLoading: PropTypes.func.isRequired,
  resetProductFiltered: PropTypes.func.isRequired,
  selectProduct: PropTypes.func.isRequired
});

export default withStyles(
  {},
  { withTheme: true }
)(connect(mapStateToProps, { getProductFiltered, setLoading, resetProductFiltered, selectProduct })(MakeCardsStep1));

