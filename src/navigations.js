export const navigations = [
  {
    name: "Cards",
    icon: "format_list_bulleted", 
    children:[
      {
        name: "Lançamentos Primavera/Verão",
        path: "/cards/loja_virtual",
        icon: "dashboard"
      },
      {
        name: "Criar",
        path: "/linkcards",
        icon: "dashboard"
      }
    ]
  },

];
