import api from '../api'
import { PiLookup } from '../../models/common/PiLookup';
import { ZApiQrCode64ResultDto } from '../../models/tenant/ZApiQrCode64ResultDto';

export default class TenantRest {

    static async getMercados()  {
        return await api.get<PiLookup<number>[]>('api/cliente/GetMercados');
    }

    static async getQrCodeBase64()  {
        return await api.get<ZApiQrCode64ResultDto>('api/empresa/getqrcodebase64');
    }

    static async disconnect()  {
        return await api.get('api/empresa/DisconnectInstance')
            .then(response => {
                console.log(response);
                return response.data.value;
            });
    }

    static async connected()  {
        return await api.get('api/empresa/InstanceConnected')
            .then(response => {
                console.log(response);
                return response.data.connected;
            });
    }

}