import api from '../api'
import { PiLookup } from '../../models/common/PiLookup';
import { ProdutoParams } from '../../models/product/ProdutoParams';
import { ColecaoDto } from '../../models/product/ColecaoDto';
import { HierarquiaGrpSubGrp } from '../../models/product/HierarquiaGrpSubGrp';

export default class ProdutoRest    {

    static handleError(error : any, showError : Function)   {
        let msg = ProdutoRest.getErrorMessage(error);
        console.log(msg);
        showError(msg);
    }

    static getErrorMessage(error : any)  {
        if (error && error.response)    {
            let msg : string;
            if (error.response.data.ExceptionMessage)   {
                msg = error.response.data.ExceptionMessage;
            }
            else    {
                msg = error.response.data;
            }
            if (msg.substr(1, 24).toUpperCase().indexOf('HTML') >= 0)   {
                return msg;
            }
            else    {
                return `[${error.response.status}/${error.response.statusText}] - ${msg} (${error.message})`;
            }
        }
        else    {
            return 'Erro!';
        }
    }

    static async getMarcas(showError : Function) {
        try {
            return await api.get<PiLookup<number>[]>('api/produtotextil/GetMarcas');
        } catch (error) {
            ProdutoRest.handleError(error, showError);
            return false;
        }
    }

    static async getTamanhosNumericos(showError : Function) {
        try {
            return await api.get<number[]>('api/produtotextil/GetTamanhosNumericos');
        } catch (error) {
            ProdutoRest.handleError(error, showError);
            return false;
        }
    }

    static async getTamanhosNaoNumericos(showError : Function) {
        try {
            return await api.get<string[]>('api/produtotextil/GetTamanhosNaoNumericos');
        } catch (error) {
            ProdutoRest.handleError(error, showError);
            return false;
        }
    }

    static async getHierarquiaGrupoSubGrupo(showError : Function) {
        try {
            return await api.get<HierarquiaGrpSubGrp[]>('api/produtotextil/GetHierarquiaGrupoSubGrupo');
        } catch (error) {
            ProdutoRest.handleError(error, showError);
            return false;
        }
    }

    static async getCores(showError : Function) {
        try {
            return await api.get<PiLookup<string>[]>('api/produtotextil/GetCores');
        } catch (error) {
            ProdutoRest.handleError(error, showError);
            return false;
        }
    }

    static async getColecoes(showError : Function) {
        try {
            return await api.get<ColecaoDto[]>('api/produtotextil/GetColecoes?xAnos=2');
        } catch (error) {
            ProdutoRest.handleError(error, showError);
            return false;
        }
    }

    // static async pesquisarProdutos(marcas: number[],
    //     grps : number[],
    //     subGrps : number[],
    //     tamanhos: string[],
    //     cores: string[],
    //     colecoes: number[]
    // )  {
    //     let data : ProdutoParams = new ProdutoParams();
    //     data.MarcasList = marcas;
    //     data.GruposList = grps;
    //     data.SubGruposList = subGrps;
    //     data.TamanhosList = tamanhos;
    //     data.CoresList = cores;
    //     data.ColecoesList = colecoes;

    //     let response = await api.post('api/produtotextil/GetProdutosPesquisaAvancada', data);
    //     console.log(response);
    //     return response.data;
    // }

    
}