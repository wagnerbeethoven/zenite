import { ProdutoCard } from '../../models/cards/ProdutoCard';
import { ProductCart, PedidoProdutoKey } from '../../models/cards/ProductCart';
import { PedidoCabecalhoDto } from '../../models/pedido/PedidoCabecalhoDto';
import localStorageService from "../localStorageService";
import api from '../api'
import qs from 'qs';
import { Pedido } from '../../models/pedido/Pedido';
import { LinkCardsCabecalhoVendedorDto } from '../../models/cards/LinkCardsCabecalhoVendedorDto';
import { Response } from '../../models/Response';

export default class PedidoRestService{
    
    static handleError(error : any, showError : Function)   {
        let msg = PedidoRestService.getErrorMessage(error);
        console.log(msg);
        showError(msg);
}

    static getErrorMessage(error : any)  {
        if (error && error.response)    {
            let msg : string;
            if (error.response.data.ExceptionMessage)   {
                msg = error.response.data.ExceptionMessage;
            }
            else    {
                msg = error.response.data;
            }
            return `[${error.response.status}/${error.response.statusText}] - ${msg} (${error.message})`;
        }
        else    {
            return 'Erro!';
        }
    }

    static async getPedidoCabecalhoCliente(cliId: number, ultimosDias: number) {
        
        //let result : ProdutoCard[] = [];
        try {
            const response = await api.get<PedidoCabecalhoDto[]>(`api/pedido/GetRecentes?dias=${ultimosDias}`);
            
            let linkCards = (response.data as PedidoCabecalhoDto[]);
            //let linkCards2 = linkCards.map(elem => { return {...elem, DtInicioVigencia: elem.DtInicioVigencia.toLocaleDateString('pt-BR')}});
            console.log(linkCards);
            return linkCards;
        } catch (error) {
            console.log(error);
        }
    }

    static async postPedidos(pedidos : Pedido[], storageList : string[], showError : Function) {
        //let result : Response | null = null;
        try {
            let msg : string = '';
            for(let i = 0; i < pedidos.length; i++) {
                console.log(pedidos[i]);
                // 
                // let ped : any = {
                //     ...pedidos[i],
                //     //DhCreate : `${pedidos[i].DhCreate.getFullYear()}-${pedidos[i].DhCreate.getMonth() + 1}-${pedidos[i].DhCreate.getDate()} ${pedidos[i].DhCreate.getHours()}:${pedidos[i].DhCreate.getMinutes()}:${pedidos[i].DhCreate.getSeconds()}`
                //     DhCreate : `${pedidos[i].DhCreate.getFullYear()}-${pedidos[i].DhCreate.getMonth() + 1}-${pedidos[i].DhCreate.getDate()}`
                // };
                //console.log(ped);
                let response : any = await api.post('api/pedido/post', qs.stringify(pedidos[i]));
                if (response && response.error) {
                    PedidoRestService.handleError(response.error, showError);
                    return false;
                }
                let storages = storageList.filter(e => e.indexOf(`SUB_PEDIDO_${pedidos[i].LinkCardClienteKey}_`) === 0);
                storages.forEach(element => {
                    localStorageService.removeItem(element); 
                });
                // console.log(`Pedido '${response.data}' gerado com sucesso, aguardando processamento.`);
                // msg += `\nPedido '${response.data}' gerado com sucesso, aguardando processamento.`;
                console.log(response.data);
                msg += '\n' + response.data;
            }
            return msg;
        }
        catch(error)    {
            PedidoRestService.handleError(error, showError);
            return false;
        }
    }

    static async postPedido(cards : ProdutoCard[], isConsignado : boolean) {
        let result : Response | null = null;
        let metaPedidos = PedidoRestService.createPedidos(cards, isConsignado);
        let pedidos = metaPedidos.data;

        try {
            let msg : string = '';
            for(let i = 0; i < pedidos.length; i++) {
                let response = await api.post('api/pedido/post', qs.stringify(pedidos[i]));
                let storages = metaPedidos.storageList.filter(e => e.indexOf(`SUB_PEDIDO_${pedidos[i].LinkCardClienteKey}_`) == 0);
                storages.forEach(element => {
                    localStorageService.removeItem(element); 
                });
                // console.log(`Pedido '${response.data}' gerado com sucesso, aguardando processamento.`);
                // msg += `\nPedido '${response.data}' gerado com sucesso, aguardando processamento.`;
                console.log(response.data);
                msg += response.data;
            }
            result = new Response(true, msg);
        }
        catch(error)    {
            result = new Response(false, PedidoRestService.getErrorMessage(error));
        }
        return result;
    }

    static createPedidos(cards : ProdutoCard[], isConsignado : boolean) {
        let pedidos : Pedido[] = [];
        let localStorageList : string[] = [];
        let modelData : Pedido | null = null;
        cards = cards.sort((a, b) => {
            if (a.LinkCardClienteGuid < b.LinkCardClienteGuid) {
                return -1;
            }
            if (a.LinkCardClienteGuid > b.LinkCardClienteGuid) {
                return 1;
            }
            return 0;            
        });
        let guid : string = '';
        for (let i = 0; i < cards.length; i++)  {
            let elem = cards[i];

            if (guid != elem.LinkCardClienteGuid)   {
                guid = elem.LinkCardClienteGuid;
                let dhCreate : Date = new Date();
                dhCreate.setTime(Date.now());
                //let date  : Date = `${dhCreate.getFullYear()}-${dhCreate.getMonth()}-${dhCreate.getDate()} ${dhCreate.getHours()}:${dhCreate.getMinutes()}:${dhCreate.getSeconds()}`;
                modelData = new Pedido(0, dhCreate, '000000', 0, 0, true, guid, isConsignado);
                pedidos.push(modelData);
            }

            let rowsCorStorage : [] | null = localStorageService.getItem(`SUB_PEDIDO_${elem.LinkCardClienteGuid}_${elem.Referencia}`);
            if (rowsCorStorage != null)    {
                for (let j = 0; j < elem.TamanhoCorList.length; j++)    {
                    let item : ProductCart | null = null;
                    let prod = elem.TamanhoCorList[j];
                    let rowCor = rowsCorStorage.find(e => e['id'] == elem.Referencia + "-" + prod.CorNome && e['pedido_' + prod.Tamanho] > 0);
                    if (rowCor && rowCor['pedido_' + prod.Tamanho] > 0) {
                        item = new ProductCart(new PedidoProdutoKey(prod.Id, 0), elem.Referencia, elem.Nome, elem.DescricaoPrimaria, elem.DescricaoSecundaria, elem.MarcaNome, elem.ColecaoNome, rowCor['pedido_' + prod.Tamanho], prod.ValUnit);
                        if (modelData != null)  {
                            modelData.PedidoProdutoDtoSet.push(item);
                            modelData.ValorTotal += item.Qtde * item.ValUnit;
                        }
                    }
                }
                if (modelData && modelData.ValorTotal > 0)  {
                    localStorageList.push(`SUB_PEDIDO_${elem.LinkCardClienteGuid}_${elem.Referencia}`);
                }
            }
        }
        return { data: pedidos.filter(p => p.ValorTotal > 0), storageList: localStorageList };
    }

    static emptyCart(storageList : string[])    {
        for(let i = 0; i < storageList.length; i++) {
            localStorageService.removeItem(storageList[i]);
        }
    }

}