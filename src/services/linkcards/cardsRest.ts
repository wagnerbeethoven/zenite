import React from 'react';
import { ProdutoCard } from '../../models/cards/ProdutoCard';
import { ProductCart, PedidoProdutoKey } from '../../models/cards/ProductCart';
import localStorageService from "../../services/localStorageService";
import api from '../api'
import qs from 'qs';
import { Pedido } from '../../models/pedido/Pedido';
import { dateComparer } from '@material-ui/data-grid';
import { LinkCardsDto } from '../../models/cards/LinkCardsDto';
import { LinkCardProdutoKey, LinkCardProdutoDto } from '../../models/cards/LinkCardProdutoDto';
import { LinkCardClienteViewDto } from '../../models/cards/LinkCardClienteViewDto';
import { LinkCardClienteWriteDto } from '../../models/cards/LinkCardClienteWriteDto';
import { LinkCardsCabecalhoVendedorDto } from '../../models/cards/LinkCardsCabecalhoVendedorDto';
import { LinkCardsCabecalhoClienteDto } from '../../models/cards/LinkCardsCabecalhoClienteDto';
import { LinkCardMensagemKey, LinkCardMensagemDto } from '../../models/cards/LinkCardMensagemDto';
import { Response } from '../../models/Response';
import { responsiveFontSizes } from '@material-ui/core';  

export default class CardsRestService { 

    static handleError(error : any, showError : Function)   {
        let msg = CardsRestService.getErrorMessage(error);
        console.log(msg);
        showError(msg);
    }

    static getErrorMessage(error : any)  {
        if (error && error.response)    {
            let msg : string;
            if (error.response.data.ExceptionMessage)   {
                msg = error.response.data.ExceptionMessage;
            }
            else    {
                msg = error.response.data;
            }
            return `[${error.response.status}/${error.response.statusText}] - ${msg} (${error.message})`;
        }
        else    {
            return 'Erro!';
        }
    }

    static async getLinkCardsCabecalhoVendedor(vendId: number, ultimosDias: number, showError : Function) {
        
        //let result : ProdutoCard[] = [];
        try {
            const response = await api.get<LinkCardsCabecalhoVendedorDto[]>(`api/linkcard/GetLinkCardsCabecalhoVendedor?id=${vendId}&ultimosDias=${ultimosDias}`);
            
            let linkCards = (response.data as LinkCardsCabecalhoVendedorDto[]);
            //let linkCards2 = linkCards.map(elem => { return {...elem, DtInicioVigencia: elem.DtInicioVigencia.toLocaleDateString('pt-BR')}});
            console.log(linkCards);
            return linkCards;
        } catch (error) {
            CardsRestService.handleError(error, showError);
            return false;
        }
    }

    static async getLinkCardsCabecalhoCliente(cliId: number, ultimosDias: number, showError : Function) {
        
        //let result : ProdutoCard[] = [];
        try {
            const response = await api.get<LinkCardsCabecalhoClienteDto[]>(`api/Cliente/GetLinkCardsCabecalhoCliente?id=${cliId}&ultimosDias=${ultimosDias}`);
            
            let linkCards = (response.data as LinkCardsCabecalhoClienteDto[]);
            //let linkCards2 = linkCards.map(elem => { return {...elem, DtInicioVigencia: elem.DtInicioVigencia.toLocaleDateString('pt-BR')}});
            console.log(linkCards);
            return linkCards;
        } catch (error) {
            CardsRestService.handleError(error, showError);
            return false;
        }
    }

    static async getProdutosCard(cardId : number) {
        
        //let result : ProdutoCard[] = [];
        return await api.get<ProdutoCard[]>('api/linkcard/GetProdutosToCard?id=' + cardId)
            .then((response)=>{
                
                let prodCards = response.data as ProdutoCard[];
                console.log(prodCards);
                return prodCards;
            })
            .catch((error) =>{
                console.log(error);
        });
    }

    static async getClientesCard(cardId : number) {
        
        return await api.get<LinkCardClienteViewDto[]>('api/linkcard/GetClientesFromLinkCards?linkCardId=' + cardId)
            .then((response)=>{
                
                let clientesCards = response.data as LinkCardClienteViewDto[];
                console.log(clientesCards);
                return clientesCards;
            })
            .catch((error) =>{
                console.log(error);
        });
    }

    static async getProdutosCardDirect(guid : string) {
        
        return await api.get<ProdutoCard[]>('api/linkcard/openlinkcards?guid=' + guid)
            .then((response)=>{
                
                let prodCards = response.data as ProdutoCard[];
                console.log(prodCards);
                return prodCards;
            })
            .catch((error) =>{
                console.log(error);
        });
    }

    static createGuid() {
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
           var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
           return v.toString(16);
        });
     }

    static async postLinkCards(nome : string, msg : string, produtos : number[], clientes : number[], viaApi : boolean)   {
        let result : Response;
        let cards : LinkCardsDto = new LinkCardsDto(0, true, nome, nome, null, null, null, 20, 4000, viaApi);
        let cardProdKey : LinkCardProdutoKey;
        let cardProd :  LinkCardProdutoDto;
        for(let i = 0; i < produtos.length; i++)    {
            cardProdKey = new LinkCardProdutoKey(produtos[i], 0);
            cardProd = new LinkCardProdutoDto(cardProdKey, true, '', '', '', '', 1000, 0, 0);
            cards.LinkCardProdutoDtoSet.push(cardProd);
        }
        let cardCli : LinkCardClienteWriteDto;
        for(let i = 0; i < clientes.length; i++)    {
            cardCli = new LinkCardClienteWriteDto(CardsRestService.createGuid(), clientes[i], true, 3, 1);
            cards.LinkCardClienteWriteDtoSet.push(cardCli);
        }
        let cardMsg : LinkCardMensagemDto = new LinkCardMensagemDto(new LinkCardMensagemKey(1, 0), true, msg);
        cards.LinkCardMensagemDtoSet.push(cardMsg);
        try {
            if (viaApi) {
                let response = await api.post('api/linkcard/post', qs.stringify(cards));       
                console.log(`LinkCards '${response.data}' gerado com sucesso, envios em andamento.`);
                result = new Response(true, `LinkCards '${response.data}' gerado com sucesso, envios em andamento.`);
            }
            else    {
                let response = await api.post('api/linkcard/post', qs.stringify(cards));       
                //console.log(`LinkCards '${response.data}' gerado com sucesso, envios em andamento.`);
                result = new Response(true, response.data.guid);    
            }
        }
        catch(error)    {
            result = new Response(false, this.getErrorMessage(error));
        }
        return result;
    }

}
