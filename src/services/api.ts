import axios from 'axios';
import localStorageService from "./localStorageService";

function getHeader() {
    return {
        //"Content-Type": "application/json; charset=utf-8",
        "Content-Type": "application/x-www-form-urlencoded;charset=UTF-8",
        'Accept': '*/*',
        //
        //'Accept-Encoding': 'gzip, deflate, br',
        "Authorization": "Bearer " + localStorageService.getItem('jwt_token')
    };
}

const api = axios.create({
    //baseURL: "http://127.0.0.1:54019",
    baseURL: "http://zenite.inf.br:33017",
    //timeout: 20000
    headers: getHeader()
});

export default api;