import axios from "axios";
import api from './api';
import qs from 'qs';
import localStorageService from "./localStorageService";
import { RolesViewModel } from '../models/session/RolesViewModel';

class JwtAuthService {

  // Dummy user object just for the demo
  // user = {
  //   userId: '0b4749fd-eb39-43d5-8e7d-acf860d7ca34',
  //   role: 'ADMIN',
  //   displayName: "Alexandre Möller",
  //   email: "alex.moller@dp",
  //   photoURL: "/assets/images/faces/10.jpg",
  //   age: 46,
  //   // userId: "1",
  //   // role: 'ADMIN',
  //   // displayName: "Maria Eduarda Pereira",
  //   // email: "maria_edu@gmail.com",
  //   // photoURL: "/assets/images/faces/4.jpg",
  //   // age: 25,
  //   token: "faslkhfh423oiu4h4kj432rkj23h432u49ufjaklj423h4jkhkjh"
  // }

  // You need to send http request with email and passsword to your server in this method
  // Your server will return user object & a Token
  // User should have role property
  // You can define roles in app/auth/authRoles.js
  loginWithEmailAndPassword = async (email, password) => {
    return new Promise((resolve, reject) => {
      // setTimeout(() => {
      //   resolve(this.user);
      // }, 1000);

      async function getUser() {
        let api = axios.create({
          //timeout: 20000,
          //baseURL: "http://127.0.0.1:54019",  
          //baseURL: "https://localhost:5001",  
          baseURL: "http://zenite.inf.br:33017",
          headers: { 
            'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
            //'Content-Type': 'application/json',
            //'Accept': 'application/json',
             //'Access-Control-Allow-Origin': '*',
            // 'Access-Control-Allow-Methods': '*',
            // 'Access-Control-Allow-Headers':'*',
            // 'Access-Control-Allow-Methods': 'POST, PUT, DELETE, GET, OPTIONS',
            // 'Access-Control-Allow-Headers':'Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With',
            //'Connection': 'keep-alive'
          }
          //headers: { 'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8' }
        });
        let data = {
          username: email,
          password: password,
          grant_type: 'password',
          //tenant: sessionStorage.getItem('tenant')
          tenant: 'overlabel'
        };
        await api.post('token', qs.stringify(data))
        //api.post('api/account/login', qs.stringify(data))
        //api.post('api/account/login', data)
          .then((response)=>{
              
              let loggedUser = response.data;
              if(loggedUser && loggedUser.access_token){
                  let today = new Date();
                  let expDate = new Date(today.getTime() + (loggedUser.expires_in*1000));
                  loggedUser.expires = expDate.toString();
                  console.log(loggedUser);
                  resolve({
                    userId: loggedUser.userId,  
                    role: loggedUser.role,
                    displayName: loggedUser.displayName,
                    email: loggedUser.email,
                    photoURL: loggedUser.photoURL,
                    tenantDomain: loggedUser.tenantDomain ? loggedUser.tenantDomain : 'overlabel',
                    //age: 46,
                    //token: "faslkhfh423oiu4h4kj432rkj23h432u49ufjaklj423h4jkhkjh"
                    token: loggedUser.access_token
                  });                                  
              }
            })
          .catch((error) =>{
              
              console.log(error);
              return null;
          });
  
      } 

      getUser();

    }).then(data => {
      // Login successful
      // Save token
      //this.setSession(data.access_token);
      this.setSession(data.token);
      // Set user
      this.setUser(data);
      return data;
    });
  };

  // You need to send http requst with existing token to your server to check token is valid
  // This method is being used when user logged in & app is reloaded
  loginWithToken = () => {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve(localStorageService.getItem('auth_user'));
      }, 100);
    }).then(data => {
      // Token is valid
      if (data) {
        this.setSession(data.token);
        this.setUser(data);
      }
      return data;
    });
  };

  logout = () => {
    this.setSession(null);
    localStorageService.removeAll();
    this.removeUser();
  }

  // Set token to all http request header, so you don't need to attach everytime
  setSession = token => {
    
    if (token) {
      if (localStorageService.setItem("jwt_token", token))  {
        axios.defaults.headers.common["Authorization"] = "Bearer " + token;
        axios.defaults.headers.common["Accept"] = '*/*';
        axios.defaults.headers.post['Content-Type'] ='application/x-www-form-urlencoded;charset=UTF-8';
        // axios.defaults.headers.post['Access-Control-Allow-Origin'] = '*';
        axios.defaults.baseURL = "http://zenite.inf.br:33017";
        //axios.defaults.baseURL = "http://0.0.0.0:54019";
        //axios.defaults.baseURL = "http://localhost:44387";
      }
    } else {
      if (localStorageService.removeItem("jwt_token"))  {
        delete axios.defaults.headers["Authorization"];
      }
    }
  };

  // Save user to localstorage
  setUser = (user) => {    
    localStorageService.setItem("auth_user", user);
  }
  // Remove user from localstorage
  removeUser = () => {
    localStorageService.removeItem("auth_user");
  }

  getUser = () => localStorageService.getItem("auth_user");

  getAllRoles = () => {
    try {
      
      return api.get('api/account/GetAllRoles').then(response => {
        console.log(response.data);
        return response.data;
        });
    } catch (error) {
      console.log(error);
      return null;
    };

    // return new Promise((resolve, reject) => {
    //   // setTimeout(() => {
    //   //   resolve(this.user);
    //   // }, 1000);

    //   async function getRoles() {
    //     api.get('api/account/GetAllRoles')
    //       .then((response) => {
    //         resolve(response.data);                                  
    //       })
    //       .catch((error) =>{
    //         
    //         console.log(error);
    //         return null;
    //     });

    //   } 

    //   getRoles();

    // }).then(data => {
    //   return data;
    // });
  }

}

export default new JwtAuthService();
