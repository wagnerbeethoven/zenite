import React, { Component } from "react";
import history from "../../history.js";
import { withRouter } from "../../../node_modules/react-router-dom";
import { Icon, IconButton, MenuItem, Tooltip } from "@material-ui/core";
import { withStyles } from "@material-ui/core/styles";
import { connect } from "react-redux";
import { setLayoutSettings } from "../../redux/actions/LayoutActions";
import { logoutUser } from "../../redux/actions/UserActions";
import PropTypes from "prop-types";
import { MatxMenu, MatxSearchBox } from "../../matx/index";
import { isMdScreen, classList } from "../../utils";
import NotificationBar from "../SharedCompoents/NotificationBar";
//import WhatsApp from '@material-ui/icons/WhatsApp';
import { Link } from "../../../node_modules/react-router-dom";
import MailOutlineIcon from '@material-ui/icons/MailOutline';
//import ShoppingBasket from '@material-ui/icons/ShoppingBasket';
import ShopTwoTwoTone from '@material-ui/icons/ShopTwoTwoTone';
import AllInclusive from '@material-ui/icons/AllInclusive';
import WhatshotOutlined from '@material-ui/icons/WhatshotOutlined';
import MenuIcon from '@material-ui/icons/Menu';
import ShoppingCarOutlinedIcon from '@material-ui/icons/ShoppingCartOutlined';
import StarOutlinedIcon from '@material-ui/icons/StarOutlined';
import WebAssetIcon from '@material-ui/icons/WebAsset';
import HomeIcon from '@material-ui/icons/Home';
import PersonIcon from '@material-ui/icons/Person';
import PersonAdd from '@material-ui/icons/PersonAdd';
import SettingsIcon from '@material-ui/icons/Settings';
import PowerSettingsNewIcon from '@material-ui/icons/PowerSettingsNew';
import MatxCustomizer from "../SharedCompoents/MatxCustomizer/MatxCustomizer";
import { navigations } from "../../navigations";
import { MatxVerticalNav } from "../../matx/index";
import localStorageService from "../../services/localStorageService";

const styles = theme => ({
  topbar: {
    "& .topbar-hold": {
      backgroundColor: theme.palette.primary.main,
      height: "80px",
      "&.fixed": {
        boxShadow: theme.shadows[8],
        height: "64px"
      }
    }
  },
  menuItem: {
    display: "flex",
    alignItems: "center",
    minWidth: 185
  }
});

const user = localStorageService.getItem("auth_user");

// const openCards = (event) =>  {
//   event.preventDefault();
//   history.push({
//     pathname: "/cards/loja_virtual",
//     guid: 'acbc9c88-246d-4683-81dc-76dfbd20ab08'
//   });
//   //history.push("/cards/loja_virtual", { guid: '0635a43c-c99d-48e0-86bd-38edcd2f0065' });
// }

// const newCards = (event) =>  {
//   event.preventDefault();
//   history.push({
//     pathname: "/linkcards"
//   });
// }

class Layout1Topbar extends Component {
  state = {};

  todosProdutosEmEstoque = (event) =>  {
    event.preventDefault();
    history.push("/cards/loja_virtual", { id: 0 });
  }

  newCards = (event) =>  {
    event.preventDefault();
    history.push({
      pathname: "/linkcards"
    });
  }

  openCards = (event) =>  {
    event.preventDefault();
    history.push({
      pathname: "/linkcardsVend"
    });
    //history.push("/cards/loja_virtual", { guid: '0635a43c-c99d-48e0-86bd-38edcd2f0065' });
  }

  registerUser = (event) =>  {
    event.preventDefault();
    history.push({
      pathname: "/session/signup"
    });
  }
  
  openCardsCli = (event) =>  {
    event.preventDefault();
    history.push({
      pathname: "/linkcardsCli"
    });
  }

  openMeusPedidos = (event) =>  {
    event.preventDefault();
    history.push({
      pathname: "/pedidos"
    });
  }

    updateSidebarMode = sidebarSettings => {
    let { settings, setLayoutSettings } = this.props;

    setLayoutSettings({
      ...settings,
      layout1Settings: {
        ...settings.layout1Settings,
        leftSidebar: {
          ...settings.layout1Settings.leftSidebar,
          ...sidebarSettings
        }
      }
    });
  };

  handleSidebarToggle = () => {
    let { settings } = this.props;
    let { layout1Settings } = settings;

    let mode;
    if (isMdScreen()) {
      mode = layout1Settings.leftSidebar.mode === "close" ? "mobile" : "close";
    } else {
      mode = layout1Settings.leftSidebar.mode === "full" ? "close" : "full";
    }
    this.updateSidebarMode({ mode });
  };

  

  handleSignOut = () => {
    this.props.logoutUser();
  };

  render() {
    let { classes, fixed } = this.props;

    return (
      <div className={`topbar ${classes.topbar}`}>
        <div className={classList({ "topbar-hold": true, fixed: fixed })}>
          <div className="flex justify-between items-center h-full">
            <div className="flex">
              {/* <IconButton
                onClick={this.handleSidebarToggle}
                //className="hide-on-pc"
              >
                <Icon><MenuIcon/></Icon>
              </IconButton> */}

              {/* <div className="hide-on-mobile"> */}
              <div>
                {/* <MatxCustomizer /> */}
                {/* <MatxVerticalNav navigation={navigations} /> */}

                {/* <Tooltip  title="Meus Cards">
                  <IconButton
                    onClick={openCards}
                  >
                    <Icon><ShoppingCarOutlinedIcon/></Icon>
                  </IconButton>                
                </Tooltip> */}
                
                {/* <Tooltip title="Meus Pedidos">
                  <IconButton>
                    <Icon><ShopTwoTwoTone/></Icon>
                  </IconButton>
                </Tooltip> */}

                {/* <Tooltip title="Geração e envio de novos Cards">
                  <IconButton
                    onClick={newCards}
                  >
                    <Icon><WhatsApp /></Icon>
                  </IconButton>
                </Tooltip> */}
              </div>
            </div>
            <div className="flex items-center">
              {/* <MatxSearchBox /> */}

              {/* <NotificationBar /> */}

              <Tooltip title="Registrar novo usuário">
                <IconButton
                  onClick={this.registerUser}
                >
                  <Icon
                  ><PersonAdd /></Icon>
                </IconButton>
              </Tooltip>

              <Tooltip title="Todos os produtos">
                <IconButton
                  onClick={this.todosProdutosEmEstoque}
                >
                  <Icon
                  ><AllInclusive /></Icon>
                </IconButton>
              </Tooltip>

              <Tooltip title="Geração e envio de novos Cards">
                <IconButton
                  onClick={this.newCards}
                >
                  <Icon
                  ><AllInclusive /></Icon>
                </IconButton>
              </Tooltip>

              <Tooltip  title="Meus Cards - Vendedor">
                <IconButton
                  onClick={this.openCards}
                >
                  <Icon><ShoppingCarOutlinedIcon/></Icon>
                </IconButton>                
              </Tooltip>
              
              <Tooltip  title="Meus Cards - Cliente">
                <IconButton
                  onClick={this.openCardsCli}
                >
                  <Icon><ShoppingCarOutlinedIcon/></Icon>
                </IconButton>                
              </Tooltip>
              
              <Tooltip title="Meus Pedidos">
                <IconButton
                  onClick={this.openMeusPedidos}
                >
                  <Icon><ShopTwoTwoTone/></Icon>
                </IconButton>
              </Tooltip>

              <MatxMenu
                menuButton={
                  <img
                    className="mx-2 align-middle circular-image-small cursor-pointer"
                    src={user ? user.photoURL : ''}
                    alt="user"
                  />
                }
              >
                <MenuItem>
                  <Link className={classes.menuItem} to="/">
                    <Icon> <HomeIcon/> </Icon>
                    <span className="pl-4"> Home </span>
                  </Link>
                </MenuItem>
                <MenuItem>
                  {/* <Link
                    className={classes.menuItem}
                    to="/page-layouts/user-profile"
                  > */}
                  <Icon> <PersonIcon/> </Icon>
                  <span className="pl-4"> Profile </span>
                  {/* </Link> */}
                </MenuItem>
                <MenuItem className={classes.menuItem}>
                  <Icon> <SettingsIcon/> </Icon>
                  <span className="pl-4"> Settings </span>
                </MenuItem>
                <MenuItem
                  onClick={this.handleSignOut}
                  className={classes.menuItem}
                >
                  <Icon> <PowerSettingsNewIcon/> </Icon>
                  <span className="pl-4"> Logout </span>
                </MenuItem>
              </MatxMenu>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

Layout1Topbar.propTypes = {
  setLayoutSettings: PropTypes.func.isRequired,
  logoutUser: PropTypes.func.isRequired,
  settings: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  setLayoutSettings: PropTypes.func.isRequired,
  logoutUser: PropTypes.func.isRequired,
  settings: state.layout.settings
});

export default withStyles(styles, { withTheme: true })(
  withRouter(
    connect(mapStateToProps, { setLayoutSettings, logoutUser })(Layout1Topbar)
  )
);
