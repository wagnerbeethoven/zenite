import React, { useState } from "react";
import { Icon, IconButton, Hidden } from "@material-ui/core";
import { classList } from "Utils";
import CloseIcon from '@material-ui/icons/Close';
import MoreVertIcon from '@material-ui/icons/MoreVert';

const TopbarMenu = props => {
  let { offsetTop } = props;
  const [open, setOpen] = useState(false);

  const handleToggle = () => {
    setOpen(!open);
  };

  return (
    <div
      className={classList({
        "menu-wrap": true,
        open: open
      })}
    >
      <Hidden mdUp>
        <IconButton onClick={handleToggle}>
          <Icon>{open ? <CloseIcon/> : <MoreVertIcon/>}</Icon>
        </IconButton>
      </Hidden>

      <div
        style={{ top: offsetTop }}
        className="flex items-center menu-area container"
      >
        {props.children}
      </div>
    </div>
  );
};

export default TopbarMenu;
