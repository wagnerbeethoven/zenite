import React from "react";
import history from "../../history.js";
import {
  Icon,
  Badge,
  Card,
  Button,
  IconButton,
  Drawer,
  Tooltip
} from "@material-ui/core";
import { Link } from "../../../node_modules/react-router-dom";
import { withStyles, ThemeProvider } from "@material-ui/core/styles";
import { getTimeDifference } from "../../utils.js";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import WhatsApp from '@material-ui/icons/WhatsApp';
import PersonAdd from '@material-ui/icons/PersonAdd';
import {
  getNotification,
  deleteAllNotification,
  deleteNotification
} from "../../redux/actions/NotificationActions";
import ShoppingCarOutlinedIcon from '@material-ui/icons/ShoppingCartOutlined';
import ShopTwoTwoTone from '@material-ui/icons/ShopTwoTwoTone';
import NotificationsIcon from '@material-ui/icons/Notifications';
import ClearIcon from '@material-ui/icons/Clear';


const NotificationBar = props => {
  const {
    container,
    theme,
    settings,
    notification: notifcationList = [],
    getNotification,
    deleteAllNotification,
    deleteNotification
  } = props;

  const [panelOpen, setPanelOpen] = React.useState(false);

  const newCards = (event) =>  {
    event.preventDefault();
    history.push({
      pathname: "/linkcards"
    });
  }

  const registerUser = (event) =>  {
    event.preventDefault();
    history.push({
      pathname: "/session/signup"
    });
  }

  const openCards = (event) =>  {
    event.preventDefault();
    history.push({
      pathname: "/linkcardsVend"
    });
    //history.push("/cards/loja_virtual", { guid: '0635a43c-c99d-48e0-86bd-38edcd2f0065' });
  }
    
  const openCardsCli = (event) =>  {
    event.preventDefault();
    history.push({
      pathname: "/linkcardsCli"
    });
  }

  const openMeusPedidos = (event) =>  {
    event.preventDefault();
    history.push({
      pathname: "/pedidos"
    });
  }

  function handleDrawerToggle() {
    if (!panelOpen) {
      getNotification();
    }
    setPanelOpen(!panelOpen);
  }
  const parentThemePalette = theme.palette;

  return (
    <ThemeProvider theme={settings.themes[settings.activeTheme]}>

      <Tooltip title="Registrar novo usuário">
        <IconButton
          onClick={registerUser}
        >
          <Icon
          ><PersonAdd /></Icon>
        </IconButton>
      </Tooltip>

      {/* <Tooltip title="Geração e envio de novos Cards">
        <IconButton
          onClick={newCards}
        >
          <Icon
          ><WhatsApp /></Icon>
        </IconButton>
      </Tooltip> */}

      <Tooltip  title="Meus Cards">
        <IconButton
          onClick={openCards}
        >
          <Icon><ShoppingCarOutlinedIcon/></Icon>
        </IconButton>                
      </Tooltip>

      <Tooltip  title="Meus Cards">
        <IconButton
          onClick={openCardsCli}
        >
          <Icon><ShoppingCarOutlinedIcon/></Icon>
        </IconButton>                
      </Tooltip>
      
      <Tooltip title="Meus Pedidos">
        <IconButton
          onClick={openMeusPedidos}
        >
          <Icon><ShopTwoTwoTone/></Icon>
        </IconButton>
      </Tooltip>

      <IconButton
        //onClick={handleDrawerToggle}
        style={{
          color:
            parentThemePalette.type === "light"
              ? parentThemePalette.text.secondary
              : parentThemePalette.text.primary
        }}
      >
        <Badge color="secondary" badgeContent={5}>
          <Icon><NotificationsIcon/></Icon>
        </Badge>
      </IconButton>

      <Drawer
        width={"100px"}
        container={container}
        variant="temporary"
        anchor={"right"}
        open={panelOpen}
        onClose={handleDrawerToggle}
        ModalProps={{
          keepMounted: true
        }}
      >
        <div className="notification">
          <div className="notification__topbar flex items-center p-4 mb-4">
            <Icon color="primary"><NotificationsIcon/></Icon>
            <h5 className="ml-2 my-0 font-medium">Notifications</h5>
          </div>

          {notifcationList.map(notification => (
            <div
              key={notification.id}
              className="notification__card position-relative"
            >
              <IconButton
                size="small"
                className="delete-button bg-light-gray mr-6"
                onClick={() => deleteNotification(notification.id)}
              >
                <Icon className="text-muted" fontSize="small">
                  <ClearIcon className="text-muted" fontSize="small"/>
                </Icon>
              </IconButton>
              <Link to={`/${notification.path}`} onClick={handleDrawerToggle}>
                <Card className="mx-4 mb-6" elevation={3}>
                  <div className="card__topbar flex items-center justify-between p-2 bg-light-gray">
                    <div className="flex items-center">
                      <div className="card__topbar__button">
                        <Icon
                          className="card__topbar__icon"
                          fontSize="small"
                          color={notification.icon.color}
                        >
                          {notification.icon.name}
                        </Icon>
                      </div>
                      <span className="ml-4 font-medium text-muted">
                        {notification.heading}
                      </span>
                    </div>
                    <small className="card__topbar__time text-muted">
                      {getTimeDifference(new Date(notification.timestamp))} ago
                    </small>
                  </div>
                  <div className="px-4 pt-2 pb-4">
                    <p className="m-0">{notification.title}</p>
                    <small className="text-muted">
                      {notification.subtitle}
                    </small>
                  </div>
                </Card>
              </Link>
            </div>
          ))}

          <div className="text-center">
            <Button onClick={deleteAllNotification}>Clear Notifications</Button>
          </div>
        </div>
      </Drawer>
    </ThemeProvider>
  );
};

NotificationBar.propTypes = {
  settings: PropTypes.object.isRequired,
  notification: PropTypes.array.isRequired
};

const mapStateToProps = state => ({
  getNotification: PropTypes.func.isRequired,
  deleteNotification: PropTypes.func.isRequired,
  deleteAllNotification: PropTypes.func.isRequired,
  notification: state.notification,
  settings: state.layout.settings
});

export default withStyles(
  {},
  { withTheme: true }
)(
  connect(mapStateToProps, {
    getNotification,
    deleteNotification,
    deleteAllNotification
  })(NotificationBar)
);
