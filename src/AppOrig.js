import React, { useEffect } from 'react';
import './App.css';

import Routes from './routes';

function App() {
  useEffect(() => {
    sessionStorage.setItem('tenant', 'overlabel');
  },[]);

  return (
    <div className="container">
      <Routes />
    </div>      
  );
}

export default App;
